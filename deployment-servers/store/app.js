const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const helmet = require('helmet');

const app = express();

app.use(helmet());

// uncomment after placing your favicon in /public
const publicPath = path.join(__dirname, 'public');

app.use(favicon(path.join(publicPath, 'favicon.ico')));

app.use(express.static(publicPath));

app.use(function (req, res) {
  res.sendFile(path.join(publicPath, 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`server listening @ port ${port}`));
