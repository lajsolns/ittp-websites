import {NgModule} from '@angular/core';
import {IttCoreComponent} from './itt-core.component';
import {LoginComponent} from './routes/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SettingsEditorComponent} from './settings-editor/settings-editor.component';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {TagPropertiesEditorComponent} from './tag-properties-editor/tag-properties-editor.component';
import {MediaEditorComponent} from './media-editor/media-editor.component';

import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {environment} from './environments/environment';
import {routes} from './itt.routes';
import {RegisterComponent} from './routes/register/register.component';
import {BasicInfoEditorComponent} from './basic-info-editor/basic-info-editor.component';
import {ProfileHeaderComponent} from './routes/profileHeader/profile-header.component';
import {ProfileComponent} from './routes/profile/profile.component';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {FooterComponent} from './routes/footer/footer.component';
import {MaterialProgressBarComponent} from './routes/material-progress-bar/material-progress-bar.component';
import {MatProgressBar, MatProgressBarModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationService} from './core/services/authentication.service';
import { RegionsEditorComponent } from './regions-editor/regions-editor.component';
import { UserAelectionEditorComponent } from './user-aelection-editor/user-aelection-editor.component';
import {AuthServiceConfig} from 'angular-6-social-login-v2';
import {provideConfig} from '../../../theme-store/src/app/app.module';
import { NotFound404Component } from './not-found404/not-found404.component';
import {ThemeListComponent} from '../../../mythemes/src/app/routes/theme-list/theme-list.component';

@NgModule({
  declarations: [IttCoreComponent,
    LoginComponent,
    RegisterComponent,
    ProfileHeaderComponent,
    ProfileComponent,
    SearchBarComponent,
    ProfileComponent,
    FooterComponent,
    MaterialProgressBarComponent,
    RegionsEditorComponent,
    UserAelectionEditorComponent,
    NotFound404Component,
  ],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    HttpClientModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {enableTracing: false})
  ],

  exports: [
    IttCoreComponent,
    BasicInfoEditorComponent,
    SettingsEditorComponent,
    TagPropertiesEditorComponent,
    MediaEditorComponent,
    LoginComponent,
    RegisterComponent,
    ProfileHeaderComponent,
    ProfileComponent,
    SearchBarComponent,
    FooterComponent,
    MaterialProgressBarComponent,
    ThemesListComponent
    // UserAuthService
  ],

  providers: [{
    provide: 'API_BASE_URL',
    useValue: environment.apiBaseUrl
  }


  ]

})

export class IttCoreModule {
}
