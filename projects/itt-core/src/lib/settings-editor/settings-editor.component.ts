import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IMultiSelectOption, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {SelectOption, ThemeSettings} from '../core/models/theme-models';

@Component({
  selector: 'itt-itt-settings-editor',
  templateUrl: './settings-editor.component.html',
  styleUrls: ['./settings-editor.component.css']
})
export class SettingsEditorComponent implements OnInit {

  @Output() editComplete = new EventEmitter<any>();

  settingsForm: FormGroup;

  @Input() settings: ThemeSettings;


  regionEnum: IMultiSelectOption[] = [];

  @Input()
  currencies;

  @Input()
  region: SelectOption;

  optionsModel: string[];


  myTexts: IMultiSelectTexts = {
    defaultTitle: 'Regions',
  };

  constructor() {
  }

  ngOnInit() {

    this.settings = {
      allowCrowdSourcing: true,
      allowMovingTags: true,
      region: [1, 2, 3, 4],
      tagCount: 2,
      price: 0.0,
      currencies: [{value: '$', label: 'USD'}],
      visibleInStore: true,
    };

    // this.region = {
    //   name: 'CANADA',
    //   description: 'select option',
    //   values: [{
    //     value: 1,
    //     order: 1,
    //     description: '',
    //     label: 'CANADA'
    //   },
    //     {
    //       value: 2,
    //       order: 1,
    //       description: '',
    //       label: 'USA'
    //     }]
    // };
    //
    //
    // this.currencies = [
    //   {value: '$', label: 'USD'},
    //   {value: '$', label: 'POUND'}
    // ];


    this.initializeForm();

    this.region.values.forEach(value => {
      this.regionEnum.push({id: value.value, name: value.label});
    });
  }

  initializeForm() {
    this.settingsForm = new FormGroup({
      region: new FormControl(this.settings.region),
      price: new FormControl(this.settings.price),
      currency: new FormControl(this.settings.currencies),
      visibleInStore: new FormControl(this.settings.visibleInStore),
      allowCrowdSourcing: new FormControl(this.settings.allowCrowdSourcing),
      allowMovingTags: new FormControl(this.settings.allowMovingTags),
    });
  }

  saveStoreProperties(value) {
    this.editComplete.emit(value);
  }

}
