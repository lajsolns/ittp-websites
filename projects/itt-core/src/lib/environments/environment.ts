// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  googleClientId: '770362430007-6rofm64an3vr2521sbioqqso6nnt4uvb.apps.googleusercontent.com',
  facebookAppId: '365884523951157',
  // apiBaseUrl: 'http://localhost:3500/api/v1',
  // apiBaseUrl: 'https://itt-new-server.azurewebsites.net/api/v1',
  apiBaseUrl: 'https://itt-new-server.herokuapp.com/api/v1',
  termsAndConditionsUrl: 'asdf'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


