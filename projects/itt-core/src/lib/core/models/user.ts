export interface IUser {
  email: string;
  name: string;
  isAdmin: boolean;
  profileImageUrl: string;
}

export interface SubscriptionStatus {
  isPaidTheme: boolean;
  isSubscribed: boolean;
  isPaymentRequired: boolean;
}
