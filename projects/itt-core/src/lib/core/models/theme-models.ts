
export class Theme {
  _id: string;
  basicInfo: ThemeBasicInfo;
  tagProperties: TagProperty[];
  storeProperties: StoreProperties;
  themeProperties: ThemeProperties;
  currentBillingPlan: ThemeBillingPlan;
  media: ThemeMedia;
  stats: ThemeStats;
  createdAt;
  updatedAt;
  _storage: any;

  getMainIconUrl() {
    // return `${environment.apiUrl}/themes/${this.id}/media/main-icon.png`;
  }
  //
  // getPriceString() {
  //   const price = parseInt(this.currentBillingPlan.price);
  //   if (price) {
  //     return `${price} USD`;
  //   } else {
  //     return 'Free';
  //   }
  // }
}


export class ThemeBillingPlan {
  _id: string;
  price: string;
  paypalBillingPlanId: string;
}

export class ThemeProperties {
  visibleInStore: boolean;
  requiresApproval: boolean;
  allowFastTags: boolean;
  allowCrowdSourcing: boolean;
  allowMovingTags: boolean;
  openToPublic: boolean;
  active: boolean;
}

export class ThemeBasicInfo {
  name: string;
  description: string;
  color: number;
  defaultDistance: number;
  defaultDistanceUnit: number;
  defaultTagSilenceUnit: number;
  defaultTagSilence: number;
}

export class  TagProperty {
  name: string;
  normalizedName: string;
  tagPropertyType = 0;
  page: number;
  order = 1;
  mandatory = false;
  showOnMap = false;
  showOnTip = false;
  inherited: boolean;
}

export class ThemeMedia {
  profileUrl: string;
}

export class ThemeSettings {
  region: number[];
  price = 0.0;
  currencies: [Currency];
  tagCount: number;
  visibleInStore: boolean;
  allowCrowdSourcing: boolean;
  allowMovingTags: boolean;
}

export class StoreProperties {
  region: number[];
  price = 0.0;
  uploadNotes: string;
  currency: number;
  visibleInStore: boolean;
  billingFreq: number;
}

export class ThemeStats {
  tagCount: number;
  activeSubscribers: number;
  totalSubscribers: number;
  averageRating: number;
  totalNumberOfRatings: number;
}

export interface Region {
  name: string;
  description: string;
  values: [RegionValues];
}

export interface RegionValues {
  value: number;
  order: number;
  description: string;
  label: string;
}

export interface Currency {
value: string;
label: string;
}

export interface ThemeEditPreview {
  _id: string;
  name: string;
  description: string;
  tagCount: string;
  themeImageUrl: string;
}


export interface SelectOption {
  name: string;
  description: string;
  values: SelectOptionUnit[];
}

export interface SelectOptionUnit {
  value: number;
  order: number;
  description: string;
  label: string;
}
