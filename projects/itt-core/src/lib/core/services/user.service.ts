import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IUser} from '../models/user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  me(): Observable<IUser> {
    const url = `${this.apiBaseUrl}/user/me`;
    return this.http.get<IUser>(url);
  }

}
