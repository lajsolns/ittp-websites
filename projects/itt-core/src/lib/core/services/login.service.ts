import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {ServerAuthService} from './server-auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private router: Router,
    private authenticationSvc: AuthenticationService,
    private serverAuthSvc: ServerAuthService
  ) {
  }

  login(params: LoginParams) {
    this
      .serverAuthSvc
      .signinLocal(params.email, params.password)
      .subscribe(loginModel => {
        this.authenticationSvc.login(loginModel);
        this.router.navigate([params.returnUrl]);
      }, err => {
        console.log(err);
        alert(err.error.message);
      });
  }
}


export interface LoginParams {
  email: string;
  password: string;
  returnUrl: string;
}
