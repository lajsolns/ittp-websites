import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServerAuthService {
  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  signinGoogle(token: string): Observable<UserLoginModel> {
    const url = `${this.apiBaseUrl}/auth/google`;
    return this.http.post<UserLoginModel>(url, null, {headers: this.getBearerHeader(token)});
  }

  signinFacebook(token: string): Observable<UserLoginModel> {
    const url = `${this.apiBaseUrl}/auth/facebook`;
    return this.http.post<UserLoginModel>(url, null, {headers: this.getBearerHeader(token)});
  }

  signinLocal(email: string, password: string): Observable<UserLoginModel> {
    const url = `${this.apiBaseUrl}/auth/local`;
    return this.http.post<UserLoginModel>(url, {email, password});
  }

  register(email: string, password: string, username: string): Observable<RegistrationErrorResponse> {
    const url = `${this.apiBaseUrl}/user/register`;
    return this.http.post<RegistrationErrorResponse>(url, {email, password, username});
  }

  private getBearerHeader(token: string): HttpHeaders {
    return new HttpHeaders().set('Authorization', `Bearer ${token}`);
  }
}

export interface UserLoginModel {
  token: string;
  email: string;
  _id: string;
  name: string;
}

export interface RegistrationErrorResponse {
  code: number;
  msg: string;
  reasons: string[];
}
