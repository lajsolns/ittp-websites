import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {ServerAuthService, UserLoginModel} from './server-auth.service';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  currentUser: Observable<UserLoginModel>;
  loginState: Observable<boolean>;

  private currentUserSubject: BehaviorSubject<UserLoginModel>;
  private loginStateSubject: BehaviorSubject<boolean>;
  private KEY_USER = 'KEY_USER';

  constructor(private loginService: ServerAuthService) {
    const user = JSON.parse(localStorage.getItem(this.KEY_USER));

    this.currentUserSubject = new BehaviorSubject<UserLoginModel>(user);
    this.loginStateSubject = new BehaviorSubject<boolean>(!!user);

    this.currentUser = this.currentUserSubject.asObservable();
    this.loginState = this.loginStateSubject.asObservable();
  }

  get currentUserValue(): UserLoginModel {
    return this.currentUserSubject.value;
  }

  get isLogggedIn(): boolean {
    return this.loginStateSubject.value;
  }

  login(userLoginModel: UserLoginModel) {
    localStorage.setItem(this.KEY_USER, JSON.stringify(userLoginModel));
    this.currentUserSubject.next(userLoginModel);
    this.loginStateSubject.next(true);
    console.log(userLoginModel);
  }

  logout() {
    localStorage.removeItem(this.KEY_USER);
    this.currentUserSubject.next(null);
    this.loginStateSubject.next(false);
    location.reload(true);
  }

  private retrieveToken(): string {
    return localStorage.getItem(this.KEY_USER);
  }

  private saveToken(token: string) {
    localStorage.setItem(this.KEY_USER, token);
  }
}
