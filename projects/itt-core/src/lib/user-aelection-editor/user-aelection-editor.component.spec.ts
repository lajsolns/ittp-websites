import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAelectionEditorComponent } from './user-aelection-editor.component';

describe('UserAelectionEditorComponent', () => {
  let component: UserAelectionEditorComponent;
  let fixture: ComponentFixture<UserAelectionEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAelectionEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAelectionEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
