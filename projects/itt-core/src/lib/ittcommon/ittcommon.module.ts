import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FooterComponent} from './footer/footer.component';
import {MaterialProgressBarComponent} from './material-progress-bar/material-progress-bar.component';
import {ProfileComponent} from './profile/profile.component';
import {ProfileHeaderComponent} from './profile-header/profile-header.component';

@NgModule({
  declarations: [
    FooterComponent,
    MaterialProgressBarComponent,
    ProfileComponent,
    ProfileHeaderComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ITTCommonModule { }
