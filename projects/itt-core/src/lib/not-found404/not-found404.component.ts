import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'itt-not-found404',
  templateUrl: './not-found404.component.html',
  styleUrls: ['./not-found404.component.css']
})
export class NotFound404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
