import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'itt-itt-media-editor',
  templateUrl: './media-editor.component.html',
  styleUrls: ['./media-editor.component.css']
})
export class MediaEditorComponent implements OnInit {

  @Input() themeMedia;
  @Input() imageDescription;
  @Input() size;
  themeImageToUpload: File;
  @Output() themeImageEmitter = new EventEmitter();

  private MAX_FILE_SIZE_IN_MB = 2;
  iconsUrls: any = {};


  constructor(private el: ElementRef, private sanitization: DomSanitizer) {

  }

  ngOnInit() {
    // this.themeImageToUpload = new FormData();
  }


  onImageSelected(event, imageId: string) {
    const sizeFactor = imageId === 'themeImageThumbnail' ? 1 : 0.5;
    const imageEl: HTMLImageElement = this.el.nativeElement.querySelector(`#${imageId}`);
    const selectedFile = event.target.files[0];
    const reader = new FileReader();

    if (!this.isValidFileSize(selectedFile, sizeFactor)) {
      // this.toastr.errorToastr('Maximum file size exceeded');
      return;
    }

    if (imageId === 'themeImageThumbnail') {
      this.emitThemeImage();

    }


    reader.onload = function (e: any) {
      const src = e.currentTarget.result;
      imageEl.src = src;
    };

    reader.readAsDataURL(selectedFile);
  }


  emitThemeImage() {
    const imageInput: HTMLInputElement = this.el.nativeElement.querySelector('#ThemeImage');
    // this.themeImageToUpload.append('file', imageInput.files.item(0));
    this.themeImageToUpload = imageInput.files.item(0);
    console.log('imgtoupload: ', this.themeImageToUpload);
    this.themeImageEmitter.emit(this.themeImageToUpload);

  }

  private isValidFileSize(file, factor: number): boolean {
    const maxFileSizeInBytes = (factor * 1024 * 1024);
    return file.size <= maxFileSizeInBytes;
  }
}
