import {LoginComponent} from './routes/login/login.component';
import {Routes} from '@angular/router';
import {RegisterComponent} from './routes/register/register.component';

export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
];
