import {AbstractControl} from '@angular/forms';

export function passwordValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const confirmPasswordValue = control.value;

    const passControl = control.root.get('password');

    if (passControl) {
      const passwordValue = passControl.value;
      if (passwordValue !== confirmPasswordValue) {
        return {
          isError: true
        };
      }
    }
    return null;
  }
}
