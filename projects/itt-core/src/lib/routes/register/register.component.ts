import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {AuthenticationService} from '../../core/services/authentication.service';
import {RegistrationErrorResponse, ServerAuthService} from '../../core/services/server-auth.service';
import {LoginService} from '../../core/services/login.service';
import {passwordValidator} from './validators';

@Component({
  selector: 'itt-itt-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  returnUrl: string;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;

  constructor(
    private authenticationSvc: AuthenticationService,
    private router: Router,
    private serverAuthSvc: ServerAuthService,
    private route: ActivatedRoute,
    private loginSvc: LoginService,
    // private sideNav: SideNavService
  ) {
    // super(router, sideNav);
    // super(router, sideNav);
    this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';
    if (authenticationSvc.isLogggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('username', Validators.compose([Validators.required, Validators.minLength(6)])),
      email: new FormControl('agorteyp@gmail.com', Validators.compose([Validators.required, Validators.minLength(6)])),
      password: new FormControl('Password.1', Validators.compose([Validators.required, Validators.minLength(6)])),
      confirmPassword: new FormControl('Password.1', Validators.compose([Validators.required, Validators.minLength(6),
        passwordValidator])),
    });
  }

  register(registrationDetails) {
    const email = registrationDetails.email.trim();
    const password = registrationDetails.password.trim();
    const username = registrationDetails.username.trim();


    this
      .serverAuthSvc
      .register(email, password, username)
      .subscribe(res => {
        this.loginSvc.login({email, password, returnUrl: this.returnUrl});
      }, err => {
        const registrationErr = err.error as RegistrationErrorResponse;
        console.log(registrationErr.msg);
        alert(registrationErr.msg);
      });
  }

}
