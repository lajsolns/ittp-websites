import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AuthenticationService} from '../../core/services/authentication.service';
import {UserService} from '../../core/services/user.service';

@Component({
  selector: 'itt-itt-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.css']
})
export class ProfileHeaderComponent implements OnInit {

  @Input()
  returnUrl: string;

  email: string;
  isLoggedIn = false;
  profileImage: string = 'assets/images/ic_profile-pic.png';


  constructor(private authenticationSvc: AuthenticationService,
              private router: Router,
              private userSvc: UserService,
              private toastr: ToastrManager) {
  }

  ngOnInit() {
    this.authenticationSvc.currentUser.subscribe(user => {
      this.email = (user && user.email) || 'Login';
    });

    this.authenticationSvc.loginState.subscribe(isLoggedIn => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn === true) {
        this.userSvc.me().subscribe((user) => {
          this.profileImage = user.profileImageUrl;
        }, (error) => {
          this.profileImage = 'assets/images/ic_profile-pic.png';

        });
      }
    });


  }

  logout() {
    this.authenticationSvc.logout();
  }

  login() {
    this.router.navigate(['/local'], {queryParams: {returnUrl: this.returnUrl}});
  }
}
