import {Component, Input, OnInit} from '@angular/core';
import {Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'itt-itt-material-progress-bar',
  templateUrl: './material-progress-bar.component.html',
  styleUrls: ['./material-progress-bar.component.css']
})
export class MaterialProgressBarComponent implements OnInit {

  loading = false;

  constructor(private router: Router) {

    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngOnInit(): void {
  }

}
