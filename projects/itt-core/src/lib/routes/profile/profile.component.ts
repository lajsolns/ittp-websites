import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AuthenticationService} from '../../core/services/authentication.service';
import {UserService} from '../../core/services/user.service';

@Component({
  selector: 'itt-itt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input()
  returnUrl: string;

  email: string;
  profileImageUrl: string = 'assets/images/ic_profile-pic.png';
  isLoggedIn = false;
  isOpened: boolean = false;


  constructor(private authenticationSvc: AuthenticationService,
              private router: Router,
              private userSvc: UserService,
              private toastr: ToastrManager) {
  }

  ngOnInit() {
    this.authenticationSvc.currentUser.subscribe(user => {
      this.email = (user && user.email) || 'Logged Out';
    });

    this.authenticationSvc.loginState.subscribe(isLoggedIn => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn === true) {
        this.userSvc.me().subscribe((user) => {
          this.profileImageUrl = user.profileImageUrl;
        }, error => {
          this.profileImageUrl = 'assets/images/ic_profile-pic.png';
        });
      }
    });
  }

  logout() {
    this.authenticationSvc.logout();
  }

  login() {
    this.router.navigate(['/local'], {queryParams: {returnUrl: this.returnUrl}});
  }
}
