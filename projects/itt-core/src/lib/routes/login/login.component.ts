import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {AuthenticationService} from '../../core/services/authentication.service';
import {LoginService} from '../../core/services/login.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'itt-itt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  returnUrl: string;
  loginForm: FormGroup;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;


  constructor(
    private router: Router,
    private authenticationSvc: AuthenticationService,
    private loginSvc: LoginService,
    route: ActivatedRoute,
  ) {
    this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';

    if (authenticationSvc.isLogggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.email, Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  loginLocal(loginCreds) {
    const email = loginCreds.email.trim();
    const password = loginCreds.password.trim();

    this
      .loginSvc
      .login({email, password, returnUrl: this.returnUrl});
  }

}
