import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectOptionUnit, TagProperty} from '../core/models/theme-models';

@Component({
  selector: 'itt-itt-tag-properties-editor',
  templateUrl: './tag-properties-editor.component.html',
  styleUrls: ['./tag-properties-editor.component.css']
})
export class TagPropertiesEditorComponent implements OnInit {
  @Input()
  tagPropertyType: SelectOptionUnit[];
  @Input()
  tagProperties: TagProperty[] = [];

  tagPropertiesForm: FormGroup = new FormGroup({});

  dropdownNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

  tagPropertiesFormArray: FormArray;


  constructor() {

  }


  ngOnInit() {
    // this.tagPropertyType = ['Text', 'Pone Number', 'Email Address', 'Web Address', 'Image Link', 'Video Link', 'Numbers Only', 'Address'];

    // this.tagProperties = [{
    //   name: 'Text',
    //   normalizedName: 'normalizedname1',
    //   tagPropertyType: 1,
    //   page: 1,
    //   order: 1,
    //   mandatory: true,
    //   showOnMap: true,
    //   showOnTip: false,
    //   inherited: true,
    // }];
    //
    // this.tagPropertyType = {
    //   name: 'A',
    //   values: [{
    //     value: 1,
    //     label: 'hello',
    //     order: 2,
    //     description: 'sfsdf'
    //   },
    //     {
    //       value: 1,
    //       label: 'hello',
    //       order: 2,
    //       description: 'sfsdf'
    //     }],
    //   description: ''
    // };

    this.initializeForm();
    console.log(this.tagPropertyType);
  }

  initializeForm() {
    this.tagPropertiesFormArray = new FormArray([]);
    this.tagPropertiesForm.setControl('tagProperties', this.tagPropertiesFormArray);
    this.initializeTagPropertiesFormArray();
  }


  initializeTagPropertyForm(tagProperty: TagProperty): FormGroup {
    return new FormGroup(
      {
        name: new FormControl(tagProperty.name, [Validators.required, this.duplicateNameValidator.bind(this)]),
        tagPropertyType: new FormControl(tagProperty.tagPropertyType, [Validators.required]),
        order: new FormControl(tagProperty.order),
        mandatory: new FormControl(tagProperty.mandatory, [Validators.required]),
        showOnMap: new FormControl(tagProperty.showOnMap, [Validators.required]),
        showOnTip: new FormControl(tagProperty.showOnTip, [Validators.required]),
        normalizedName: new FormControl(tagProperty.normalizedName),
      }
    );
  }

  add() {
    this.tagPropertiesFormArray.push(this.initializeTagPropertyForm(new TagProperty()));
  }

  delete(index: number) {
    this.tagPropertiesFormArray.removeAt(index);
  }

  reset(index: number) {
    this.tagPropertiesFormArray.at(index).reset();
  }

  initializeTagPropertiesFormArray() {
    this.tagProperties.forEach(tagProperty => {
      this.tagPropertiesFormArray.push(this.initializeTagPropertyForm(tagProperty));
    });
  }


  tagPropertyNameChanged(i: string) {

    if (this.tagPropertiesFormArray.controls[i].controls['name'].value) {
      this.tagPropertiesFormArray.controls[i].controls['normalizedName']
        .setValue(this.tagPropertiesFormArray.controls[i].controls['name'].value.toLowerCase());
      this.tagPropertiesFormArray.updateValueAndValidity();
    }
  }


  viewMoreOderNumbers(index: string) {
    for (let i = 1; i <= 10; i++) {
      this.dropdownNumbers.push(this.dropdownNumbers[this.dropdownNumbers.length - 1] + 1);
    }

    this.tagPropertiesFormArray.controls[index].controls['order'].setValue(this.dropdownNumbers[this.dropdownNumbers.length - 9]);

  }

  reOrderTags(fromIndex, toIndex) {
    const formArrayControl = this.tagPropertiesFormArray.controls[`${fromIndex}`];
    this.tagPropertiesFormArray.removeAt(fromIndex);
    this.tagPropertiesFormArray.insert(toIndex, formArrayControl);
  }

  duplicateNameValidator(control: AbstractControl) {

    let nameCount = 0;
    this.tagPropertiesFormArray.value.forEach(tagProperty => {
      if (control.value && tagProperty.normalizedName === control.value.toLocaleLowerCase()) {
        nameCount++;
      }
      return null;
    });
    if (nameCount > 1) {
      return {duplicateName: true};
    } else {
      return (null);
    }
  }

  saveTagProperties(tagProperties) {
    console.log(tagProperties);
  }
}
