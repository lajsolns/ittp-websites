import {Component, Input, OnInit} from '@angular/core';
import {ThemeListPaginationItemModel, ThemeListPagingHandler, ThemeListSearchHandler} from './models';
import {PaginationResModel} from '../../../../../mythemes/src/app/core/services/theme-edit.service';

// import {PaginationResModel} from '../../../../../../admin-site/src/app/core/services/tagProperties-edit.service';

// @ts-ignore
// @ts-ignore
@Component({
  selector: 'itt-itt-themes-list',
  templateUrl: './themes-list.component.html',
  styleUrls: ['./themes-list.component.css']
})
export class ThemesListComponent implements OnInit {
  @Input()
  searchHandler: ThemeListSearchHandler;

  @Input()
  pagingHandler: ThemeListPagingHandler;

  themes: ThemeListPaginationItemModel[] = [];
  pageSize = 10;
  currentPage;
  count;

  constructor() {
  }

  ngOnInit() {
    this.loadFirstPage();
  }

  onPageChanged(page: number) {
    this
      .pagingHandler
      .getPage({page, pageSize: this.pageSize})
      .subscribe(resModel => this.updatePage(resModel), error1 => alert(error1.toString()));
  }


  private loadFirstPage() {
    this
      .pagingHandler
      .getPage({page: 1, pageSize: this.pageSize})
      .subscribe(resModel => this.updatePage(resModel), error1 => alert(error1.toString()));
  }

  updatePage(resModel: PaginationResModel) {
    this.themes = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
  }

  findTheme() {
    //  todo
  }
}
