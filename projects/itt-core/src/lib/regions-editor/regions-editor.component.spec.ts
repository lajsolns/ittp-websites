import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionsEditorComponent } from './regions-editor.component';

describe('RegionsEditorComponent', () => {
  let component: RegionsEditorComponent;
  let fixture: ComponentFixture<RegionsEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionsEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
