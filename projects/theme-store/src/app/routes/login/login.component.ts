import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserAuthService} from '../../core/services/user-auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SigninStrategyService} from '../../core/services/signin-strategy.service';
import {environment} from '../../../environments/environment';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ServerAuthService} from '../../core/services/server-auth.service';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from 'angularfire2/auth';
import {BlockUI, NgBlockUI} from "ng-block-ui";

@Component({
  selector: 'itt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  loginForm: FormGroup;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;
  user? = {};
  @BlockUI() blockUI: NgBlockUI;

  constructor(
    private router: Router,
    private authenticationSvc: UserAuthService,
    private signinStrategyService: SigninStrategyService,
    private serverAuth: ServerAuthService,
    private afAuth: AngularFireAuth,
    route: ActivatedRoute,
    private toastr: ToastrManager
  ) {
    this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';

    if (authenticationSvc.isLoggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.getLoginDetailsAndLogin();
  }

  loginLocal(loginCreds) {
    const email = loginCreds.email.trim();
    const password = loginCreds.password.trim();

    this
      .signinStrategyService
      .signInFirebaseUserWithCreds({email, password, returnUrl: this.returnUrl});
  }

  loginFacebook() {
    this
      .signinStrategyService
      .signInFacebook(this.returnUrl)
      .then((res) => {

        // console.log('res', res);
        // res.subscribe(r => {
        //   console.log('is login: ', r);
        //   this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
        //   // this.router.navigate([this.returnUrl]);
        //   window.location.href = this.returnUrl;
        // });
      });
    this.getLoginDetailsAndLogin();
  }

  loginGoogle() {

    this
      .signinStrategyService
      .signInGoogle(this.returnUrl)
      .then((res) => {
          // res.subscribe(r => {
          //   console.log('is login: ', r);
          //   this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
          //   // this.router.navigate([this.returnUrl]);
          //   window.location.href = this.returnUrl;
          // });
        }
      );
    this.getLoginDetailsAndLogin();
  }

  loginTwitter() {
    this
      .signinStrategyService
      .signInTwitter(this.returnUrl)
      .then((res) => {
        res.subscribe(r => {
          console.log('is login: ', r);
          this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
          // this.router.navigate([this.returnUrl]);
          window.location.href = this.returnUrl;
        });
      });
  }

  goToHome() {
    this.router.navigate([this.returnUrl]);
  }


  getLoginDetailsAndLogin() {
    // this.blockUI.start('loading');
    this.blockUI.start('loading');
    firebase
      .auth()
      .getRedirectResult()
      .then((result) => {
        if (result.user) {
          this.handleRedirect(result)
        }else {
          this.blockUI.stop();
        }
        console.log(result);
      }).catch((error) => {
      this.toastr.errorToastr('Something went wrong', error);
    });
  }

  private handleRedirect(result: firebase.auth.UserCredential) {
    // @ts-ignore
    this.serverAuth.signinFirebase(result.user.ra)
      .subscribe(userModel => {
        this.signinStrategyService.saveLoginAndNavigateToReturnUrl(userModel);
        this.blockUI.stop();
        window.location.href = this.returnUrl;
      });
  }
}
