import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute, Router} from '@angular/router';
import {SimilarTheme, ThemeDetail} from '../../core/models/theme-detail.model';
import {SubscriptionStatus} from '../../core/models/user';
import {SubscriptionService} from '../../core/services/subscription.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSidenavContainer, MatSidenavContent} from '@angular/material';
import {StoreService} from '../../core/services/store.service';
import {BlockUI, NgBlockUI} from "ng-block-ui";

@Component({
  selector: 'itt-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {
  theme: ThemeDetail;
  subscriptionStatus: SubscriptionStatus;
  btnCode: number;
  isOpened = false;
  events: string[] = [];
  opened: boolean;
  similarThemes: SimilarTheme[];
  @BlockUI() blockUI: NgBlockUI;

  constructor(
    private toastr: ToastrManager,
    private state: ActivatedRoute,
    private subscriptionService: SubscriptionService,
    private router: Router,
    private storeService: StoreService
  ) {
    this.theme = this.state.snapshot.data['themeDetail'];
    this.subscriptionStatus = this.state.snapshot.data['subscriptionStatus'];
    console.log('status: ', this.subscriptionStatus);
    this.processSubscriptionStatus();
  }

  ngOnInit(): void {
    if (!this.subscriptionStatus.hasAccess) {
      this.toastr.warningToastr(`You aren't eligible for subscription! please contact Admin `);
    } else if (!this.subscriptionStatus.isSubscribed && !this.subscriptionStatus.isPaymentRequired) {
      this.subscribe();
    } else if (!this.subscriptionStatus.isSubscribed && this.subscriptionStatus.isPaymentRequired) {
      this.blockUI.start('going to paypal');
      this.goToPaypal();
    }

    // this.storeService.getSimilarThemes(this.theme._id).subscribe((res) => {
    //   this.similarThemes = res;
    // }, (err) => {
    // });
  }

  subscribe() {
    this
      .subscriptionService
      .subscribeToTheme(this.theme._id)
      .subscribe(res => {
        this.toastr.successToastr('successfully subscribed');

        this.reloadSubscriptionStatus();
      }, error1 => {
        this.toastr.errorToastr('Could not subscribe you');

      });
  }

  unsubscribe() {
    this
      .subscriptionService
      .unsubscribe(this.theme._id)
      .subscribe(res => {
        this.toastr.successToastr(res.message);
        this.reloadSubscriptionStatus();
        this.router.navigate(['/store-details', this.theme._id]);
      }, error1 => {
        this.toastr.errorToastr('Could not unsubscribe');
      });
  }

  goToPaypal() {
    this
      .subscriptionService
      .createPaypalBillingAgreement(this.theme._id)
      .subscribe(billingAgreementRes => {
        location.href = billingAgreementRes.approvalUrl;
      }, error1 => {
        console.log(error1);
      });
  }

  processSubscriptionStatus() {
    if (this.subscriptionStatus.isSubscribed) {
      this.btnCode = 1; // show unsubscribe
    } else {
      if (this.subscriptionStatus.isPaymentRequired) {
        this.btnCode = 2; // show go to Paypal
      } else {
        this.btnCode = 3; // show subscribe
      }
    }
  }

  reloadSubscriptionStatus() {
    this
      .subscriptionService
      .getThemeSubscriptionStatus(this.theme._id)
      .subscribe(status => {
        this.subscriptionStatus = status;
        this.processSubscriptionStatus();
      }, error1 => {
        console.log(error1);
      });
  }


  gotoReview() {
    this.router.navigate(['add-review', this.theme._id]);
  }

  ngOnDestroy() {
    this.blockUI.stop();
  }


}


