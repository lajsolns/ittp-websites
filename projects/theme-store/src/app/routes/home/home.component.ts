import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {IThemePreview} from '../../core/models/store-models';
import {UserAuthService} from '../../core/services/user-auth.service';
import {SelectOption} from '../../core/models/select-option';
import {SubscriptionService} from '../../core/services/subscription.service';
import {StoreService} from '../../core/services/store.service';
import {RouteComponent} from '../../core/route-component';
import {Router} from '@angular/router';
import {SideNavService} from '../../core/services/side-nav.service';
import {SearchService} from '../../core/services/search.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends RouteComponent implements OnInit {
  topFreePreviews: IThemePreview[] = [];
  topPaidPreviews: IThemePreview[] = [];
  subscribedThemes: Observable<string[]>;
  categoryOptions: SelectOption[];
  nextFree = true;
  nextPaid = true;
  mystyle;
  showSubscribeBtn = true;
  events: string[] = [];
  opened: boolean;
  currentPageFree = 1;
  currentPagePaid = 1;
  pageSize = 8;
  count;
  themeManager = environment.mythemeUrl;

  constructor(
    private authenticationSvc: UserAuthService,
    private subscriptionSvc: SubscriptionService,
    private storeSvc: StoreService,
    private searchSvc: SearchService,
    private router: Router,
    private toastr: ToastrManager,
    sideNavSvc: SideNavService,
    el: ElementRef
  ) {
    super(router, sideNavSvc);
    this.topFreePreviews = this.storeSvc.getDummyThemePreviewCollection(8);
    this.topPaidPreviews = this.storeSvc.getDummyThemePreviewCollection(8);
    this.loadHomePageModels();
    if (this.authenticationSvc.isLoggedIn) {
      this.subscribedThemes = this.processSubscribedThemes();
      // subscribe(subscriptionsArr => {
      //   console.log('subscribed themes', subscriptionsArr);
      //   this.subscribedThemes = subscriptionsArr;
      //
      // }, err => {
      //   console.log('get subscribed themes error', err);
      // });
    } else {
      this.showSubscribeBtn = true;
    }
  }

  ngOnInit() {

    this.processSubscribedThemes();
    this.setupAuthStateListener();
  }


  trySubscribe(event) {
    this.router.navigate(['subscription', event.id]);
  }

  setupAuthStateListener() {
    this.authenticationSvc.loginState.subscribe(x => this.processSubscribedThemes());
  }


  loadHomePageModels() {
    // this.storeSvc
    //   .getHomePageModels()
    //   .subscribe(result => {
    //     this.topFreePreviews = result.topFree;
    //     this.topPaidPreviews = result.topPaid;
    //     this.categoryOptions = result.categories;
    //   }, err => console.log(err));

    this.storeSvc.getFreeThemePaginate(this.currentPageFree, this.pageSize).subscribe(result => {
        this.topFreePreviews = result.data;
        this.nextFree = result.next;
      },
      error => {

      });

    this.storeSvc.getPaidThemePaginate(this.currentPagePaid, this.pageSize).subscribe(result => {
        this.topPaidPreviews = result.data;
        this.nextPaid = result.next;
      },
      error => {

      });
  }


  processSubscribedThemes(): Observable<string[]> {
    return this.subscriptionSvc.getUserThemes();
    // this.subscribedThemes = [];
  }

  topFreeViewMore() {
    this.currentPageFree++;
    this.storeSvc.getFreeThemePaginate(this.currentPageFree, this.pageSize).subscribe(result => {
      this.topFreePreviews = result.data;
      this.nextFree = result.next;
      console.log(result.data);
    });
  }

  topPaidViewMore() {
    this.currentPagePaid++;
    this.storeSvc.getPaidThemePaginate(this.currentPagePaid, this.pageSize).subscribe(result => {
      this.topPaidPreviews = result.data;
      this.nextPaid = result.next;
    });
  }

  onCategorySelected(selected: number) {
    // console.log(selected);
  }

  onSearchInput(value: any) {
    console.log('searched pressed');
    const searchValue = value.searchInput;
    this.router.navigate([`/search/${searchValue}`]);
  }

}
