import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscriptionService} from '../../core/services/subscription.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeDetail} from '../../core/models/theme-detail.model';

@Component({
  selector: 'itt-paypal-aprroval',
  templateUrl: './paypal-aprroval.component.html',
  styleUrls: ['./paypal-aprroval.component.css']
})
export class PaypalAprrovalComponent implements OnInit {
  theme: ThemeDetail;
  token: string;
  isOpened: boolean = false;


  constructor(
    private toastr: ToastrManager,
    private state: ActivatedRoute,
    private subscriptionService: SubscriptionService,
    private router: Router
  ) {
    this.theme = this.state.snapshot.data['themeDetail'];
    this.token = this.state.snapshot.queryParams['token'];
    this.executeBillingAgreement();
  }

  ngOnInit() {
  }

  executeBillingAgreement() {
    this
      .subscriptionService
      .executeBillingAgreement(this.theme._id, this.token)
      .subscribe(res => {
        this.router.navigate(['/subscription', this.theme._id]);
      }, error1 => {
        console.log(error1);
      });
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
