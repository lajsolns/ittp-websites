import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import * as firebase from "firebase";
import {ActivatedRoute, Router} from "@angular/router";
import {UserAuthService} from "../../core/services/user-auth.service";
import {SigninStrategyService} from "../../core/services/signin-strategy.service";
import {ServerAuthService} from "../../core/services/server-auth.service";
import {ToastrManager} from "ng6-toastr-notifications";

@Component({
  selector: 'itt-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css']
})
export class AccessComponent implements OnInit {
  store = environment.storeUrl;

  constructor() {
  }

  ngOnInit() {
  }

}
