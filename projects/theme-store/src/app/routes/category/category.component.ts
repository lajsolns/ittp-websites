import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StoreService} from '../../core/services/store.service';
import {IThemePreview} from '../../core/models/store-models';
import {Observable} from 'rxjs';
import {SelectOption} from '../../core/models/select-option';
import {environment} from '../../../environments/environment';
import {UserAuthService} from '../../core/services/user-auth.service';
import {SubscriptionService} from '../../core/services/subscription.service';
import {SearchService} from '../../core/services/search.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {SideNavService} from '../../core/services/side-nav.service';

@Component({
  selector: 'itt-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  // categoryId;
  // pageSize;
  // currentPage;
  // themes;
  //
  // constructor(private route: ActivatedRoute, private storeService: StoreService) {
  //   this.categoryId = route.snapshot.paramMap.get('categoryId');
  //  // this.themes =  this.storeService.getThemesByCategoryPaginate(this.currentPage, this.pageSize, this.categoryId);
  // }
  //
  // ngOnInit() {
  // }


  themes: IThemePreview[] = [];
  topFreePreviews: IThemePreview[] = [];
  topPaidPreviews: IThemePreview[] = [];
  subscribedThemes: Observable<string[]>;
  categoryOptions: SelectOption[];
  nextFree = true;
  nextPaid = true;
  mystyle;
  showSubscribeBtn = true;
  events: string[] = [];
  opened: boolean;
  currentPage =  1;
  currentPageFree = 1;
  currentPagePaid = 1;
  pageSize = 8;
  count;
  categoryId;
  categoryName;
  themeManager = environment.mythemeUrl;

  constructor(
    private authenticationSvc: UserAuthService,
    private subscriptionSvc: SubscriptionService,
    private storeSvc: StoreService,
    private searchSvc: SearchService,
    private router: Router,
    private storeService: StoreService,
    private route: ActivatedRoute
  ) {
    this.categoryId = route.snapshot.paramMap.get('categoryId');
    this.categoryName = route.snapshot.paramMap.get('categoryName');
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    // this.themes = this.storeSvc.getDummyThemePreviewCollection(8);
    this.topFreePreviews = this.storeSvc.getDummyThemePreviewCollection(8);
    this.topPaidPreviews = this.storeSvc.getDummyThemePreviewCollection(8);
    this.loadHomePageModels();
    if (this.authenticationSvc.isLoggedIn) {
      this.subscribedThemes = this.processSubscribedThemes();
    } else {
      this.showSubscribeBtn = true;
    }
  }

  ngOnInit() {

    this.processSubscribedThemes();
    this.setupAuthStateListener();
  }


  trySubscribe(event) {
    this.router.navigate(['subscription', event.id]);
  }

  setupAuthStateListener() {
    this.authenticationSvc.loginState.subscribe(x => this.processSubscribedThemes());
  }


  loadHomePageModels() {
    // this.storeService.getThemesByCategoryPaginate(this.currentPage, this.pageSize, this.categoryId).subscribe(result => {
    //     this.themes = result.data;
    //     this.nextPaid = result.next;
    //     console.log('rest : ', this.themes);
    //   },
    //   error => {
    //
    //   });

    this.storeSvc.getFreeThemeByCategoryPaginate(this.currentPageFree, this.pageSize, this.categoryId).subscribe(result => {
        this.topFreePreviews = result.data;
        this.nextFree = result.next;
      },
      error => {

      });

    this.storeSvc.getPaidThemeByCategoryPaginate(this.currentPagePaid, this.pageSize, this.categoryId).subscribe(result => {
        this.topPaidPreviews = result.data;
        this.nextPaid = result.next;
      },
      error => {

      });
  }


  processSubscribedThemes(): Observable<string[]> {
    return this.subscriptionSvc.getUserThemes();
    // this.subscribedThemes = [];
  }


  topFreeViewMore() {
    this.currentPageFree++;
    this.storeSvc.getPaidThemePaginate(this.currentPage, this.pageSize).subscribe(result => {
      this.themes = result.data;
      this.nextPaid = result.next;
    });
  }
  topPaidViewMore() {
    this.currentPagePaid++;
    this.storeSvc.getPaidThemePaginate(this.currentPage, this.pageSize).subscribe(result => {
      this.themes = result.data;
      this.nextPaid = result.next;
    });
  }

  onSearchInput(value: any) {
    console.log('searched pressed');
    const searchValue = value.searchInput;
    this.router.navigate([`/search/${searchValue}`]);
  }

}
