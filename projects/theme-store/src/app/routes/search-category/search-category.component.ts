import {Component, Input, OnInit} from '@angular/core';
import {PaginationResult} from '../../core/models/pagination-result.model';
import {StoreService} from '../../core/services/store.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'itt-search-category',
  templateUrl: './search-category.component.html',
  styleUrls: ['./search-category.component.css']
})
export class SearchCategoryComponent implements OnInit {

  // category: string;
  // pageSize = 10;
  // currentPage = 1;
  // count: number;
  // categories;
  //
  constructor(private storeSvc: StoreService, private route: ActivatedRoute) {
    // this.category = this.route.snapshot.params.category;
  }

  ngOnInit() {
    // this.loadFirstPage();
  }
  //
  // onPageChanged(page: number) {
  //   this.storeSvc.getThemesByCategoryPaginate(page, this.pageSize, this.category).subscribe((res) => {
  //     this.updatePage(res);
  //   });
  // }
  //
  //
  // private loadFirstPage() {
  //   this.storeSvc.getThemesByCategoryPaginate(this.currentPage, this.pageSize, this.category).subscribe((res) => {
  //     this.updatePage(res);
  //   });
  // }
  //
  //
  // updatePage(resModel: PaginationResult) {
  //   this.categories = resModel.data;
  //   this.count = resModel.count;
  //   this.currentPage = resModel.page;
  // }
}
