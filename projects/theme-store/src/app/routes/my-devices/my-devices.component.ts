import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-my-devices',
  templateUrl: './my-devices.component.html',
  styleUrls: ['./my-devices.component.css']
})
export class MyDevicesComponent implements OnInit {
  isOpened: boolean = false;
  events: string[] = [];
  opened: boolean;
  themeManager = environment.mythemeUrl;

  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
