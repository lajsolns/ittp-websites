import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.css']
})
export class MySubscriptionsComponent implements OnInit {
  isOpened: boolean = false;
  themeManager = environment.mythemeUrl;
  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
