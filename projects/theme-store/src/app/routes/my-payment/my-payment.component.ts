import {Component, OnInit} from '@angular/core';
import {Payment, Wishlist} from "../../core/models/store-models";
import {PaginationResult} from "../../core/models/pagination-result.model";
import {PaymentService} from "../../core/services/payment.service";

@Component({
  selector: 'itt-my-payment',
  templateUrl: './my-payment.component.html',
  styleUrls: ['./my-payment.component.css']
})
export class MyPaymentComponent implements OnInit {

  myPayments: Payment[];
  pageSize = 10;
  currentPage = 1;
  count;

  constructor(private  paymentService: PaymentService) {
  }

  ngOnInit() {
    this.loadFirstPage();

  }

  onPageChanged(page: number) {
    this.paymentService.getMyPaymentsPaginated(page, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  private loadFirstPage() {
    this.paymentService.getMyPaymentsPaginated(this.currentPage, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  updatePage(resModel: PaginationResult) {
    this.myPayments = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
    console.log('payments:', this.myPayments);
  }


}
