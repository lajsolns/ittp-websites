import { Component, OnInit } from '@angular/core';
import {Subscription, Wishlist} from '../../core/models/store-models';
import {PaginationResult} from '../../core/models/pagination-result.model';
import {WishlistService} from '../../core/services/wishlist.service';

@Component({
  selector: 'itt-my-wish-list',
  templateUrl: './my-wish-list.component.html',
  styleUrls: ['./my-wish-list.component.css']
})
export class MyWishListComponent implements OnInit {

  myWishlists: Wishlist[];
  pageSize = 10;
  currentPage = 1;
  count;
  constructor(private  wishlistService: WishlistService) {
  }

  ngOnInit() {
    this.loadFirstPage();

  }

  onPageChanged(page: number) {
    this.wishlistService.getUserWishListPaginated(page, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  private loadFirstPage() {
    this.wishlistService.getUserWishListPaginated(this.currentPage, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  updatePage(resModel: PaginationResult) {
    this.myWishlists = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
  }

}
