import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {passwordValidator} from './validators';
import {UserAuthService} from '../../core/services/user-auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ServerAuthService} from '../../core/services/server-auth.service';
import {environment} from '../../../environments/environment';
import {RouteComponent} from '../../core/route-component';
import {SigninStrategyService} from '../../core/services/signin-strategy.service';
import {SideNavService} from '../../core/services/side-nav.service';

@Component({
  selector: 'itt-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends RouteComponent implements OnInit {
  registerForm: FormGroup;
  returnUrl: string;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;

  constructor(
    private authenticationSvc: UserAuthService,
    private router: Router,
    private serverAuthSvc: ServerAuthService,
    private route: ActivatedRoute,
    private signInStrategySvc: SigninStrategyService,
    private sideNav: SideNavService
  ) {
    super(router, sideNav);
    this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';
    if (authenticationSvc.isLoggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      email: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      confirmPassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6),
        passwordValidator])),
    });
  }

  register(registrationDetails) {
    const email = registrationDetails.email.trim();
    const password = registrationDetails.password.trim();
    const username = registrationDetails.username.trim();

    this
      .signInStrategySvc
      .createFirebaseUser({email, password, username, returnUrl: this.returnUrl});
  }
}
