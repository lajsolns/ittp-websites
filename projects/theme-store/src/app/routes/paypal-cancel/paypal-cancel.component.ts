import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'itt-paypal-cancel',
  templateUrl: './paypal-cancel.component.html',
  styleUrls: ['./paypal-cancel.component.css']
})
export class PaypalCancelComponent implements OnInit {
  isOpened: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
