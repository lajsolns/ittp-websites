import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {NgbModal, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {LoginModalComponent} from '../../core/ui-components/login-modal/login-modal.component';
import {UserAuthService} from '../../core/services/user-auth.service';
import {ActivatedRoute, Router, RouterStateSnapshot} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {StoreService} from '../../core/services/store.service';
import {environment} from '../../../environments/environment';
import {SimilarTheme} from '../../core/models/theme-detail.model';
import {SubscriptionStatus} from '../../core/models/user';
import {AccessComponent} from '../access/access.component';


@Component({
  selector: 'itt-theme-store-detail',
  templateUrl: './theme-store-detail.component.html',
  styleUrls: ['./theme-store-detail.component.css']
})

export class ThemeStoreDetailComponent implements OnInit {
  // testing purpose variable
  isLoggedIn = false;
  averageRating;

  theme;
  subscriptionStatus: SubscriptionStatus;
  similarThemes: SimilarTheme[] = [];
  readonly = true;
  isOpened = false;
  resettable = false;
  innerHeight;
  btnCode;
  events: string[] = [];
  opened: boolean;
  themeManager = environment.mythemeUrl;

  constructor(private toastr: ToastrManager,
              private modalService: NgbModal,
              private authService: UserAuthService,
              private route: ActivatedRoute,
              private config: NgbRatingConfig,
              private storeService: StoreService,
              private router: Router,
  ) {
    this.theme = this.route.snapshot.data['themeDetail'];
    this.subscriptionStatus = this.route.snapshot.data['subscriptionStatus'] || {isSubscribed: false};
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.config.max = 5;
    this.config.resettable = false;

    if (!this.authService.isLoggedIn) {
      this.btnCode = 1;
    } else if (this.authService.isLoggedIn) {
      this.storeService.isOnWishList(this.theme._id).subscribe((res) => {
        if (this.subscriptionStatus.isSubscribed) {
          this.btnCode = 4;
        } else if (res.isOnWishlist === true) {
          this.btnCode = 2;
        } else {
          this.btnCode = 3;
        }
      });
    }

  }


  ngOnInit() {
    this.innerHeight = window.innerHeight;
    this.storeService.getSimilarThemes(this.theme._id).subscribe((res) => {
      this.similarThemes = res;
    }, (err) => {
    });
  }

  tryOpenAddWishListModal() {
    if (this.authService.isLoggedIn) {
      this.tryAddToWishlist(this.theme._id);
    } else {
      this.router.navigate(['/login'], {queryParams: {returnUrl: `store-details/${this.theme._id}`}});

      // this.modalService.open(AccessComponent, {centered: true, size: 'lg'});
    }
  }


  reviewclick() {
    if (this.authService.isLoggedIn) {
      return true;
    } else {
      this.modalService.open(LoginModalComponent, {centered: true});

    }
  }

  //
  // tryOpenSubscribeModal() {
  //   const modalRef = this.modalService.open(LoginModalComponent, { centered: true,  size: 'lg' });
  //   modalRef.componentInstance.tagProperties = this.tagProperties;
  // }

  // tryOpenSubscribeModal() {
  //   if (!this.authService.isAuthenticated()) {
  //     return this.authService.emitLoginRequired(true);
  //   } else if (this.subscriptionStatusCode === 2) {
  //     this.openSubscriptionModal();
  //   } else if (this.subscriptionStatusCode === 0) {
  //     this.openEditSubscriptionModal();
  //   }
  // }


  tryRemoveFromWishlist() {
    this.storeService.removeFromWishList(this.theme._id).subscribe((res) => {
      this.toastr.successToastr('Removed from wishList');
      this.btnCode = 1;
    }, (err) => {
      this.toastr.errorToastr('Something went wrong');
    });
  }

  tryAddToWishlist(themeId: string) {
    this.storeService.addToWhishList(themeId).subscribe((res) => {
      this.toastr.successToastr('Added to wishList');
      this.btnCode = 2;
    }, (err) => {
      this.toastr.errorToastr('something went wrong');
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, {centered: true, size: 'lg'});
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
