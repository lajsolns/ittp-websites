import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'itt-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  themeSearchResults;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.themeSearchResults = this.activatedRoute.snapshot.data['searchInput'];
    console.log('theme-search results', this.themeSearchResults);
  }

  ngOnInit() {
  }

  subscribe(event) {
    this.router.navigate(['subscription', event.id]);
  }

  more() {

  }
}
