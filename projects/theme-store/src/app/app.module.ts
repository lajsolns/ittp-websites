import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AuthServiceConfig, GoogleLoginProvider, SocialLoginModule, FacebookLoginProvider} from 'angular-6-social-login-v2';
import {environment} from '../environments/environment';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ProfileHeaderComponent} from './core/ui-components/profile-header/profile-header.component';
import {RouterModule} from '@angular/router';
import {routes} from './app.routes';
import {MySubscriptionsComponent} from './routes/my-subscriptions/my-subscriptions.component';
import {MyDevicesComponent} from './routes/my-devices/my-devices.component';
import {HomeComponent} from './routes/home/home.component';
import {FooterComponent} from './core/ui-components/footer/footer.component';
import {ThemePreviewComponent} from './core/ui-components/theme-preview/theme-preview.component';
import {NotFound404Component} from './routes/not-found404/not-found404.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {ThemeGridComponent} from './core/ui-components/theme-grid/theme-grid.component';
import {SearchBarComponent} from './core/ui-components/search-bar/search-bar.component';
import {CategorySearchComponent} from './core/ui-components/category-search/category-search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterByPipe} from './pipes/filter-by.pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import {ApiRequestTokenInterceptor} from './core/http-interceptors/api-request-token-interceptor';
import {ChartsModule} from 'ng2-charts';
import {RegisterComponent} from './routes/register/register.component';
import {ThemeStoreDetailComponent} from './routes/theme-store-detail/theme-store-detail.component';
import {ThemeReviewComponent} from './core/ui-components/theme-review/theme-review.component';
import {NgbDropdownModule, NgbModalModule, NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';

import {ErrorResponseInterceptor} from './core/http-interceptors/error-body-response-interceptor';
import {TokenErrorResponseInterceptor} from './core/http-interceptors/token-error-reponse-interceptor';
import {MainHeaderComponent} from './core/ui-components/main-header/main-header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ThemeSubscriptionModalComponent} from './core/ui-components/theme-subscription-modal/theme-subscription-modal.component';
import {LoginModalComponent} from './core/ui-components/login-modal/login-modal.component';
import {NavBtnHeaderComponent} from './core/ui-components/nav-btn-header/nav-btn-header.component';
import {SideNavDetailsComponent} from './core/ui-components/side-nav-details/side-nav-details.component';
import {SidebarModule} from 'ng-sidebar';
import {ProfileComponent} from './core/ui-components/profile/profile.component';
import {SearchComponent} from './routes/search/search.component';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {ColorPickerModule} from 'ngx-color-picker';
import {ToastrModule} from 'ng6-toastr-notifications';
import {SubscriptionComponent} from './routes/subscription/subscription.component';
import {PaypalAprrovalComponent} from './routes/paypal-aprroval/paypal-aprroval.component';
import {PaypalCancelComponent} from './routes/paypal-cancel/paypal-cancel.component';
import {MatProgressBarModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {LoadInterceptor} from './core/services/loading-bar-interceptor';
import {AgmCoreModule} from '@agm/core';
import {MySubcriptionListComponent} from './core/ui-components/my-subcription-list/my-subcription-list.component';
import {MyDeviceListComponent} from './core/ui-components/my-device-list/my-device-list.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {ReviewComponent} from './core/ui-components/review/review.component';
import {FileUploadModule} from 'ng2-file-upload';
import {NavBarComponent} from './core/ui-components/nav-bar/nav-bar.component';
import {AccessComponent} from './routes/access/access.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {SideNavComponent} from './core/ui-components/side-nav/side-nav.component';
import {SimilarThemePreviewComponent} from './core/ui-components/similar-theme-preview/similar-theme-preview.component';
import {SimilarThemeListComponent} from './core/ui-components/similar-theme-list/similar-theme-list.component';
import {ReviewChartComponent} from './core/ui-components/review-chart/review-chart.component';
import {SearchCategoryComponent} from './routes/search-category/search-category.component';
import {SearchGridComponent} from './core/ui-components/search-grid/search-grid.component';
import {CategoryComponent} from './routes/category/category.component';
import {LoginComponent} from './routes/login/login.component';
import {MomentModule} from 'ngx-moment';
import { MyWishListComponent } from './routes/my-wish-list/my-wish-list.component';
import { MyPaymentComponent } from './routes/my-payment/my-payment.component';
import {BlockUIModule} from "ng-block-ui";


const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.googleClientId)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.facebookAppId)
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    ProfileHeaderComponent,
    LoginComponent,
    MySubscriptionsComponent,
    MyDevicesComponent,
    HomeComponent,
    FooterComponent,
    ThemePreviewComponent,
    NotFound404Component,
    // PaymentRedirectComponent,
    ThemeGridComponent,
    SearchBarComponent,
    CategorySearchComponent,
    FilterByPipe,
    RegisterComponent,
    ThemeStoreDetailComponent,
    ThemeReviewComponent,
    MainHeaderComponent,
    NavBtnHeaderComponent,
    SideNavDetailsComponent,
    ThemeSubscriptionModalComponent,
    LoginModalComponent,
    ProfileComponent,
    SearchComponent,
    SubscriptionComponent,
    PaypalAprrovalComponent,
    PaypalCancelComponent,
    MySubcriptionListComponent,
    MyDeviceListComponent,
    ReviewComponent,
    NavBarComponent,
    AccessComponent,
    ReviewChartComponent,
    SideNavComponent,
    SimilarThemePreviewComponent,
    SimilarThemeListComponent,
    SearchGridComponent,
    SearchCategoryComponent,
    CategoryComponent,
    MyWishListComponent,
    MyPaymentComponent,
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MomentModule,
    NgxPaginationModule,
    NgbRatingModule,
    NgbDropdownModule,
    NgbModalModule,
    MultiselectDropdownModule,
    BrowserAnimationsModule,
    MultiselectDropdownModule,
    ColorPickerModule,
    MatProgressBarModule,
    FileUploadModule,
    MatTabsModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTabsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    SlimLoadingBarModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey
    }),
    ToastrModule.forRoot(),
    SidebarModule.forRoot(),
    RouterModule.forRoot(routes, {enableTracing: false}),
    BlockUIModule.forRoot()

  ],
  entryComponents: [ThemeSubscriptionModalComponent, LoginModalComponent, LoginComponent],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    {
      provide: 'API_BASE_URL',
      useValue: environment.apiBaseUrl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiRequestTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenErrorResponseInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorResponseInterceptor,
      multi: true
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadInterceptor,
      multi: true
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
