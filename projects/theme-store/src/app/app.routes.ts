import {AuthGuard} from './core/auth.guard';
import {MySubscriptionsComponent} from './routes/my-subscriptions/my-subscriptions.component';
import {MyDevicesComponent} from './routes/my-devices/my-devices.component';
import {HomeComponent} from './routes/home/home.component';
import {NotFound404Component} from './routes/not-found404/not-found404.component';
import {ThemeStoreDetailComponent} from './routes/theme-store-detail/theme-store-detail.component';
import {ProfileComponent} from './core/ui-components/profile/profile.component';
import {SearchComponent} from './routes/search/search.component';
import {ThemeSearchResolver} from './core/resolvers/theme-search.resolver';
import {ThemeStoreDetailResolver} from './core/resolvers/theme-store-detail-resolver.resolver';

import {SubscriptionComponent} from './routes/subscription/subscription.component';
import {SubscriptionStatusResolver} from './core/resolvers/subscription-status.resolver';
import {PaypalAprrovalComponent} from './routes/paypal-aprroval/paypal-aprroval.component';
import {PaypalCancelComponent} from './routes/paypal-cancel/paypal-cancel.component';
import {ReviewComponent} from './core/ui-components/review/review.component';
import {AccessComponent} from './routes/access/access.component';
import {SearchCategoryComponent} from './routes/search-category/search-category.component';
import {CategoryComponent} from './routes/category/category.component';
import {MyWishListComponent} from './routes/my-wish-list/my-wish-list.component';
import {MyPaymentComponent} from "./routes/my-payment/my-payment.component";
import {LoginComponent} from "./routes/login/login.component";


export const routes = [
  {path: 'login', component: LoginComponent},
  {path: 'access', component: LoginComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'register', component: AccessComponent},
  {path: 'home', component: HomeComponent},
  {path: 'category', component: SearchCategoryComponent},
  {path: '404', component: NotFound404Component},
  {path: 'my-subscriptions', component: MySubscriptionsComponent, canActivate: [AuthGuard]},
  {path: 'my-wishlists', component: MyWishListComponent, canActivate: [AuthGuard]},
  {path: 'my-payments', component: MyPaymentComponent,  canActivate: [AuthGuard]},
  {path: 'my-devices', component: MyDevicesComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'store-details/:themeId',
    component: ThemeStoreDetailComponent,
    resolve: {themeDetail: ThemeStoreDetailResolver, subscriptionStatus: SubscriptionStatusResolver}
  },
  {path: 'search/:searchInput', component: SearchComponent, resolve: {searchInput: ThemeSearchResolver}},
  {path: 'category/:categoryId/:categoryName', component: CategoryComponent},
  {
    path: 'subscription/:themeId',
    component: SubscriptionComponent,
    canActivate: [AuthGuard],
    resolve: {
      themeDetail: ThemeStoreDetailResolver,
      subscriptionStatus: SubscriptionStatusResolver
    }
  },

  {
    path: 'paypal-approval/:themeId',
    component: PaypalAprrovalComponent,
    canActivate: [AuthGuard],
    resolve: {themeDetail: ThemeStoreDetailResolver}
  },

  {
    path: 'paypal-cancel/:themeId',
    component: PaypalCancelComponent,
    canActivate: [AuthGuard],
    resolve: {themeDetail: ThemeStoreDetailResolver}
  },
  {path: 'add-review/:themeId', component: ReviewComponent},

  {path: '**', component: NotFound404Component},
  {path: '**', redirectTo: '/404'}
];
