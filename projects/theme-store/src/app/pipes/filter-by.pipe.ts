import {Pipe, PipeTransform} from '@angular/core';
import { MyThemePreview } from '../core/models/edit-theme-preview.model';

@Pipe({
  name: 'themeFilter',
  pure: false
})
export class FilterByPipe implements PipeTransform {

  transform(editThemes: MyThemePreview[], searchTerm: string): any[] {
    if (!editThemes) {
      return [];
    }
    return editThemes.filter(x => ( x.name.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ));
  }

}
