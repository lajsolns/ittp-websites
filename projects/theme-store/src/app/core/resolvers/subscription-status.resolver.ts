import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SubscriptionStatus} from '../models/user';
import {Observable} from 'rxjs/Observable';
import {SubscriptionService} from '../services/subscription.service';
@Injectable({
  providedIn: 'root'
})
export class SubscriptionStatusResolver implements Resolve<SubscriptionStatus> {
  constructor(private subsSvc: SubscriptionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SubscriptionStatus> {
    const themeId = route.paramMap.get('themeId');
    console.log('resolve second');
    return this.subsSvc.getThemeSubscriptionStatus(themeId);
  }
}
