import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {StoreService} from '../services/store.service';
import {ThemeDetail} from '../models/theme-detail.model';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})

export class ThemeStoreDetailResolver implements Resolve<ThemeDetail> {
  constructor(private storeService: StoreService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<ThemeDetail> {
    const themeId = route.paramMap.get('themeId');
    console.log('resolved first');
    return this.storeService.getThemeDetailById(themeId);

  }
}
