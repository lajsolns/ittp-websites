// import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
// import {Injectable} from '@angular/core';
// import {Observable} from 'rxjs';
// import {ThemeEditService} from '../../routes/my-themes/core/services/theme-edit.service';
// import {ThemeMedia} from '../../routes/my-themes/core/models/theme-models';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class ThemeMediaResolver implements Resolve<ThemeMedia> {
//   constructor(private themeEditSvc: ThemeEditService) {
//   }
//
//   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeMedia> {
//     const themeId = route.paramMap.get('themeId');
//     return this.themeEditSvc.getMediaById(themeId);
//   }
//
// }
