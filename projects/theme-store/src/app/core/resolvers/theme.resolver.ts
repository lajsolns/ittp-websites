// import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
// import {ThemeService} from '../services/theme.service';
// import {Injectable} from '@angular/core';
// import {Observable} from 'rxjs';
// import {ToastrManager} from 'ng6-toastr-notifications';
// import {Theme} from '../../routes/my-themes/core/models/theme-models';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class ThemeResolver implements Resolve<any> {
//   constructor(private themeEditSvc: ThemeEditService, private toastr: ToastrManager) {
//   }
//
//   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Theme> {
//     const themeId = route.paramMap.get('themeId');
//     return  this.themeEditSvc.getThemeById(themeId);
//   }
//
// }
