// import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
// import {Injectable} from '@angular/core';
// import {Observable} from 'rxjs';
// import {ThemeEditService} from '../../routes/my-themes/core/services/theme-edit.service';
// import {ToastrManager} from 'ng6-toastr-notifications';
// import {TagProperty} from '../models/theme.model';
// import {ThemeSettings} from '../../routes/my-themes/core/models/theme-models';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class ThemeSettingsResolver implements Resolve<ThemeSettings> {
//   constructor(private themeEditSvc: ThemeEditService, private toastr: ToastrManager) {
//   }
//
//   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeSettings> {
//     const themeId = route.paramMap.get('themeId');
//     return  this.themeEditSvc.getThemeSettings(themeId);
//   }
//
// }
