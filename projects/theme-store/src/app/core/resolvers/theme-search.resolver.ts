import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {ThemeService} from '../services/theme.service';
import {Injectable} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Observable} from 'rxjs';
import {SearchService} from '../services/search.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeSearchResolver implements Resolve<any> {
  constructor(private searchSvc: SearchService, private toastr: ToastrManager, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const searchInput = route.paramMap.get('searchInput');
    return this.searchSvc.search(searchInput);
  }
}
