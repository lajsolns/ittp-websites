
export interface ThemeStoreDetail {
  _id: string;
  name: string;
  createdBy: string;
  themeImageUrl: string;
  color: string;
  description: string;
  createdAt: string;
  activeSubscribers: string;
  showSubscription: string;
  showIsOnWishlist: string;
  isOnWishlist: string;
  price: string;
  subscriptionStatusCode: string;
  currentRate: string;
  tagCount: string;
}

