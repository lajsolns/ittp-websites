import {defaultThemeTileImage} from './common.models';

export interface IThemePreview {
  _id: string;
  name: string;
  rating: number;
  imageUrl: string;
  price: string;
  isSubscribed: boolean;
}

export class ThemePreview implements IThemePreview {
  _id: string;
  rating = 0;
  imageUrl = defaultThemeTileImage;
  isSubscribed = false;
  name: string;
  price: string;

  priceString(): string {
    return `US$ ${this.price}`;
  }

  shouldShowPrice(): boolean {
    return parseInt(this.price, 10) > 0;
  }
}

export class Subscription {
  id: string;
  imageUrl: string;
  name: string;
  description: string;
}

export class Wishlist {
  id: string;
  imageUrl: string;
  name: string;
  description: string;
}

export class Payment {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  paymentMethod: string;
  billingPlan: string;
  billingAgreement: string;
  price: string;
  currency: string;
}



