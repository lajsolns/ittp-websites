export class ThemeProperties {
  visibleInStore: boolean;
  requiresApproval: boolean;
  allowFastTags: boolean;
  allowCrowdSourcing: boolean;
  allowMovingTags: boolean;
  openToPublic: boolean;
  active: boolean;
}

export class TagProperty {
  name: string;
  normalizedName: string;
  tagPropertyType = 0;
  page: number;
  order = 1;
  mandatory = false;
  showOnMap = false;
  showOnTip = false;
  inherited: boolean;
}
