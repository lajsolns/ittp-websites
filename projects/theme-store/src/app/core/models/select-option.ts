export interface SelectOption {
  name: string;
  description: string;
  values: SelectOptionUnit[];
}

export interface SelectOptionUnit {
  value: number;
  order: number;
  description: string;
  label: string;
}
