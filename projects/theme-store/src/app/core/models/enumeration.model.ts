export class Enumeration {
    name: string;
    values: { value: number, label: string, order: number, description: string }[];
    description: string;
  }
