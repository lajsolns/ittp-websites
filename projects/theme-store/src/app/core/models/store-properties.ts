
export interface StoreProperties {
  region: number[];
  price: 0.0;
  uploadNotes: string;
  currency: number;
  visibleInStore: boolean;
  billingFreq: number;
}
