export interface ThemeDetail {
  _id: string;
  name: string;
  description: string;
  themeImageUrl: string;
  createdBy: string;
  createdAt: string;
  updatedBy: string;
  updatedAt: string;
  price: string;
  color: string;
  averageRating: number;
}


export interface SimilarTheme {
  _id: string;
  name: string;
  description: string;
  themeImageUrl: string;
  createdBy: string;
  price: string;
  rating: number;
}


