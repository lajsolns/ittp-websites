export class PaginationResult {
    page: number;
    count: number;
    data: any[];
  }
