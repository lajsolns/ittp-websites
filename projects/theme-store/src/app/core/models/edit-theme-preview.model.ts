export interface MyThemePreview {
  _id: string;
  name: string;
  tagCount: number;
  description: string;
  themeImageUrl: string;
}
