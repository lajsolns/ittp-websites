import {Router} from '@angular/router';
import {SideNavService} from './services/side-nav.service';

export class RouteComponent {
  currentUrl: string;
  opened = false;

  constructor(router: Router, sideNavSvc: SideNavService) {
    this.currentUrl = router.url;
    sideNavSvc.resetNav();
    sideNavSvc.navState.subscribe(newState => this.opened = newState);
  }
}
