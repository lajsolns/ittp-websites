import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'itt-theme-subscription-modal',
  templateUrl: './theme-subscription-modal.component.html',
  styleUrls: ['./theme-subscription-modal.component.css']
})
export class ThemeSubscriptionModalComponent implements OnInit {
@Input() theme;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
  }

}
