import { Component, OnInit } from '@angular/core';
import {SideNavService} from '../../services/side-nav.service';

@Component({
  selector: 'itt-nav-btn-header',
  templateUrl: './nav-btn-header.component.html',
  styleUrls: ['./nav-btn-header.component.css']
})
export class NavBtnHeaderComponent implements OnInit {

  constructor(private sideNavSvc: SideNavService) { }

  ngOnInit() {
  }

  toggleNav() {
    this.sideNavSvc.toggleNavState();
  }
}
