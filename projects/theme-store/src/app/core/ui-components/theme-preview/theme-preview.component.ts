import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ThemePreview} from '../../models/store-models';
import {ReviewService} from '../../services/review.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'itt-theme-preview',
  templateUrl: './theme-preview.component.html',
  styleUrls: ['./theme-preview.component.css']
})
export class ThemePreviewComponent implements OnInit, OnChanges {
  @Input()
  themePreview: ThemePreview;
  showSubscribeBtn = true;
  @Input()
  subscribedThemes: string[];
  @Output()
  onSubscribedBtnPressed = new EventEmitter();

  selected = 0;
  hovered = 0;
  readonly = true;
  max = 5;
  themeId;

  constructor(private svc: ReviewService, private route: ActivatedRoute) {
    this.themeId = this.route.snapshot.paramMap.get('id');
  }

  ngOnChanges() {
    if (this.subscribedThemes) {
      this.showSubscribeBtn = !this.subscribedThemes.find((x) => x === this.themePreview._id);
    }
  }

  ngOnInit() {
    // this.svc.getReviewById( this.themeId).subscribe(res => {
    //   this.selected = res.rating;
    // });

    // console.log(this.themePreview);
    // console.log(this.showSubscribeBtn+"  "+this.themePreview.isSubscribed);
  }

  tryToSubscribe() {
    if (this.themePreview.price === '0') {
      this.onSubscribedBtnPressed.emit({subscribe: true, id: this.themePreview._id});
    } else {
      this.onSubscribedBtnPressed.emit({subscribe: false, id: this.themePreview._id});
    }
  }

}
