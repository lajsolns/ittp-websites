import {Component, ElementRef, Input, OnInit, Renderer} from '@angular/core';
import {Chart} from 'node_modules/chart.js/';
import {ReviewService} from '../../services/review.service';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-review-chart',
  templateUrl: './review-chart.component.html',
  styleUrls: ['./review-chart.component.css']
})
export class ReviewChartComponent implements OnInit {
  @Input() themeId;

  reviewChart = [];


  constructor(private reviewSvc: ReviewService, private toastr: ToastrManager) {
  }

  ngOnInit() {

    this.reviewSvc.getThemeRatings(this.themeId).subscribe((res) => {
      const {labels, data} = res;

      // res.forEach((result, index) => {
      //   label[index] = result._id;
      //   data[index] = result.count;
      // });

      this.reviewChart = new Chart('canvas', {
        type: 'horizontalBar',
        data: {
          labels,
          datasets: [{
            data,
            backgroundColor: [
              '#ff6f31',
              '#ff9f02',
              '#ffcf02',
              '#9ace6a',
              '#57bb8a',
              '#57bb8a'
            ],
            borderColor: [
              '#ff6f31',
              '#ff9f02',
              '#ffcf02',
              '#9ace6a',
              '#57bb8a',
              '#57bb8a'
            ],
            borderWidth: 0.5
          }]
        },
        options: {
          legend: {
            display: false
          },
          barThickness: 3,
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              },
              gridLines: {
                display: false
              }
            }],
            xAxes: [{
              display: false
            },
            ],
          }
        }
      });
    }, error => {
      this.toastr.errorToastr('error', error);
      console.log('error', error);
    });
  }
}
