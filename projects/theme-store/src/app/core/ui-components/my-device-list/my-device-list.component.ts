import { Component, OnInit } from '@angular/core';
import {SubscriptionService} from '../../services/subscription.service';
import {UserAuthService} from '../../services/user-auth.service';
import {UserService} from '../../services/user.service';
import {PaginationResult} from '../../models/pagination-result.model';

@Component({
  selector: 'itt-my-device-list',
  templateUrl: './my-device-list.component.html',
  styleUrls: ['./my-device-list.component.css']
})
export class MyDeviceListComponent implements OnInit {

  device;
  devices: PaginationResult[] = [];
  pageSize = 10;
  currentPage = 1;
  count;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loadFirstPage();

  }


  onPageChanged(page: number) {
    this.userService.getMyDevicesPaginated(page, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }


  private loadFirstPage() {
    this.userService.getMyDevicesPaginated(this.currentPage, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  updatePage(resModel: PaginationResult) {
    this.devices = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
    console.log('these are the themes my-devices: ', this.devices);
  }

}







