import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectOption} from '../../models/select-option';

@Component({
  selector: 'itt-category-search',
  templateUrl: './category-search.component.html',
  styleUrls: ['./category-search.component.css']
})
export class CategorySearchComponent implements OnInit {
  @Input()
  options: SelectOption[];

  @Output()
  onselected = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
// sort options on initialization
  }

  onSelected($event) {
    const selected = $event.target.value;
    this.onselected.emit(selected);
  }
}
