import {Component, Input, OnInit} from '@angular/core';
import {ThemeStoreDetail} from '../../models/theme-store-detail.model';
import {SimilarTheme} from '../../models/theme-detail.model';
import {Router} from '@angular/router';

@Component({
  selector: 'itt-similar-theme-preview',
  templateUrl: './similar-theme-preview.component.html',
  styleUrls: ['./similar-theme-preview.component.css']
})
export class SimilarThemePreviewComponent implements OnInit {
  @Input()
  theme: SimilarTheme;

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log('similar', this.theme);
  }

  getPrice(price: string) {
    if (price === '0') {
      return 'Free';
    } else {
      return `US $ ${price}`;
    }
  }

  // navigate(themeId) {
  //   this.router.navigate(['store-details', themeId])
  //     .then((url) => {
  //       window.location.reload();
  //     })
  //     .catch(err => {});
  // }
}
