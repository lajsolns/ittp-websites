import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ThemePreview} from '../../models/store-models';
import {Observable} from 'rxjs';

@Component({
  selector: 'itt-search-grid',
  templateUrl: './search-grid.component.html',
  styleUrls: ['./search-grid.component.css']
})
export class SearchGridComponent implements OnInit{
  @Input()
  themePreviews: ThemePreview[];

  @Input()
  title: string;

  @Input()
  next: boolean;

  @Input()
  showSubscribeBtn;


  @Output()
  onviewmore = new EventEmitter();


  @Output()
  SubscribedBtnPressed = new EventEmitter();

  // mysubscribedThemes = [];

  constructor() {
  }

  ngOnInit() {
  }

  // ngOnChanges() {
  //   this.subscribedThemes.subscribe((res) => {
  //     this.mysubscribedThemes = res;
  //
  //   });
  // }


  viewMore() {
    if (this.next) {
      this.onviewmore.emit(true);
    } else {
      this.onviewmore.emit(false);
    }
  }

  submitBtnPressed(event) {
    this.SubscribedBtnPressed.emit(event);
  }

}
