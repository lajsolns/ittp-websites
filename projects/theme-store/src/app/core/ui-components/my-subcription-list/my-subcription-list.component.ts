import {Component, OnInit} from '@angular/core';

import {SubscriptionService} from '../../services/subscription.service';
import {UserAuthService} from '../../services/user-auth.service';
import {PaginationResult} from '../../models/pagination-result.model';
import {Subscription} from '../../models/store-models';

@Component({
  selector: 'itt-my-subcription-list',
  templateUrl: './my-subcription-list.component.html',
  styleUrls: ['./my-subcription-list.component.css']
})
export class MySubcriptionListComponent implements OnInit {

  subscriptions: Subscription[];
  pageSize = 10;
  currentPage = 1;
  count;
  constructor(private  subscriptionService: SubscriptionService, private authService: UserAuthService) {
  }

  ngOnInit() {
    this.loadFirstPage();

  }

  onPageChanged(page: number) {
    this.subscriptionService.getUserSubscriptionPaginated(page, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  private loadFirstPage() {
      this.subscriptionService.getUserSubscriptionPaginated(this.currentPage, this.pageSize).subscribe((res) => {
      this.updatePage(res);
    });
  }

  updatePage(resModel: PaginationResult) {
    this.subscriptions = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
  }
}
