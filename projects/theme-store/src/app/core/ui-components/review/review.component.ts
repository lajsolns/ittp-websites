import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ReviewService} from '../../services/review.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {UserAuthService} from '../../services/user-auth.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'itt-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  addReviewForm: FormGroup;
  themeId;
  name;
  imageUrl;
  numStars = 0;
  hovered = 0;
  readonly = true;
  max = 5;
  resettable = true;
  events: string[] = [];
  opened: boolean;
  userId = '';
  isEdit: boolean;
  rating;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private svc: ReviewService,
              private toast: ToastrManager,
              private router: Router,
              private authService: UserService
  ) {
    this.addReviewForm = this.fb.group({
      message: [''],
      rating: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.themeId = this.route.snapshot.paramMap.get('themeId');
    this.authService.me().subscribe((user => {
      console.log('user', user);
      this.name = user.name;
      this.imageUrl = user.profileImageUrl;
      this.userId = user.email;
      this.svc.getMyReview(this.themeId, this.userId)
        .subscribe((review) => {
          if (!review) {
            review = {themeId: '', message: '', rating: 0};
          } else {
            this.addReviewForm = this.fb.group({
              message: review.message,
              rating: review.rating
            });
            this.numStars = review.rating;
            this.isEdit = true;
          }
        }, error => {
          this.isEdit = false;
        });
    }));

  }

  onSubmit(addReviewForm) {
    if (this.isEdit) {
      addReviewForm.rating = this.numStars;
      const addReview = addReviewForm;
      this.svc.updateReview(addReview, this.themeId, this.userId).subscribe(res => {
        window.location.reload(true);
        this.toast.successToastr('update successfuly');

      }, err => {
        this.toast.errorToastr('couldnt update Review');
      });
    } else {
      addReviewForm.rating = this.numStars;
      const addReview = addReviewForm;
      this.svc.saveReview(addReview, this.themeId, this.name, this.imageUrl, this.userId).subscribe(res => {
        window.location.reload(true);
        this.toast.successToastr('saved successfuly');
      }, err => {
        this.toast.errorToastr('couldnt save Review');
      });
    }
  }
}

export interface Review {
  themeId: string;
  message: string;
  rating: number;
}
