import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ThemePreview} from '../../models/store-models';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs';

@Component({
  selector: 'itt-theme-grid',
  templateUrl: './theme-grid.component.html',
  styleUrls: ['./theme-grid.component.css']
})
export class ThemeGridComponent implements OnInit, OnChanges {
  @Input()
  themePreviews: ThemePreview[];

  @Input()
  subscribedThemes: Observable<string[]>;

  @Input()
  title: string;

  @Input()
  next: boolean;

  @Input()
  showSubscribeBtn;


  @Output()
  onviewmore = new EventEmitter();


  @Output()
  SubscribedBtnPressed = new EventEmitter();

  mysubscribedThemes = [];

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.subscribedThemes) {
      this.subscribedThemes.subscribe((res) => {
        this.mysubscribedThemes = res;

      });
    }

  }


  viewMore() {
    if (this.next) {
      this.onviewmore.emit(true);
    } else {
      this.onviewmore.emit(false);
    }
  }

  submitBtnPressed(event) {
    this.SubscribedBtnPressed.emit(event);
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   if (changes.subscribedThemes) {
  //     const ids = changes.subscribedThemes.currentValue;
  //     this.themePreviews.forEach(x => {
  //       const idx = ids.indexOf(x.id);
  //       x.isSubscribed = (idx !== -1);
  //     });
  //   }
  // }
}
