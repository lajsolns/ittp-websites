import {Component, ElementRef, OnInit, Renderer} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {SelectOptionService} from '../../services/select-option.service';
import {Router} from '@angular/router';

@Component({
  selector: 'itt-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  themeManager = environment.mythemeUrl;

  mySidenav;
  categories;
  categoryLabel = 'Categories';

  constructor(private el: ElementRef, private renderer: Renderer, private selectOptionSvc: SelectOptionService, private router: Router) {
  }

  ngOnInit() {
    this.getCategories();
    this.mySidenav = this.el.nativeElement.querySelector('.sidenav');
  }

  openSideNav() {
    this.renderer.setElementStyle(this.mySidenav, 'width', '250px');
  }

  closeNav() {
    this.renderer.setElementStyle(this.mySidenav, 'width', '0px');
  }

  getCategories() {
    this.selectOptionSvc.getCategories().subscribe(res => {
      this.categories = res;
    }, error => {
      console.log(error);
    });
  }

  showCategory(category) {
    this.router.navigate(['/category', category.value, category.label ])
      .then((res) => {
        this.categoryLabel = category.label;
      });

  }

  showHome() {
    this.router.navigate(['/'])
      .then((res) => {
        this.categoryLabel = 'Categories';
      });
  }
}
