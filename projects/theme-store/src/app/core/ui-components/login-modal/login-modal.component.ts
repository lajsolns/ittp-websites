import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {UserAuthService} from '../../services/user-auth.service';
import {SigninStrategyService} from '../../services/signin-strategy.service';

@Component({
  selector: 'itt-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

  returnUrl: string;
  disableBtns = true;
  loginForm: FormGroup;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;

  constructor(
    public activeModal: NgbActiveModal,
    private router: Router,
    private authenticationSvc: UserAuthService,
    private signinStrategySvc: SigninStrategyService,
    route: ActivatedRoute,
  ) {
    // this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';

    this.returnUrl = router.url;

    if (authenticationSvc.isLoggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.email, Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  signIn(loginCreds) {
    const email = loginCreds.email.trim();
    const password = loginCreds.password.trim();

    this
      .signinStrategySvc
      .signInFirebaseUserWithCreds({email, password, returnUrl: this.returnUrl});
    this.activeModal.close('Close click');
  }

  setTermsAndConditionsAccepted(event) {
    console.log(event);
    this.disableBtns = !(event.target.checked);
  }

}
