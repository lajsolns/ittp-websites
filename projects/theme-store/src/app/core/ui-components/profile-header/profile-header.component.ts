import {Component, Input, OnInit} from '@angular/core';
import {UserAuthService} from '../../services/user-auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {FirebaseUserModel} from '../../models/user';

@Component({
  selector: 'itt-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.css']
})
export class ProfileHeaderComponent implements OnInit {
  @Input()
  returnUrl: string;

  email: string;
  isLoggedIn = false;
  profileImage: string = 'assets/images/ic_profile-pic.png';
  user = new FirebaseUserModel();


  constructor(private authenticationSvc: UserAuthService,
              private router: Router,
              private userSvc: UserService,
              private toastr: ToastrManager) {
  }

  ngOnInit() {
    this.authenticationSvc.currentUser.subscribe(user => {
        if (user) {
          this.email = (user && user.name) || 'Login';
        }
      }
    );

    this.authenticationSvc.loginState.subscribe(isLoggedIn => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn === true) {
        this.userSvc.me().subscribe((user) => {
          this.profileImage = user.profileImageUrl;
        }, (error) => {
          this.profileImage = 'assets/images/ic_profile-pic.png';

        });
      }
    });


  }

  logout() {
    this.authenticationSvc.signOut();
    this.router.navigate(['/']);

  }

  login() {
    this.router.navigate(['/access'], {queryParams: {returnUrl: this.returnUrl}});
  }
}
