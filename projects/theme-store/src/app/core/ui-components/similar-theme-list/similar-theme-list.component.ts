import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'itt-similar-theme-list',
  templateUrl: './similar-theme-list.component.html',
  styleUrls: ['./similar-theme-list.component.css']
})
export class SimilarThemeListComponent implements OnInit {

  @Input()
  similarThemes;
  constructor() { }

  ngOnInit() {
  }

}
