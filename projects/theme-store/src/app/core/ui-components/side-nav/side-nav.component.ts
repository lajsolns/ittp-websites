import {Component, ElementRef, OnInit, Renderer} from '@angular/core';


@Component({
  selector: 'itt-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  mySidenav;

  constructor(private el: ElementRef, private renderer: Renderer) {
  }

  ngOnInit() {
    this.mySidenav = this.el.nativeElement.querySelector('.sidenav');
    this.renderer.setElementStyle(this.mySidenav, 'width', '0');
  }

  openSideNav() {
    this.renderer.setElementStyle(this.mySidenav, 'width', '600px');
  }

  closeNav() {
    this.renderer.setElementStyle(this.mySidenav, 'width', '0px');
  }

}
