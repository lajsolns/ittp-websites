import {Component, Input, OnInit} from '@angular/core';
import {ReviewService} from '../../services/review.service';
import {ActivatedRoute} from '@angular/router';
import {Review} from '../review/review.component';
import {UserAuthService} from '../../services/user-auth.service';

@Component({
  selector: 'itt-theme-review',
  templateUrl: './theme-review.component.html',
  styleUrls: ['./theme-review.component.css']
})
export class ThemeReviewComponent implements OnInit {
  reviews: Review[] = [];
  @Input() themeId;
  pageSize = 10;
  currentPage = 1;
  count = 10;
  imageUrl = 'http://s3.amazonaws.com/nvest/Blank_Club_Website_Avatar_Gray.jpg';
  hovered = 0;
  readonly = true;
  max = 5;
  resettable = true;

  constructor(private svc: ReviewService, private route: ActivatedRoute) {
    // this.themeId = this.route.snapshot.paramMap.get('themeId');
    // console.log('this is the themeId', this.themeId);
  }


  ngOnInit() {
    this.loadReviews();
  }

  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.loadReviews();
  }


  private loadReviews() {
    this
      .svc
      .getMyReviewPaginated(this.currentPage, this.pageSize, this.themeId).subscribe(reviews => {
      this.reviews = reviews.data;
      this.count = reviews.count;
    });
  }
}


