import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'itt-side-nav-details',
  templateUrl: './side-nav-details.component.html',
  styleUrls: ['./side-nav-details.component.css']
})
export class SideNavDetailsComponent implements OnInit {
  mythemesUrl: string;

  constructor() {
    this.mythemesUrl = environment.mythemeUrl;
  }

  ngOnInit() {
  }

}
