import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// import {Observable} from 'rxjs';
import { Observable } from 'rxjs/Observable';
import {UserLoginModel} from './server-auth.service';
import {ThemePreview} from '../models/store-models';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  search(searchInput: string): Observable<ThemePreview[]> {
    const url = `${this.apiBaseUrl}/store/theme-search/?q=${searchInput.toString()}`;
    return this.http.get<ThemePreview[]>(url).pipe(map(res => {
      res = res.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }

}
