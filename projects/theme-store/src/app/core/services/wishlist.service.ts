import {Inject, Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {PaginationResult} from '../models/pagination-result.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserAuthService} from './user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl, private authService: UserAuthService) {
  }

  getUserWishListPaginated(currentPage, pageSize): Observable<PaginationResult> {
    const url = `${this.apiBaseUrl}/store/user-wishlists`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(url, {params: params});
    // return of({
    //   count: 15, data: [{
    //     id: '5a97d02aa53a150004f4b4fc',
    //     name: 'coffe theme',
    //     themeImageUrl: '',
    //     description: 'Take the best coffe wherever you are !'
    //   }],
    //   page: 1
    // });
  }
}
