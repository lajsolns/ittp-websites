import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {UserAuthService} from './user-auth.service';
import {ServerAuthService, UserLoginModel} from './server-auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SigninStrategyService {
  firebaseAuth = firebase.auth;
  returnUrl: string;

  constructor(
    private router: Router,
    private userAuthSvc: UserAuthService,
    private serverAuthSvc: ServerAuthService,
    private afAuth: AngularFireAuth,
    private toastr: ToastrManager
  ) {
  }

  // local(params: LoginParams) {
  //   this.returnUrl = params.returnUrl;
  //   this
  //     .serverAuthSvc
  //     .signinLocal(params.email, params.password)
  //     .subscribe(this.saveLoginAndNavigateToReturnUrl, this.logError);
  // }

  signInFacebook(returnUrl: string) {
    this.returnUrl = returnUrl;
    return this.signinWithProvider(new this.firebaseAuth.FacebookAuthProvider());
  }

  signInTwitter(returnUrl: string) {
    this.returnUrl = returnUrl;
    return this.signinWithProvider(new this.firebaseAuth.TwitterAuthProvider());
  }

  signInGoogle(returnUrl: string) {
    this.returnUrl = returnUrl;
    const provider = new this.firebaseAuth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    return this.signinWithProvider(provider);
  }

  createFirebaseUser(creds: { email: string, password: string, username: string, returnUrl: string }) {
    this.returnUrl = creds.returnUrl;
    firebase
      .auth()
      .createUserWithEmailAndPassword(creds.email, creds.password)
      .then(user => {
        firebase
          .auth()
          .currentUser
          .updateProfile({displayName: creds.username, photoURL: ''});

      })
      .catch(this.logError);
    this.retrieveIdTokenAndExchangeToken();
  }

  signInFirebaseUserWithCreds(creds: { email: string, password: string, returnUrl: string }) {
    this.returnUrl = creds.returnUrl;
    firebase
      .auth()
      .signInWithEmailAndPassword(creds.email, creds.password)
      .catch(this.logError);
    this.retrieveIdTokenAndExchangeToken();
  }

  // private signinWithProvider = (provider) => {
  //   this.afAuth.auth
  //     .signInWithPopup(provider)
  //     .then((succ) => {
  //         console.log('inside signinwithpopup:');
  //         this.router.navigate([this.returnUrl]);
  //         // this.retrieveIdTokenAndExchangeToken();
  //       }
  //       , (err) => {
  //         this.logError(err);
  //       });
  // };

  // private signinWithProvider = (provider): Promise<any> => {
  //   return this.afAuth.auth
  //     .signInWithPopup(provider)
  //     .then((res) => {
  //         this.retrieveIdTokenAndExchangeToken().then((r) => {
  //           return r;
  //         });
  //         return this.retrieveIdTokenAndExchangeToken();
  //       }, (err) => this.logError(err)
  //     );
  // };

  private signinWithProvider = (provider): Promise<any> => {
    provider.setCustomParameters({
      state: '345'
    });
    return this.afAuth.auth
      .signInWithRedirect(provider);
      // .then((res) => {
      //   console.log('res', res);
      //     this.retrieveIdTokenAndExchangeToken().then((r) => {
      //       return r;
      //     });
      //     return this.retrieveIdTokenAndExchangeToken();
      //   }, (err) => {
      //     this.logError(err);
      //   }
      // );
  };


  private getFirebaseUserIdToken = () => {
    return firebase
      .auth()
      .currentUser
      .getIdToken(true);
  };

  // private getFirebaseUserIdToken = () => {
  //   return firebase
  //     .auth()
  //     .getRedirectResult()
  // };

  retrieveIdTokenAndExchangeToken = (): Promise<any> => {
    return this
      .getFirebaseUserIdToken()
      .then(idToken => {
        return this
          .serverAuthSvc
          .signinFirebase(idToken);
        // .subscribe(this.saveLoginAndNavigateToReturnUrl, this.logError);
        // .subscribe((result) => {
        //   console.log('userinfo', result);
        //   return result;
        // }, this.logError);
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  saveLoginAndNavigateToReturnUrl = (loginModel: UserLoginModel) => {
    this.userAuthSvc.signIn(loginModel);
    // this.router.navigate([this.returnUrl]);
    window.location.href = this.returnUrl;
  };

  private logError = (err) => {
    console.log(err);
    this.toastr.errorToastr('Login Error', err);
  };

}


export interface LoginParams {
  email: string;
  password: string;
  returnUrl: string;
}
