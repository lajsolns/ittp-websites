import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {UserLoginModel} from './server-auth.service';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {
  currentUser: Observable<UserLoginModel>;
  loginState: Observable<boolean>;

  private currentUserSubject: BehaviorSubject<UserLoginModel>;
  private loginStateSubject: BehaviorSubject<boolean>;
  private KEY_USER = 'KEY_USER';

  constructor() {
    const user = JSON.parse(localStorage.getItem(this.KEY_USER));

    this.currentUserSubject = new BehaviorSubject<UserLoginModel>(user);
    this.loginStateSubject = new BehaviorSubject<boolean>(!!user);

    this.currentUser = this.currentUserSubject.asObservable();
    this.loginState = this.loginStateSubject.asObservable();
  }

  get currentUserValue(): UserLoginModel {
    return this.currentUserSubject.value;
  }

  get isLoggedIn(): boolean {
    return this.loginStateSubject.value;
  }

  signIn(userLoginModel: UserLoginModel) {
    localStorage.setItem(this.KEY_USER, JSON.stringify(userLoginModel));
    console.log('inside sign in:', this.KEY_USER);

    this.currentUserSubject.next(userLoginModel);
    this.loginStateSubject.next(true);
  }

  signOut() {
    localStorage.removeItem(this.KEY_USER);
    this.currentUserSubject.next(null);
    this.loginStateSubject.next(false);
    // location.reload(true);
  }
}
