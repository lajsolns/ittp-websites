import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {UserAuthService} from "./user-auth.service";
import {Observable, of} from "rxjs";
import {PaginationResult} from "../models/pagination-result.model";

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getMyPaymentsPaginated(currentPage, pageSize): Observable<PaginationResult> {
    const url = `${this.apiBaseUrl}/store/user-payments`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(url, {params: params});
    // return of({
    //   count: 15, data: [{
    //     id: '5a97d02aa53a150004f4b4fc',
    //     firstName: 'Atsu',
    //     lastName: 'Adzadogo',
    //     email: 'adzadogo@gmail.com',
    //     paymentMethod: 'paypal',
    //     billingPlan: 'Monthly',
    //     billingAgreement: 'Monthly',
    //     price: '30',
    //     currency: 'dollars',
    //   }],
    //   page: 1
    // });
  }
}

