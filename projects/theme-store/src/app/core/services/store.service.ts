import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ThemePreview} from '../models/store-models';
import {SelectOption} from '../models/select-option';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/operators';
import {SimilarTheme, ThemeDetail} from '../models/theme-detail.model';
import {of} from 'rxjs/internal/observable/of';
import {PaginationResult} from '../models/pagination-result.model';
import {ThemeStoreDetail} from '../models/theme-store-detail.model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getHomePageModels(): Observable<HomePageModels> {
    const url = `${this.apiBaseUrl}/store/home-page-models`;
    return this.http.get<HomePageModels>(url).pipe(map(res => {
      res.topPaid = res.topPaid.map(x => Object.assign(new ThemePreview(), x));
      res.topFree = res.topFree.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }

  getSimilarThemes(themeId): Observable<SimilarTheme[]> {
    const date = new Date();
    const today = date.getDate().toString();
    const url = `${this.apiBaseUrl}/store/similar-themes/${themeId}`;
    return this.http.get<SimilarTheme[]>(url);
    // return of([{
    //   _id: '',
    //   name: 'Accra Parks',
    //   description: 'This theme gives all the park and playground available in accra',
    //   themeImageUrl: 'https://cdn.vox-cdn.com/thumbor/1i4TW0_PBv9UOO03Ek1HaQW809I=/0x0:2460x1640/1200x900/filters:focal(1034x624:1426x1016)/cdn.vox-cdn.com/uploads/chorus_image/image/55997615/shutterstock_384332770.0.jpg',
    //   createdBy: 'Better life Inc.',
    //   createdAt: today,
    //   updatedBy: 'Better life Inc.',
    //   updatedAt: today,
    //   price: 'free',
    //   color: '#4387fd',
    //   currentRate: 3,
    // },
    //   {
    //     _id: '',
    //     name: 'Accra Parks',
    //     description: 'This theme gives all the park and playground available in accra',
    //     themeImageUrl: 'https://cdn.vox-cdn.com/thumbor/1i4TW0_PBv9UOO03Ek1HaQW809I=/0x0:2460x1640/1200x900/filters:focal(1034x624:1426x1016)/cdn.vox-cdn.com/uploads/chorus_image/image/55997615/shutterstock_384332770.0.jpg',
    //     createdBy: 'Better life Inc.',
    //     createdAt: today,
    //     updatedBy: 'Better life Inc.',
    //     updatedAt: today,
    //     price: 'free',
    //     color: '#4387fd',
    //     currentRate: 4,
    //   },
    //   {
    //     _id: '',
    //     name: 'Accra Parks',
    //     description: 'This theme gives all the park and playground available in accra',
    //     themeImageUrl: 'https://cdn.vox-cdn.com/thumbor/1i4TW0_PBv9UOO03Ek1HaQW809I=/0x0:2460x1640/1200x900/filters:focal(1034x624:1426x1016)/cdn.vox-cdn.com/uploads/chorus_image/image/55997615/shutterstock_384332770.0.jpg',
    //     createdBy: 'Better life Inc.',
    //     createdAt: today,
    //     updatedBy: 'Better life Inc.',
    //     updatedAt: today,
    //     price: 'free',
    //     color: '#4387fd',
    //     currentRate: 4,
    //   }]);
  }

  getThemeDetailById(themeId: string): Observable<ThemeDetail> {
    const url = `${this.apiBaseUrl}/store/theme-detail/${themeId}`;
    return this.http.get<ThemeDetail>(url);
  }

  addToWhishList(themeId): Observable<string> {
    const url = `${this.apiBaseUrl}/store/wishlist/add/${themeId}`;
    return this.http.post<string>(url, themeId);
  }

  removeFromWishList(themeId): Observable<string> {
    const url = `${this.apiBaseUrl}/store/wishlist/delete/${themeId}`;
    return this.http.post<string>(url, themeId);
  }


  getDummyThemePreview() {
    return {
      _id: '',
      averageRating: 0,
      imageUrl: 'assets/images/placeholder_theme.jpg',
      isSubscribed: false,
      name: '',
      price: '',

      priceString(): string {
        return ``;
      },

      shouldShowPrice(): boolean {
        return true;
      }
    };
  }

  getDummyThemePreviewCollection(count) {
    const collection = [];
    for (let i = 1; i <= count; i++) {
      collection.push(this.getDummyThemePreview());
    }
    return collection;
  }

  isOnWishList(themeId): Observable<{ isOnWishlist: boolean }> {
    const url = `${this.apiBaseUrl}/store/wishlist/check/${themeId}`;
    return this.http.get<{ isOnWishlist: boolean }>(url);

  }

  getFreeThemePaginate(currentPage, pageSize): Observable<{ data: any[], next: boolean }> {
    const url = `${this.apiBaseUrl}/store/top-free/`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<{ data: any[], next: boolean }>(url, {params: params}).pipe(map(res => {
      res.data = res.data.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));

  }

  getPaidThemePaginate(currentPage, pageSize): Observable<{ data: any[], next: boolean }> {

    const url = `${this.apiBaseUrl}/store/top-paid/`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<{ data: any[], next: boolean }>(url, {params: params}).pipe(map(res => {
      res.data = res.data.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }


  getThemesByCategoryPaginate(currentPage, pageSize, category): Observable<{ data: any[], next: boolean }> {
    const url = `${this.apiBaseUrl}/store/category/${category}`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<{ data: any[], next: boolean }>(url, {params: params}).pipe(map(res => {
      res.data = res.data.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }

  getPaidThemeByCategoryPaginate(currentPage: any, pageSize: number, categoryId: any): Observable<{ data: any[], next: boolean }> {
    const url = `${this.apiBaseUrl}/store/category/${categoryId}/paid`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<{ data: any[], next: boolean }>(url, {params: params}).pipe(map(res => {
      res.data = res.data.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }

  getFreeThemeByCategoryPaginate(currentPage: number, pageSize: number, categoryId: string): Observable<{ data: any[], next: boolean }> {
    const url = `${this.apiBaseUrl}/store/category/${categoryId}/free`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<{ data: any[], next: boolean }>(url, {params: params}).pipe(map(res => {
      res.data = res.data.map(x => Object.assign(new ThemePreview(), x));
      return res;
    }));
  }
}


export interface HomePageModels {
  topPaid: ThemePreview[];
  topFree: ThemePreview[];
  categories: SelectOption[];
}


