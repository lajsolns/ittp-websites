import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {SelectOption} from '../models/select-option';

@Injectable({
  providedIn: 'root'
})
export class SelectOptionService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) { }

  getCategories(): Observable<SelectOption[]> {
    const url = `${this.apiBaseUrl}/select-option`;
    const params = new HttpParams().set('q', 'category');
    return this.http.get<SelectOption[]>(url, { params: params });
  }
}
