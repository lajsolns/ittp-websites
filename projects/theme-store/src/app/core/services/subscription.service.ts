import {Inject, Injectable} from '@angular/core';
import {IUser, SubscriptionStatus} from '../models/user';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {IThemePreview} from '../models/store-models';
import {environment} from '../../../environments/environment';
import {of} from 'rxjs/internal/observable/of';
import {PaginationResult} from '../models/pagination-result.model';
import {AuthService} from 'angular-6-social-login-v2';
import {UserAuthService} from './user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl, private authService: UserAuthService) {
  }

  createPaypalBillingAgreement(themeId): Observable<PaypalBillingAgreementResponse> {
    const url = `${this.apiBaseUrl}/store/subscription/create-billing-agreement/${themeId}`;
    return this.http.post<PaypalBillingAgreementResponse>(url, {});
  }

  getUserThemes(): Observable<string[]> {
    const url = `${this.apiBaseUrl}/store/subscription/user-themes`;
    return this.http.get<string[]>(url);
  }

  getThemeSubscriptionStatus(themeId: string): Observable<SubscriptionStatus> {
    if (this.authService.isLoggedIn) {
      const url = `${this.apiBaseUrl}/store/subscription-status/${themeId}`;
      return this.http.get<SubscriptionStatus>(url);
    } else {
      return ;
    }
  }

  executeBillingAgreement(themeId: string, token: string): Observable<any> {
    const url = `${this.apiBaseUrl}/store/subscription/execute-billing-agreement/${themeId}`;
    return this.http.post<any>(url, {token});
  }

  subscribeToTheme(themeId: string): Observable<any> {
    const url = `${this.apiBaseUrl}/store/subscription/subscribe/${themeId}`;
    return this.http.post<any>(url, {});
  }

  unsubscribe(themeId: string): Observable<any> {
    const url = `${this.apiBaseUrl}/subscription/theme/unsubscribe/${themeId}`;
    return this.http.post<any>(url, {});
  }

  getUserSubscriptionPaginated(currentPage, pageSize): Observable<PaginationResult> {
    const url = `${this.apiBaseUrl}/store/user-subscriptions` ;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(url, {params: params});
    // return of({count: 15, data:[{id:'5a97d02aa53a150004f4b4fc', name: 'coffe theme', themeImageUrl: '', description: 'Take the best coffe wherever you are !' }], page:1});
  }
}

export interface PaypalBillingAgreementResponse {
  approvalUrl: string;
}
