// import {AdminUsersListComponent} from './ui-components/user/admin-users-list/admin-users-list.component';
import {HomeComponent} from './ui-components/home/home.component';
import {ThemesComponent} from './routes/themes/themes.component';
import {LoginComponent} from './ui-components/login/login.component';
import {UsersListComponent} from './ui-components/user/users-list/users-list.component';
import {ErrorLogComponent} from './ui-components/log/error-log/error-log.component';
import {OperationLogComponent} from './ui-components/log/operation-log/operation-log.component';
import {AdminUsersListComponent} from './ui-components/user/admin-users-list/admin-users-list.component';
import {AuthGuard} from './core/auth.guard';

import {UsersDetailsComponent} from './ui-components/user/users-details/users-details.component';
import {UserLocationComponent} from './ui-components/user/user-location/user-location.component';
import {CategoriesComponent} from './ui-components/configuration/categories/categories.component';
import {RegionsComponent} from './ui-components/configuration/regions/regions.component';
import {RegionResolver} from './resolvers/region.resolver';
import {CategoryResolver} from './resolvers/category.resolver';
import {CategoryEditComponent} from './ui-components/configuration/category-edit/category-edit.component';
import {RegionEditComponent} from './ui-components/configuration/region-edit/region-edit.component';
import {UserDetailsResolver} from './resolvers/user-details.resolver';
import {ProfileComponent} from './ui-components/profile/profile.component';
import {AccessComponent} from './ui-components/access/access.component';
import {SearchComponent} from './ui-components/search/search.component';
import {UserSearchResolver} from './services/user-search.resolver';
import {AdminSearchComponent} from './ui-components/admin-search/admin-search.component';
import {AdminUserSearchResolver} from './services/admin-user-search.resolver';
import {NotFound404Component} from './ui-components/not-found404/not-found404.component';


export const routes = [
  {path: 'admin-users', component: AdminUsersListComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersListComponent, canActivate: [AuthGuard]},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/home', pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'access', component: AccessComponent},
  {path: 'error-logs', component: ErrorLogComponent, canActivate: [AuthGuard]},
  {path: 'operation-logs', component: OperationLogComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'regions', component: RegionsComponent, canActivate: [AuthGuard]},
  {path: 'categories', component: CategoriesComponent, canActivate: [AuthGuard]},
  {path: 'search/:searchInput', component: SearchComponent, resolve: {searchInput: UserSearchResolver}},
  {path: 'search-admin/:searchInput', component: AdminSearchComponent, resolve: {searchInput: AdminUserSearchResolver}},
  {path: '404', component: NotFound404Component},
  {
    path: 'categories/:categoryId/categories-edit',
    component: CategoryEditComponent,
    resolve: {category: CategoryResolver},
    canActivate: [AuthGuard]
  },
  {path: 'regions/:regionId/region-edit', component: RegionEditComponent, resolve: {region: RegionResolver}, canActivate: [AuthGuard]},

  {
    path: 'users/:userId/devices/:deviceId/locations', component: UserLocationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'users/:userId/details', component: UsersDetailsComponent,
    resolve: {
      userDetails: UserDetailsResolver
    },
    canActivate: [AuthGuard]
  },

  {path: '**', component: NotFound404Component},
  {path: '**', redirectTo: '/404'},

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },

];
