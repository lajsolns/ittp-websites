import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeviceInfo} from '../models/themes.model';
import {IUser} from '../../../../theme-store/src/app/core/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  me(): Observable<IUser> {
    const url = `${this.apiBaseUrl}/user/me`;
    return this.http.get<IUser>(url);
  }
}
