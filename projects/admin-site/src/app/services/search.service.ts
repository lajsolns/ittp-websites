import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserDetails} from '../models/themes.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  search(searchInput: string): Observable<any[]> {
    const url = `${this.apiBaseUrl}/user/search-user`;
    return this.http.post<any>(url, {searchInput})
      .pipe(map(res => {
        res = res.map(x => Object.assign(new UserDetails(), x));
        return res;
      }));
  }

  searchAdmin(searchInput: string): Observable<any[]> {
    const url = `${this.apiBaseUrl}/user/search-adminuser`;
    return this.http.post<any>(url, {searchInput})
      .pipe(map(res => {
        res = res.map(x => Object.assign(new UserDetails(), x));
        return res;
      }));
  }
}
