import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SearchService} from '../services/search.service';

@Injectable({
  providedIn: 'root'
})
export class AdminUserSearchResolver implements Resolve<any> {
  constructor(private searchSvc: SearchService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const searchInput = route.paramMap.get('searchInput');
    return this.searchSvc.searchAdmin(searchInput);
  }
}
