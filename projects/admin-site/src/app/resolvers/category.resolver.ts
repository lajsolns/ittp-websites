import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SelectOptionService} from '../core/services/select-option.service';
import {SelectOption} from '../core/models/select-option-models';


@Injectable({
  providedIn: 'root'
})
export class CategoryResolver implements Resolve<SelectOption> {
  constructor(private userSvc: SelectOptionService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<SelectOption> {
    const categoryId = route.paramMap.get('categoryId');
    return this.userSvc.getCategoryById(categoryId);
  }

}
