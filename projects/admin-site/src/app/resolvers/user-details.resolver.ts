import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserDetails, UserInfo} from '../models/themes.model';
import {UserService} from '../ui-components/user/users-details/user-details.service';


@Injectable({
  providedIn: 'root'
})
export class UserDetailsResolver implements Resolve<UserDetails> {
  userId;
  constructor(private userSvc: UserService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserDetails> {
    this.userId = route.paramMap.get('userId');
    return  this.userSvc.getUserDetails(this.userId);
  }
}
