import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SelectOptionService} from '../core/services/select-option.service';
import {SelectOption} from '../core/models/select-option-models';


@Injectable({
  providedIn: 'root'
})
export class RegionResolver implements Resolve<SelectOption> {
  constructor(private userSvc: SelectOptionService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SelectOption> {
    const regionId = route.paramMap.get('regionId');
    return this.userSvc.getRegionById(regionId);
  }

}

