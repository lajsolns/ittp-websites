export class DeviceLocation {
  location: DeviceProperties[];
}

class DeviceProperties {
  _id: string;
  speed: number;
  latitude: number;
  longitude: number;
  acquired: number;
  isOnline: boolean;
}
