export class UserInfo {
  _id: string;
  email: string;
  profileImageUrl: string;
  username: string;
}

export class DeviceInfo {
  _id: string;
  hardwareId: string;
  operatingSystem: string;
  manufacturer: string;
  model: string;
  operatingSystemVersion: string;
  installDate: string;
}

export class SubscriptionsInfo {
  id: string;
  imageUrl: string;
  name: string;
}

export class OwnedThemeInfo {
  imageUrl: string;
  name: string;
}

export class UserDescription {
  id: string;
  email: string;
  username: string;
}

export class UserDetails {
  id: string;
  name: string;
  email: string;
  profileImageUrl: string;
  isAdmin: boolean;
  devices: [DeviceInfo];
  subscriptions: [SubscriptionsInfo];
  themes: [OwnedThemeInfo];
}




