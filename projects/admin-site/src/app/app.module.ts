import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarComponent} from './ui-components/navbar/navbar.component';
import {RouterModule} from '@angular/router';

import {ThemesComponent} from './routes/themes/themes.component';
import {routes} from './app.routes';
import {HomeComponent} from './ui-components/home/home.component';
// import {ToastrModule} from 'ng6-toastr-notifications';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {ToastrModule} from '../../../../node_modules/ng6-toastr-notifications';
import {ErrorLogComponent} from './ui-components/log/error-log/error-log.component';
import {OperationLogComponent} from './ui-components/log/operation-log/operation-log.component';
import {environment} from '../environments/environment';
import {LoginComponent} from './ui-components/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ChartsModule} from 'ng2-charts';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterByPipe} from './core/pipes/filter-by.pipe';
import {UsersListComponent} from './ui-components/user/users-list/users-list.component';
import {AdminUsersListComponent} from './ui-components/user/admin-users-list/admin-users-list.component';
// import {HTTP_INTERCEPTORS} from '@angular/common/http';
// import {ApiRequestTokenInterceptor} from '../../../tagProperties-store/src/app/core/http-interceptors/api-request-token-interceptor';
// import {TokenErrorResponseInterceptor} from '../../../tagProperties-store/src/app/core/http-interceptors/token-error-reponse-interceptor';
// import {ErrorResponseInterceptor} from '../../../tagProperties-store/src/app/core/http-interceptors/error-body-response-interceptor';
import {UsersDetailsComponent} from './ui-components/user/users-details/users-details.component';
import {CategoriesComponent} from './ui-components/configuration/categories/categories.component';
import {RegionsComponent} from './ui-components/configuration/regions/regions.component';
import {UserLocationComponent} from './ui-components/user/user-location/user-location.component';
import {AgmCoreModule} from '@agm/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {DpDatePickerModule} from 'ng2-date-picker';
import {UserSelectionEditorComponent} from './ui-components/configuration/user-selection-editor/user-selection-editor.component';
import {CategoryEditComponent} from './ui-components/configuration/category-edit/category-edit.component';
import {RegionEditComponent} from './ui-components/configuration/region-edit/region-edit.component';
import {ErrorResponseInterceptor} from './core/http-interceptors/error-body-response-interceptor';
import {TokenErrorResponseInterceptor} from './core/http-interceptors/token-error-reponse-interceptor';
import {ApiRequestTokenInterceptor} from './core/http-interceptors/api-request-token-interceptor';
import {ProfileComponent} from './ui-components/profile/profile.component';
import {FooterComponent} from './ui-components/footer/footer.component';
import {LoadInterceptor} from './core/services/loading-bar-interceptor';
import {ProfileHeaderComponent} from './ui-components/profile-header/profile-header.component';
import {SearchBarComponent} from './ui-components/search-bar/search-bar.component';
import {AccessComponent} from './ui-components/access/access.component';
import {MatTabsModule} from '@angular/material';
import {SearchComponent} from './ui-components/search/search.component';
import {AdminSearchComponent} from './ui-components/admin-search/admin-search.component';
import {NotFound404Component} from './ui-components/not-found404/not-found404.component';
import { RemoveModalComponent } from './ui-components/remove-modal/remove-modal.component';
import {NgbModal, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

// import {ProfileLibComponent} from 'profile-lib/src/lib/profile-lib.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    ThemesComponent,
    FilterByPipe,
    HomeComponent,
    AdminUsersListComponent,
    ErrorLogComponent,
    OperationLogComponent,
    UsersListComponent,
    UsersDetailsComponent,
    UserLocationComponent,
    CategoriesComponent,
    RegionsComponent,
    UserSelectionEditorComponent,
    CategoryEditComponent,
    RegionEditComponent,
    ProfileComponent,
    FooterComponent,
    ProfileHeaderComponent,
    SearchBarComponent,
    AccessComponent,
    SearchComponent,
    AdminSearchComponent,
    NotFound404Component,
    RemoveModalComponent

  ],
  imports: [
    BrowserModule,
    RouterModule,
    BrowserAnimationsModule,
    NgbModalModule,
    ChartsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SlimLoadingBarModule.forRoot(),
    FormsModule,
    HttpClientModule,
    DpDatePickerModule,
    MatTabsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey
    }),
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(routes, {enableTracing: false}),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features


  ],
   entryComponents: [RemoveModalComponent],
  providers: [
    {
      provide: 'API_BASE_URL',
      useValue: environment.apiUrl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadInterceptor,
      multi: true
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiRequestTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenErrorResponseInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorResponseInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
