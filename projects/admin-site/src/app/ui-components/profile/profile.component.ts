import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {UserService} from '../../services/user.service';
import {UserAuthService} from '../../../../../theme-store/src/app/core/services/user-auth.service';

@Component({
  selector: 'itt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input()
  returnUrl: string;

  name: string;
  profileImageUrl: string = 'assets/images/ic_profile-pic.png';
  isLoggedIn = false;
  isOpened: boolean = false;


  constructor(private authenticationSvc: UserAuthService,
              private router: Router,
              private userSvc: UserService,
              private toastr: ToastrManager) {
  }

  ngOnInit() {
    this.authenticationSvc.currentUser.subscribe(user => {
      this.name = (user && user.name) || 'Login';
    });

    this.authenticationSvc.loginState.subscribe(isLoggedIn => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn === true) {
        this.userSvc.me().subscribe((user) => {
          this.profileImageUrl = user.profileImageUrl;
        }, error => {
          this.profileImageUrl = 'assets/images/ic_profile-pic.png';
        });
      }
    });
  }

  logout() {
    this.authenticationSvc.signOut();
    this.router.navigate(['/']);

  }

  login() {
    this.router.navigate(['/access']);
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
