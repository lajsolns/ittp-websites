import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'itt-remove-modal',
  templateUrl: './remove-modal.component.html',
  styleUrls: ['./remove-modal.component.css']
})
export class RemoveModalComponent implements OnInit {
  @Input() username;
  @Output() remove = new EventEmitter();
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  removeAdmin() {
    this.remove.emit(true);
    this.activeModal.close();
  }

  cancel() {
    this.activeModal.dismiss();
  }

}
