import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';
import {ToastrManager} from 'ng6-toastr-notifications';
import {UserService} from '../../../core/services/user.service';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RemoveModalComponent} from '../../remove-modal/remove-modal.component';


@Component({
  selector: 'itt-admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.css']
})
export class AdminUsersListComponent implements OnInit {
  adminUsers = [];
  users = [];
  searchTermUser = '';
  apiUrl;
  pageSize = 10;
  currentPage = 1;
  count = 10;

  constructor(private sanitization: DomSanitizer,
              private _userService: UserService,
              private toastr: ToastrManager,
              private router: Router,
              private modalService: NgbModal

  ) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit() {
    this.loadAdminUsersPaginate();
    // this.loadUsers();
  }

  loadAdminUsersPaginate() {
    this._userService.getAdminUsersPaginate(this.currentPage, this.pageSize).subscribe(adminUsers => {
      this.adminUsers = adminUsers.data;
      this.count = adminUsers.count;
    });
  }

  //
  // loadUsers() {
  //   this._userService.getUsers().subscribe(users => {
  //     this.users = users;
  //   });
  // }

  tryRemovingAdmin(id, index, username) {
    const modalRef = this.modalService.open(RemoveModalComponent);
    modalRef.componentInstance.username = username;
    modalRef.componentInstance.remove.subscribe( res => this.removeUserFromAdmin(id, index));
  }

  removeUserFromAdmin(id, index) {
    this._userService.removeUserFromAdmin(id).subscribe(res => {
        this.toastr.successToastr('Removed Admin!', 'Success!');
        this.adminUsers.splice(index, 1);
        this.loadAdminUsersPaginate();
      },
      err => {
        this.toastr.errorToastr('Error removing Admin!', 'Error!');
      });

  }

  makeAdmin(id) {
    this._userService.makeUserAdmin(id).subscribe(res => {
        this.loadAdminUsersPaginate();
        this.toastr.successToastr('Admin Added!', 'Success!');
      },
      err => {
        this.toastr.errorToastr('Error adding Admin!', err);
      });
  }

  searchForAdmin(value) {
    const searchTermAdmin = value.searchInput;
    this.router.navigate([`/search-admin/${searchTermAdmin}`]);
  }

  searchForUser(searchTermUser) {
    this.searchTermUser = searchTermUser.searchInput;
    console.log(this.searchTermUser);
    this._userService.searchUser(this.searchTermUser).subscribe(users => {
        this.users = users;
      },
      err => {
        this.toastr.errorToastr('user theme-search failed', err);
      });
  }

  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.loadAdminUsersPaginate();
  }


}
