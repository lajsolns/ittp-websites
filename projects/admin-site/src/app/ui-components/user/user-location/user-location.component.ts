import {Component, ElementRef, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../../core/services/user.service';
import {ActivatedRoute} from '@angular/router';
import {DeviceLocationService} from '../../../core/services/device-location.service';
import {DeviceLocation} from '../../../models/device-location.model';

@Component({
  selector: 'itt-user-location',
  templateUrl: './user-location.component.html',
  styleUrls: ['./user-location.component.css']
})
export class UserLocationComponent implements OnInit {
  startDateConfig;
  endDateConfig;
  startDate: string;
  endDate: string;
  locations = [];
  count: number = 10;
  currentPage = 1;
  pageSize = 500;
  lat: number;
  lng: number;

  userId: string;
  deviceId: string;
  selectedRowItem;
  dateForm: FormGroup;

  constructor(private userService: UserService, activatedRoute: ActivatedRoute,
              private tagElement: ElementRef, private deviceLocationSvc: DeviceLocationService) {
    this.deviceId = activatedRoute.snapshot.params['deviceId'];
    this.userId = activatedRoute.snapshot.params['userId'];
    this.initForm();
    this.initializeDatePickerConfigs();
  }

  ngOnInit() {
    this.getIitialDate();
    this.getInitialDeviceLocationsPaginate(this.startDate, this.endDate);
  }

  initForm() {
    const currentDate = new Date();
    this.dateForm = new FormGroup({
      endDate: new FormControl(`${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()} ${currentDate.getHours()}:${currentDate.getMinutes()}`),
      startDate: new FormControl(`${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate() - 1} ${currentDate.getHours()}:${currentDate.getMinutes()}`)
    });
  }

  getIitialDate() {
    const currentDate = new Date();
    this.endDate = new Date(`${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()} ${currentDate.getHours()}:${currentDate.getMinutes()}`).getTime().toString();
    this.startDate = new Date(`${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate() - 1} ${currentDate.getHours()}:${currentDate.getMinutes()}`).getTime().toString();
    console.log('initialStart', this.startDate);
    console.log('initialEnd', this.endDate);
  }

  initializeDatePickerConfigs() {
    this.startDateConfig = {
      disableKeypress: true,
      format: 'YYYY-MM-DD H:mm',
      // max:this.form.value.endDate
    };
    this.endDateConfig = {
      disableKeypress: true,
      format: 'YYYY-MM-DD H:mm',
      // min: this.form.value.startDate
    };
  }


  downloadLocation(startDate, endDate) {
    startDate = new Date(startDate).getTime();
    endDate = new Date(endDate).getTime();
    this.deviceLocationSvc.downloadDeviceLocationsKmlFile(startDate, endDate, this.deviceId);
  }

  getInitialDeviceLocationsPaginate(startDate, endDate) {
    console.log('after initial convert start:', startDate);
    console.log('after initial convert endDate:', endDate);
    this.startDate = startDate;
    this.endDate = endDate;
    this.deviceLocationSvc.getDeviceLocationsPaginate(this.startDate, this.endDate, this.deviceId, this.currentPage, this.pageSize).subscribe(
      result => {
        this.count = result.count;
        this.locations = result.data || [];
      }, err => {
        console.log('devicelocation', err);
      }
    );
  }

  getDeviceLocationsPaginate(startDate, endDate) {
    startDate = new Date(startDate).getTime();
    endDate = new Date(endDate).getTime();

    console.log('after convert start:', startDate);
    console.log('after convert endDate:', endDate);
    this.startDate = startDate;
    this.endDate = endDate;
    this.deviceLocationSvc.getDeviceLocationsPaginate(this.startDate, this.endDate, this.deviceId, this.currentPage, this.pageSize).subscribe(
      result => {
        this.count = result.count;
        this.locations = result.data || [];
      }
    );
  }

  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.getDeviceLocationsPaginate(this.startDate, this.endDate);
  }

  showMarker(index: number) {
    this.lat = this.locations[index].location[1] || this.locations[index].location.coordinates[1];
    this.lng = this.locations[index].location[0] || this.locations[index].location.coordinates[0];

    this.setSelectedRow(index);
  }

  onMarkerClicked(index) {
    this.scrollElementIntoView(index);
  }

  scrollElementIntoView(index) {
    const tableRow: HTMLInputElement = this.tagElement.nativeElement.querySelector(`#loc${index}`);
    this.setSelectedRow(index);
    tableRow.scrollIntoView();
  }

  setSelectedRow(index) {
    this.selectedRowItem = index;

  }

}
