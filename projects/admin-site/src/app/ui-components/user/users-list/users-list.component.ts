import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../../core/services/user.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'itt-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  users = [];
  apiUrl;
  pageSize = 10;
  currentPage = 1;
  count = 10;

  constructor(private sanitization: DomSanitizer,
              private _userService: UserService,
              private router: Router) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit() {
    this.loadUser();
  }


  loadUser() {
    this._userService.getUsersPaginate(this.currentPage, this.pageSize).subscribe(users => {
      this.users = users.data;
      this.count = users.count;
    });
  }


  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.loadUser();
  }


  searchForUser(searchValue) {
    const searchTerm = searchValue.searchInput;
    this.router.navigate([`/search/${searchTerm}`]);
  }
}
