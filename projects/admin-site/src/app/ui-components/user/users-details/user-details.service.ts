import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeviceInfo, OwnedThemeInfo, SubscriptionsInfo, UserDetails, UserInfo} from '../../../models/themes.model';
import {of} from 'rxjs/internal/observable/of';
import {ThemeDetail} from '../../../core/models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getUserDevices(userId: string): Observable<DeviceInfo[]> {
    // return of([{
    //     id: '5555',
    //     hardwareId: '1200',
    //     operatingSystem: 'IOS',
    //     manufacturer: 'samsung',
    //     model: 'SM-G960W',
    //     installDate: '',
    //     operatingSystemVersion: '11.0',
    //     pnsHandle: ''
    //   },
    //   {
    //     id: '5555',
    //     hardwareId: '1200',
    //     operatingSystem: 'IOS',
    //     manufacturer: 'samsung',
    //     model: 'SM-G960W',
    //     installDate: '',
    //     operatingSystemVersion: '11.0',
    //     pnsHandle: ''
    //   }]
    // );


    const url = `${this.apiBaseUrl}/user/device/${userId}`;
    return this.http.get<DeviceInfo[]>(url);
  }

  getUserDeviceLocation(userId: string, deviceId: string, currentPage: number, pageSize: number) {
    // return this.http.get<any>(`${this.apiBaseUrl}/users/${userId}/devices/${deviceId}/locations`,
    //   {params: {currentPage: currentPage.toString(), pageSize: pageSize.toString()}});
  }


  getUserInfoById(userId: string): Observable<UserInfo> {
    const url = `${this.apiBaseUrl}/user/itt-user/${userId}`;
    return this.http.get<UserInfo>(url);
  }

  getUserSubscriptions(userId: string): Observable<SubscriptionsInfo[]> {
    const url = `${this.apiBaseUrl}/user/subscriptions/${userId}`;
    // const fakeSub: SubscriptionsInfo[] = [{
    //   id: '665123d46653d82dd3d16d',
    //   name: 'accra tip',
    //   imageUrl: 'dfda',
    //   createdBy: 'dfdd'
    // }];
    return this.http.get<SubscriptionsInfo[]>(url);
    // return Observable.of(fakeSub).delay(500);
  }


  getOwnedThemes(userId): Observable<OwnedThemeInfo[]> {
    const url = `${this.apiBaseUrl}/user/themes/${userId}`;
    return this.http.get<OwnedThemeInfo[]>(url);
  }

  getUserDetails(userId): Observable<UserDetails> {
    const url = `${this.apiBaseUrl}/admin/itt-user/${userId}/detail`;
    return this.http.get<UserDetails>(url);
  }

  getThemeDetails(themeId): Observable<ThemeDetail> {
    const url = `${this.apiBaseUrl}/admin/theme/${themeId}/detail`;
    return this.http.get<ThemeDetail>(url);
  }


  makeUserAdmin(userId): Observable<string> {
    const url = `${this.apiBaseUrl}/admin/admin-user/add`;
    const body = {userId: userId};
    return this.http.post<string>(url, body);
  }
}
