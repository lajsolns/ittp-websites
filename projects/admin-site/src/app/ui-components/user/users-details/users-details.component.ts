import {Component, Inject, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ThemeService} from '../../../core/services/theme.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DeviceInfo, OwnedThemeInfo, SubscriptionsInfo, UserDetails, UserInfo} from '../../../models/themes.model';
import {UserService} from './user-details.service';
import {ToastsManager} from 'ng6-toastr';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.css']
})
export class UsersDetailsComponent implements OnInit {
  apiUrl;
  userDetails: UserDetails;

  constructor(private sanitization: DomSanitizer, private themeService: ThemeService, private userService: UserService,
              private activatedRoute: ActivatedRoute, private toastr: ToastrManager) {
    this.userDetails = activatedRoute.snapshot.data['userDetails'];
    console.log('ddfd');
  }

  ngOnInit() {

  }

  makeUserAdmin(userId) {
    this.userService.makeUserAdmin(userId).subscribe(res => {
        this.toastr.successToastr('User added as Admin');
        window.location.reload();
      },
      err => {
        this.toastr.errorToastr('Could not add this user, something went wrong');
      });

  }

}
