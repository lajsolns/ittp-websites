import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../core/services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
// import {UserAuthService} from '../../../../../theme-store/src/app/core/services/user-auth.service';
// import {SigninStrategyService} from '../../../../../theme-store/src/app/core/services/signin-strategy.service';
// import {ServerAuthService} from '../../../../../theme-store/src/app/core/services/server-auth.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {SigninStrategyService} from '../../core/services/signin-strategy.service';
import {ServerAuthService} from '../../core/services/server-auth.service';
import {UserAuthService} from '../../core/services/user-auth.service';

@Component({
  selector: 'itt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  loginForm: FormGroup;
  termsAndConditionsUrl = environment.termsAndConditionsUrl;

  constructor(
    private router: Router,
    private authenticationSvc: UserAuthService,
    private signinStrategyService: SigninStrategyService,
    private serverAuth: ServerAuthService,
    route: ActivatedRoute,
    private toastr: ToastrManager
  ) {
    this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';

    if (authenticationSvc.isLoggedIn) {
      router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.email, Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  loginLocal(loginCreds) {
    const email = loginCreds.email.trim();
    const password = loginCreds.password.trim();

    this
      .signinStrategyService
      .signInFirebaseUserWithCreds({email, password, returnUrl: this.returnUrl});
  }

  loginFacebook() {
    this
      .signinStrategyService
      .signInFacebook(this.returnUrl)
      .then((res) => {
        res.subscribe(r => {
          console.log('is login: ', r);
          if (!r.isAdmin) {
            this.toastr.errorToastr('You cant login in, You are not an admin');
          } else {
            this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
            window.location.href = this.returnUrl;
          }
        });
      });
  }


  loginGoogle() {

    this
      .signinStrategyService
      .signInGoogle(this.returnUrl)
      .then((res) => {
        res.subscribe(r => {
          console.log('is login: ', r);
          if (!r.isAdmin) {
            this.toastr.errorToastr('You cant login in, You are not an admin');
          } else {
            this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
            window.location.href = this.returnUrl;
          }
        });
      });
  }

  loginTwitter() {
    this
      .signinStrategyService
      .signInTwitter(this.returnUrl)
      .then((res) => {
        res.subscribe(r => {
          console.log('is login: ', r);
          if (r.isAdmin === false) {
            this.toastr.errorToastr('You cant login in, You are not an admin');
          } else {
            this.signinStrategyService.saveLoginAndNavigateToReturnUrl(r);
            window.location.href = this.returnUrl;
          }
        });
      });
  }

  goToHome() {
    this.router.navigate([this.returnUrl]);
  }
}
