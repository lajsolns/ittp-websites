import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectOptionService } from '../../../core/services/select-option.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {
  category;
  constructor(private route: ActivatedRoute,
    private svc: SelectOptionService,
    private toastr: ToastrManager,
    private router: Router) {
    this.category = this.route.snapshot.data['category'];
  }

  ngOnInit() {
  }

  save(category) {
    this.svc.saveUpdatedCategory(category, this.category._id).subscribe((res) => {
      this.toastr.successToastr('saved succesfully');
      this.router.navigate(['categories']);
    });

  }

}
