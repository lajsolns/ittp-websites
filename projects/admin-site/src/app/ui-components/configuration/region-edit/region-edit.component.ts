import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectOptionService} from '../../../core/services/select-option.service';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-region-edit',
  templateUrl: './region-edit.component.html',
  styleUrls: ['./region-edit.component.css']
})
export class RegionEditComponent implements OnInit {
region;

  constructor(private route: ActivatedRoute,
     private svc: SelectOptionService,
     private toast: ToastrManager,
     private router: Router) {
    this.region = this.route.snapshot.data['region'];

  }

  ngOnInit() {
  }

  save(region) {
    this.svc.saveUpdatedRegion(region, this.region._id).subscribe((res) => {
      this.toast.successToastr('saved succesfully');
      console.log('i have saved this: ', this.region);
      this.router.navigate(['regions']);
    });

  }
}
