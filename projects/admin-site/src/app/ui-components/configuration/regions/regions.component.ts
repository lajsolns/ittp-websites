import {Component, ElementRef, OnInit, Renderer} from '@angular/core';
import {SelectOptionService} from '../../../core/services/select-option.service';
import {SelectOption} from '../../../core/models/select-option-models';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Router} from '@angular/router';

@Component({
  selector: 'itt-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class RegionsComponent implements OnInit {
  searchTerm;
  regions: SelectOption[] = [];
  addRegionForm: FormGroup;
  modal;

  constructor(private regionService: SelectOptionService,
              private fb: FormBuilder,
              private toastr: ToastrManager,
              private router: Router,
              private el: ElementRef,
              private renderer: Renderer) {
    this.loadRegions();
  }

  ngOnInit() {
    this.modal = this.el.nativeElement.querySelector('.modal');
    this.addRegionForm = this.fb.group({
      label: ['', Validators.required],
      order: ['', Validators.required],
      value: ['', Validators.required],
      description: ['']
    });
  }

  onSubmit(addRegionForm) {
    const addRegion = addRegionForm;
    this.regionService.saveRegion(addRegion).subscribe(res => {
      this.toastr.successToastr('saved successfuly');
      this.loadRegions();
      window.location.reload();
    }, err => {
      this.toastr.errorToastr('Saving region failed');
    });
  }

  searchForRegion(searchTerm) {
    this.searchTerm = searchTerm.value;
  }

  ondeleteRegion(id, index) {
    this.regionService.deleteRegion(id).subscribe(res => {
      this.toastr.successToastr('Removed Region successfully');
      this.loadRegions();
    }, err => {
      this.toastr.errorToastr('deleting region failed');
    });
  }

  updatePage() {
    this.router.navigate(['regions']);
  }

  loadRegions() {
    this.regionService.getRegions().subscribe(result => {
      this.regions = result;
    });

  }

}


