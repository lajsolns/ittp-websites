import {Component, OnInit} from '@angular/core';
import {SelectOptionService} from '../../../core/services/select-option.service';
import {SelectOptionUnit} from '../../../core/models/select-option-models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Router} from '@angular/router';

@Component({
  selector: 'itt-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: SelectOptionUnit[] = [];
  searchTerm;

  addCategoryForm: FormGroup;

  constructor(private categoryService: SelectOptionService,
              private fb: FormBuilder,
              private toastr: ToastrManager,
              private router: Router) {
    this.loadCategory();
  }

  ngOnInit() {
    this.addCategoryForm = this.fb.group({
      label: ['', Validators.required],
      order: ['', Validators.required],
      value: ['', Validators.required],
      description: ['']
    });
  }

  searchCategory(searchTerm) {
    this.searchTerm = searchTerm.value;
    console.log('this.searchTerm');
  }

  onSubmit(value) {
    console.log('i have saved this: ', value);

    this.categoryService.saveCategory(value).subscribe((res) => {
      this.toastr.successToastr('saved succesfully');
      // this.loadCategory();
      window.location.reload();
    }, err => {
      this.toastr.errorToastr('couldnt save category');
      this.router.navigate(['categories']);
    });

  }

  onDeleteCategory(id, index) {
    this.categoryService.deleteCategory(id).subscribe(res => {
      this.toastr.successToastr('Removed Category');
      this.loadCategory();
    });
  }

  loadCategory() {
    this.categoryService.getCategories().subscribe(result => {
      this.categories = result;
      console.log(this.categories);
    });
  }

}

