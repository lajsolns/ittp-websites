import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectOption} from '../../../core/models/select-option-models';

@Component({
  selector: 'itt-user-selection-editor',
  templateUrl: './user-selection-editor.component.html',
  styleUrls: ['./user-selection-editor.component.css']
})
export class UserSelectionEditorComponent implements OnInit {
  @Input()
 selectOption: SelectOption;

  @Output()
    editCompleted = new EventEmitter();

  selectOptionForm: FormGroup;


  constructor() {
  }

  ngOnInit() {
    this.initializeSelectionForm(this.selectOption);
  }



  onSubmit(formValue) {
    this.editCompleted.emit(formValue);
  }

  initializeSelectionForm(selectOption: SelectOption) {

    this.selectOptionForm = new FormGroup({
      label: new FormControl(selectOption.label, [Validators.required]),
      order: new FormControl(selectOption.order),
      value: new FormControl(selectOption.value),
      description: new FormControl(selectOption.description),

    });
  }


}
