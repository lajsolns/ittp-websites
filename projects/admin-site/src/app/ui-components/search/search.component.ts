import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'itt-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  userSearchResult;

  constructor(private activatedRoute: ActivatedRoute) {
    this.userSearchResult = this.activatedRoute.snapshot.data['searchInput'];
    console.log('theme-search results', this.userSearchResult);
  }

  ngOnInit() {

  }
}
