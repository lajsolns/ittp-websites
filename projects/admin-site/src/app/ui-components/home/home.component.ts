import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'itt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // Doughnut
  public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: number[] = [350, 450, 100];
  public doughnutChartType = 'doughnut';

  // Bar Chart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [
    { data: [28, 48, 40, 19, 86, 27, 100], label: 'Themes' }
  ];

  constructor() { }

  ngOnInit() {
  }

  // events
  public chartClicked(e: any): void {

  }

  public chartHovered(e: any): void {

  }
}
