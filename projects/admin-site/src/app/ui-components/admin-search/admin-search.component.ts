import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'itt-admin-search',
  templateUrl: './admin-search.component.html',
  styleUrls: ['./admin-search.component.css']
})
export class AdminSearchComponent implements OnInit {
  adminUserSearchResults;

  constructor(private activatedRoute: ActivatedRoute) {
    this.adminUserSearchResults = this.activatedRoute.snapshot.data['searchInput'];
    console.log('theme-search results', this.adminUserSearchResults);
  }

  ngOnInit() {
  }

}
