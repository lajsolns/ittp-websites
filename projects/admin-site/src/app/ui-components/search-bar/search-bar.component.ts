import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'itt-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output() Search = new EventEmitter<string>();

  searchForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.searchForm = new FormGroup({
      searchInput: new FormControl('', Validators.required),
    });
  }

  search(searchInput) {
    console.log(searchInput);
    this.Search.emit(searchInput);
  }

}
