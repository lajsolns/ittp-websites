export class Enum {
  name: string;
  description: string;
  values: EnumUnit[];
}

export class EnumUnit {
  value: number;
  label: string;
  description: string;
  order: number;
}
