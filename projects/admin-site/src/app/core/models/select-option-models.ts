export interface SelectOption {
  _id: string;
  label: string;
  order: number;
  value: number;
  description: string;
}

export interface SelectOptionUnit {
  _id: string;
  value: number;
  order: number;
  description: string;
  label: string;
}
