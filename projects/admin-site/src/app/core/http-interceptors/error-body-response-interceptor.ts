import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs/internal/observable/throwError';
import {Injectable} from '@angular/core';

@Injectable()
export class ErrorResponseInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError((event => {
      if (event instanceof HttpErrorResponse) {
        const response = event as HttpErrorResponse;
        if (response.headers.get('content-type') === 'application/json') {
          return throwError(new HttpErrorResponse({
            error: JSON.parse(response.error),
            headers: response.headers,
            status: response.status,
            statusText: response.statusText,
            url: response.url,
          }));
        }
      }
      return throwError(event);
    })));
  }
}
