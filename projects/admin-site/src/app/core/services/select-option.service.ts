import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {SelectOption, SelectOptionUnit} from '../models/select-option-models';


@Injectable({providedIn: 'root'})
export class SelectOptionService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  saveUpdatedCategory(category, categoryId): Observable<SelectOption> {
    const selection = {
      id: categoryId,
      label: category.label,
      order: category.order,
      value: category.value,
      description: category.description
    };

    console.log('saving this body: ', selection);
    return this.http.post<SelectOption>(`${this.apiBaseUrl}/admin/select-option/category/${categoryId}/edit`, selection);
  }

  saveUpdatedRegion(region, regionId): Observable<SelectOption> {
    const selection = {
      id: regionId,
      label: region.label,
      order: region.order,
      value: region.value,
      description: region.description
    };

    console.log('saving this body: ', selection);
    return this.http.post<SelectOption>(`${this.apiBaseUrl}/admin/select-option/region/${regionId}`, selection);
  }

  saveRegion(region) {
    const addRegion = {
      label: region.label,
      order: region.order,
      value: region.value,
      description: region.description

    };
    return this.http.post(`${this.apiBaseUrl}/admin/select-option/region/create`, addRegion);
  }

  saveCategory(category): Observable<any> {
    const addCategory = {
      label: category.label,
      order: category.order,
      value: category.value,
      description: category.description

    };
    return this.http.post<any>(`${this.apiBaseUrl}/admin/select-option/category/create`, addCategory);
  }

  getRegions(): Observable<SelectOption[]> {
    return this.http.get<SelectOption[]>(`${this.apiBaseUrl}/select-option?q=region`);
  }


  getRegionById(regionId): Observable<SelectOption> {
    return this.http.get<SelectOption>(`${this.apiBaseUrl}/admin/select-option/region/${regionId}`);
  }
  //
  // getCategories(): Observable<SelectOptionUnit[]> {
  //   return this.http.get<SelectOptionUnit[]>(`${this.apiBaseUrl}/admin/select-option/category/categories`);
  // }

  getCategories(): Observable<SelectOptionUnit[]> {
    return this.http.get<SelectOptionUnit[]>(`${this.apiBaseUrl}/select-option?q=category`);
  }

  getCategoryById(categoryId): Observable<SelectOptionUnit> {
    return this.http.get<SelectOptionUnit>(`${this.apiBaseUrl}/admin/select-option/category/${categoryId}`);
  }

  deleteRegion(regionId) {
    return this.http.post<any>(`${this.apiBaseUrl}/admin/select-option/region/remove`, {regionId: regionId});
  }

  deleteCategory(categoryId) {
    return this.http.post<any>(`${this.apiBaseUrl}/admin/select-option/category/remove`, {categoryId: categoryId});
  }
}

