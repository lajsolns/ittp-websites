import {Inject, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs';
import {PaginationResult} from '../models/pagination-result.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  getUsers() {
    return this._http.get<any[]>(`${this.apiBaseUrl}/user/me`);
  }

  getAdminUsers() {
    return this._http.get<any[]>(`${this.apiBaseUrl}/user/itt-admins`);
  }

  getAdminUsersPaginate(currentPage: number, pageSize: number = 10): Observable<PaginationResult> {
    const adminUserUrl = `${this.apiBaseUrl}/admin/admin-user`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this._http.get<PaginationResult>(adminUserUrl, {params: params});
  }

  removeUserFromAdmin(userId) {
    return this._http.post<any[]>(`${this.apiBaseUrl}/admin/admin-user/remove`, {userId: userId});
  }

  makeUserAdmin(userId) {
    return this._http.post<any[]>(`${this.apiBaseUrl}/user/admins/${userId}`, {makeAdmin: true});
  }

  searchUser(searchTerm) {
    return this._http.post<any[]>(`${this.apiBaseUrl}/user/search-user`, {searchTerm: searchTerm});

  }

  getUsersPaginate(currentPage: number, pageSize: number = 10): Observable<PaginationResult> {
    const userUrl = `${this.apiBaseUrl}/admin/itt-user`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this._http.get<PaginationResult>(userUrl, {params: params});
  }

  getUserDeviceLocation(userId: string, deviceId: string, currentPage: number, pageSize: number = 10): Observable<PaginationResult> {
    const locationsUrl = `${this.apiBaseUrl}/user/${userId}/devices/${deviceId}/locations`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this._http.get<PaginationResult>(locationsUrl, {params: params});
  }

  // downloadUserLocation(userId: string, deviceId: string, startDate: number, endDate: number): Observable<Blob> {
  //   return this._http.get<any>(`${this.apiBaseUrl}/users/${userId}/devices/${deviceId}/locations/download-locations`,
  //     { params: { startDate: startDate.toString(), endDate: endDate.toString(), responseType: 'text' } })
  //     .map(res => res.blob());
  // }
}
