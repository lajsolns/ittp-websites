import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {PaginationResult} from '../models/pagination-result.model';

@Injectable({
  providedIn: 'root'
})
export class DeviceLocationService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getDeviceLocationsPaginate(start, end, deviceId: string, currentPage: number, pageSize: number = 10): Observable<PaginationResult> {
    const locationsUrl = `${this.apiBaseUrl}/admin/device/${deviceId}/location`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.post<PaginationResult>(locationsUrl, {start: start, end: end}, {params: params});
  }

  downloadDeviceLocationsKmlFile(startDate, endDate, deviceId) {
    const locationsUrl = `${this.apiBaseUrl}/theme/device/${deviceId}/location/download/${startDate}/${endDate}`;
    window.location.href = locationsUrl;
  }
}
