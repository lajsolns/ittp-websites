import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TagProperty, Theme, ThemeBasicInfo, ThemeEditPreview, ThemeMedia, ThemeSettings} from '../models/theme-models';
import {ThemeDetail} from '../../../../../theme-store/src/app/core/models/theme-detail.model';

@Injectable({
  providedIn: 'root'
})
export class ThemeEditService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getUserThemePreviewsPagination(model: PaginationReqModel): Observable<PaginationResModel> {
    const url = `${this.apiBaseUrl}/my-themes/theme/user-themes`;
    const paramsModel = {
      page: model.page.toString(),
      pageSize: model.pageSize.toString()
    };
    const params = new HttpParams({fromObject: paramsModel});
    return this.http.get<PaginationResModel>(url, {params});
  }

  getThemeById(themeId: string): Observable<Theme> {
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}`;
    return this.http.get<Theme>(url);
  }

  getNewBasicInfo(): Observable<ThemeBasicInfo> {
    const url = `${this.apiBaseUrl}/my-themes/theme/new/`;
    return this.http.get<ThemeBasicInfo>(url);
  }

  createTheme(basicInfo: ThemeBasicInfo): Observable<number> {
    const url = `${this.apiBaseUrl}/my-themes/theme/create`;
    return this.http.post<number>(url, basicInfo);
  }

  getTagPropertiesById(themeId: string): Observable<TagProperty[]> {
    const paramsModel = {
      filter: 'tagProperties'
    };
    const params = new HttpParams({fromObject: paramsModel});
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}:`;
    return this.http.get<TagProperty[]>(url, {params});
  }


  // updateThemeTagProperties(tagProperties: Tag): Observable<any> {
  //   const url = '';
  //   // const url = `${this.apiBaseUrl}/my-themes/tagProperties/create/${themeId}`;
  //   return this.http.post<any>(url, basicInfo);
  // }

  getThemeBasicInfoById(themeId: string): Observable<ThemeBasicInfo> {
    const paramsModel = {
      filter: 'basicInfo'
    };
    const params = new HttpParams({fromObject: paramsModel});
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}:`;
    return this.http.get<ThemeBasicInfo>(url, {params});
  }

  updateThemeBasicInfoById(basicInfo: ThemeBasicInfo): Observable<any> {
    const url = '';
    // const url = `${this.apiBaseUrl}/my-themes/tagProperties/create/${themeId}`;
    return this.http.post<any>(url, basicInfo);
  }

  getThemeSettingsById(themeId: string): Observable<ThemeSettings> {
    const paramsModel = {
      filter: 'themeSettings'
    };
    const params = new HttpParams({fromObject: paramsModel});
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}:`;
    return this.http.get<ThemeSettings>(url, {params});
  }

  updateThemeSettingsById(themeSettings: ThemeSettings): Observable<any> {
    const url = '';
    // const url = `${this.apiBaseUrl}/my-themes/tagProperties/create/${themeId}`;
    return this.http.post<any>(url, themeSettings);
  }


  getMediaById(themeId: string): Observable<ThemeMedia> {
    const paramsModel = {
      filter: 'themeMedia'
    };
    const params = new HttpParams({fromObject: paramsModel});
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}:`;
    return this.http.get<ThemeMedia>(url, {params});
  }

  updateThemeMediaById(themeMedia: ThemeMedia): Observable<any> {
    const url = '';
    // const url = `${this.apiBaseUrl}/my-themes/tagProperties/create/${themeId}`;
    return this.http.post<any>(url, themeMedia);
  }


}

export interface PaginationReqModel {
  page: number;
  pageSize: number;
}

export interface PaginationResModel {
  data: ThemeEditPreview[];
  count: number;
  page: number;
}
