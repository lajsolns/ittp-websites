import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SelectOptionUnit} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class ThemeEditSelectOptionService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  // getDistanceOptions(): Observable<SelectOption[]> {
  //
  // }
  //
  // getTagSilenceOptions(): Observable<SelectOption[]> {
  //
  // }
  //
  // getCategoryOptions(): Observable<SelectOption[]> {
  //
  // }
  //
  // getTagProperyTypeOptions(): Observable<SelectOption[]> {
  //
  // }
  //
  // getCurrencyOptions(): Observable<SelectOption[]> {
  //
  // }
  //
  // getRegionOptions(): Observable<SelectOption[]> {
  //
  // }

  getAllOptions(): Observable<AllSelectOptions> {
    const url = `${this.apiBaseUrl}/select-option`;
    return this.http.get<AllSelectOptions>(url);
  }
}

export interface AllSelectOptions {
  distance: SelectOptionUnit[];
  time: SelectOptionUnit[];
  currency: SelectOptionUnit[];
  regions: SelectOptionUnit[];
  categories: SelectOptionUnit[];
  tagPropertyType: SelectOptionUnit[];
}
