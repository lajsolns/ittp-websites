import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AllSelectOptions, ThemeEditSelectOptionService} from '../services/theme-edit-select-option.service';

@Injectable({
  providedIn: 'root'
})
export class SelectOptionResolverService {

  constructor(private selectOptionService: ThemeEditSelectOptionService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AllSelectOptions> {
    return this.selectOptionService.getAllOptions();
  }
}
