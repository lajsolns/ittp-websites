import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ThemeSettings} from '../models/theme-models';
import {ThemeEditService} from '../services/theme-edit.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeSettingsResolver implements Resolve<ThemeSettings> {
  constructor(private themeEditSvc: ThemeEditService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeSettings> {
    const themeId = route.paramMap.get('themeId');
    return  this.themeEditSvc.getThemeSettingsById(themeId);
  }

}
