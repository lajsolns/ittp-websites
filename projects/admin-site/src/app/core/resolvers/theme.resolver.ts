import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {Theme} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class ThemeResolver implements Resolve<any> {
  constructor(private themeEditSvc: ThemeEditService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Theme> {
    const themeId = route.paramMap.get('themeId');
    return  this.themeEditSvc.getThemeById(themeId);
  }

}
