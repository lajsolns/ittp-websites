import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeDetail} from '../models/theme-models';
import {UserService} from '../../ui-components/user/users-details/user-details.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeDetailResolver {

  constructor(private userService: UserService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeDetail> {
    const themeId = route.paramMap.get('themeId');
    return this.userService.getThemeDetails(themeId);
  }
}
