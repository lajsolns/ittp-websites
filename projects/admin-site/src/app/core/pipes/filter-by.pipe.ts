import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'themeFilter',
  pure: false
})
export class FilterByPipe implements PipeTransform {

  transform(editThemes, searchTerm: string): any[] {
    if (!editThemes) {
      return [];
    }
    return editThemes.filter(x => ( x.name.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ));
  }

}
