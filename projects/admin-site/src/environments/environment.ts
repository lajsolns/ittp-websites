// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  manageThemes: 'https://itt-new-theme-manager.azurewebsites.net',
  store: 'https://itt-new-store.azurewebsites.net',
  googleClientId: '770362430007-6rofm64an3vr2521sbioqqso6nnt4uvb.apps.googleusercontent.com',
  facebookAppId: '365884523951157',
  // apiUrl: 'http://localhost:3500/api/v1',
  // apiUrl: 'https://itt-new-server.azurewebsites.net/api/v1',
  apiUrl: 'https://itt-new-server.herokuapp.com/api/v1',
  termsAndConditionsUrl: 'https://www.itagtip.com/About/Terms',
  defaultLocationCalgary: {
    latitude: 53.631611,
    longitude: -113.323975
  },
  firebase: {
    apiKey: 'AIzaSyAnJzrKJIVq3q5Cv_lqpqhsTpZqHLua2QI',
    authDomain: 'itagtip-dev.firebaseapp.com',
    databaseURL: 'https://itagtip-dev.firebaseio.com',
    projectId: 'itagtip-dev',
    storageBucket: 'itagtip-dev.appspot.com',
    messagingSenderId: '770362430007'
  },
  mapApiKey: 'AIzaSyA2oJLGnuBizPTavRWNHT1PFTGnP4hKHsU'

};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


