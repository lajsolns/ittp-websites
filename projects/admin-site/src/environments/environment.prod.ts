export const environment = {
  production: false,
  manageThemes: 'https://itt-new-theme-manager.azurewebsites.net',
  store: 'https://itt-new-store.azurewebsites.net',
  googleClientId: '770362430007-6rofm64an3vr2521sbioqqso6nnt4uvb.apps.googleusercontent.com',
  facebookAppId: '365884523951157',
  // apiUrl: 'https://itt-new-server.azurewebsites.net/api/v1',
  apiUrl: 'https://itt-new-server.herokuapp.com/api/v1',
  termsAndConditionsUrl: 'https://www.itagtip.com/About/Terms',
  defaultLocationCalgary: {
    latitude: 53.631611,
    longitude: -113.323975
  },
  firebase: {
    apiKey: 'AIzaSyAnJzrKJIVq3q5Cv_lqpqhsTpZqHLua2QI',
    authDomain: 'itagtip-dev.firebaseapp.com',
    databaseURL: 'https://itagtip-dev.firebaseio.com',
    projectId: 'itagtip-dev',
    storageBucket: 'itagtip-dev.appspot.com',
    messagingSenderId: '770362430007'
  },
  mapApiKey: 'AIzaSyDKGuo7TefeBPUBCRRkI8gGWiGwMrprkaI'
};
