export const environment = {
  production: true,
  googleClientId: '770362430007-6rofm64an3vr2521sbioqqso6nnt4uvb.apps.googleusercontent.com',
  facebookAppId: '365884523951157',
  // apiBaseUrl: 'https://itt-new-server.azurewebsites.net/api/v1',
  apiBaseUrl: 'https://itt-new-server.herokuapp.com/api/v1',
  termsAndConditionsUrl: 'https://www.itagtip.com/About/Terms',
  storeUrl: 'https://itt-new-store.azurewebsites.net',
  mapApiKey: 'AIzaSyA2oJLGnuBizPTavRWNHT1PFTGnP4hKHsU',
  firebase: {
    apiKey: 'AIzaSyAnJzrKJIVq3q5Cv_lqpqhsTpZqHLua2QI',
    authDomain: 'itagtip-dev.firebaseapp.com',
    databaseURL: 'https://itagtip-dev.firebaseio.com',
    projectId: 'itagtip-dev',
    storageBucket: 'itagtip-dev.appspot.com',
    messagingSenderId: '770362430007'
  }

};

