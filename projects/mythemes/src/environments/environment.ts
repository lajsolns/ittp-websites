// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  googleClientId: '770362430007-6rofm64an3vr2521sbioqqso6nnt4uvb.apps.googleusercontent.com',
  facebookAppId: '365884523951157',
  apiBaseUrl: 'http://localhost:3500/api/v1',
  // apiBaseUrl: 'https://itt-new-server.azurewebsites.net/api/v1',
  // apiBaseUrl: 'https://itt-new-server.herokuapp.com/api/v1',
  termsAndConditionsUrl: 'https://www.itagtip.com/About/Terms',
  storeUrl: 'https://itt-new-store.azurewebsites.net',
  mapApiKey: 'AIzaSyA2oJLGnuBizPTavRWNHT1PFTGnP4hKHsU',
  firebase: {
    apiKey: 'AIzaSyAnJzrKJIVq3q5Cv_lqpqhsTpZqHLua2QI',
    authDomain: 'itagtip-dev.firebaseapp.com',
    databaseURL: 'https://itagtip-dev.firebaseio.com',
    projectId: 'itagtip-dev',
    storageBucket: 'itagtip-dev.appspot.com',
    messagingSenderId: '770362430007'
  }
};
