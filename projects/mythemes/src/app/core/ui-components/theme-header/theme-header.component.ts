import {Component, Input, OnInit} from '@angular/core';
import {ThemeHeaderModel} from '../../models/common.models';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'itt-theme-header',
  templateUrl: './theme-header.component.html',
  styleUrls: ['./theme-header.component.css']
})
export class ThemeHeaderComponent implements OnInit {
  @Input() themeId: string;
  themeHeaderModel: ThemeHeaderModel = {themeImageUrl: '/assets/images/theme-icon.png', id: '', name: ''};

  constructor(private commonSvc: CommonService) { }

  ngOnInit() {
    this
      .commonSvc
      .getThemeHeaderModel(this.themeId)
      .subscribe(model => this.themeHeaderModel = model, console.log);
  }
}


