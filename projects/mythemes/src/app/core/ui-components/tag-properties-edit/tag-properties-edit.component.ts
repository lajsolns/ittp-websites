import {Component, Input, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute, ActivatedRouteSnapshot, Route, Router} from '@angular/router';
import {SelectOptionUnit} from '../../models/edit-models';
import {ThemeEditService} from '../../services/theme-edit.service';
import {TagProperty, Theme} from '../../models/theme-models';

@Component({
  selector: 'itt-tag-properties-edit',
  templateUrl: './tag-properties-edit.component.html',
  styleUrls: ['./tag-properties-edit.component.css']
})
export class TagPropertiesEditComponent implements OnInit {

  tagProperties: TagProperty[];
  tagPropertyType: SelectOptionUnit[];
  theme: Theme;
  themeId: string;
  isOpened: boolean = false;


  constructor(private toastr: ToastrManager,
              private activatedRoute: ActivatedRoute,
              private themeEditService: ThemeEditService,
              private router: Router) {

    this.tagProperties = this.activatedRoute.snapshot.data['tagProperties'];
    this.tagPropertyType = this.activatedRoute.snapshot.data['selectOptions'].tagPropertyType;
    this.themeId = this.activatedRoute.snapshot.params['themeId'];
  }

  ngOnInit() {

  }

  saveTagProperties(tagProperties) {
    console.log('tag-props', tagProperties);
    this.themeEditService
      .updateThemeTagProperties(this.themeId, tagProperties)
      .subscribe(res => {
        this.toastr.successToastr('Tag Property Updated');
      }, error => this.toastr.errorToastr('Error updating tag property'));
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
