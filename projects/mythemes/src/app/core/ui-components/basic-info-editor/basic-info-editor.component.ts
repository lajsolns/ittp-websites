import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectOptionUnit} from '../../models/edit-models';
import {ThemeBasicInfo} from '../../models/theme-models';

@Component({
  selector: 'itt-basic-info-editor',
  templateUrl: './basic-info-editor.component.html',
  styleUrls: ['./basic-info-editor.component.css']
})
export class BasicInfoEditorComponent implements OnInit {
  @Input()
  distanceOptions: SelectOptionUnit[];
  @Input()
  tagSilenceOptions: SelectOptionUnit[];

  @Input()
  basicInfo: ThemeBasicInfo;

  @Output()
  editComplete = new EventEmitter();

  basicInfoForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.initializeBasicInfoForm(this.basicInfo);
  }

  initializeBasicInfoForm(basicInfo: ThemeBasicInfo) {

    this.basicInfoForm = new FormGroup({
      name: new FormControl(basicInfo.name, [Validators.required]),
      description: new FormControl(basicInfo.description),
      color: new FormControl(basicInfo.color),
      defaultDistance: new FormControl(basicInfo.defaultDistance),
      defaultDistanceUnit: new FormControl(basicInfo.defaultDistanceUnit),
      defaultTagSilence: new FormControl(basicInfo.defaultTagSilence),
      defaultTagSilenceUnit: new FormControl(basicInfo.defaultTagSilenceUnit),
    });
  }

  get color() {
    return this.basicInfoForm.get('color');
  }

  onSubmit(formValue) {
    console.log(formValue);
    this.editComplete.emit(formValue);
  }

}
