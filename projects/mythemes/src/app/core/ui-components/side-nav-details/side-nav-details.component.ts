import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'itt-side-nav-details',
  templateUrl: './side-nav-details.component.html',
  styleUrls: ['./side-nav-details.component.css']
})
export class SideNavDetailsComponent implements OnInit {

  storeUrl: string;

  constructor(private router: Router) {
    this.storeUrl = environment.storeUrl;
  }

  ngOnInit() {
  }
}
