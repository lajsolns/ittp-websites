import {Component, Input, OnInit} from '@angular/core';
import {Chart} from 'node_modules/chart.js/';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ReviewService} from '../../services/review.service';


@Component({
  selector: 'itt-review-chart',
  templateUrl: './review-chart.component.html',
  styleUrls: ['./review-chart.component.css']
})
export class ReviewChartComponent implements OnInit {
  @Input() themeId;
  @Input() data;
  reviewChart = [];


  constructor(private reviewSvc: ReviewService, private toastr: ToastrManager) {
  }

  ngOnInit() {

    this.reviewSvc.getThemeRatings(this.themeId).subscribe((res) => {
      const {labels, data} = res;

      // res.forEach((result, index) => {
      //   label[index] = result._id;
      //   data[index] = result.count;
      // });

      console.log(' data ', this.data);

      this.reviewChart = new Chart('canvas', {
        type: 'horizontalBar',
        data: {
          labels,
          datasets: [{
            data: this.data,
            backgroundColor: [
              '#ff6f31',
              '#ff9f02',
              '#ffcf02',
              '#9ace6a',
              '#57bb8a',
              '#57bb8a'
            ],
            borderColor: [
              '#ff6f31',
              '#ff9f02',
              '#ffcf02',
              '#9ace6a',
              '#57bb8a',
              '#57bb8a'
            ],
            borderWidth: 0.5
          }]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            yAxes: [{
              barThickness: 15,
              ticks: {
                beginAtZero: true
              },
              gridLines: {
                display: false
              }
            }],
            xAxes: [{
              display: false
            },
            ],
          }
        }
      });
    }, error => {
      this.toastr.errorToastr('error', error);
      console.log('error', error);
    });
  }
}
