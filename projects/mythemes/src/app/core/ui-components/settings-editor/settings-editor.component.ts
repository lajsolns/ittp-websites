import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validator, Validators} from '@angular/forms';
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {ThemeSettings} from '../../models/theme-models';
import {SelectOptionUnit} from '../../models/edit-models';

@Component({
  selector: 'itt-settings-editor',
  templateUrl: './settings-editor.component.html',
  styleUrls: ['./settings-editor.component.css']
})
export class SettingsEditorComponent implements OnInit {

  @Output() editComplete = new EventEmitter<any>();
  @Input() settings: ThemeSettings;
  @Input() currencies: SelectOptionUnit[];
  @Input() regions: SelectOptionUnit[];
  @Input() categories: SelectOptionUnit[];

  settingsForm: FormGroup;

  regionOptionsModel = [];
  categoryOptionsModel = [];

  regionEnum: IMultiSelectOption[] = [];
  categoryEnum: IMultiSelectOption[] = [];

  myRegionTexts: IMultiSelectTexts = {
    defaultTitle: 'Regions',
  };

  myCategoryTexts: IMultiSelectTexts = {
    defaultTitle: 'Category',
  };

  myCategorySettings: IMultiSelectSettings = {
    // buttonClasses: 'btn btn-default'
  };

  constructor() {
  }

  ngOnInit() {
    this.initializeForm();
    console.log('setting data return from server: ', this.settings);
    this.regions.forEach((region) => {
      this.regionEnum.push({id: region.value, name: region.label});
    });

    this.categories.forEach((category) => {
      this.categoryEnum.push({id: category.value, name: category.label});
    });

    this.regionOptionsModel = this.settings.region;
    this.categoryOptionsModel = this.settings.category;
    console.log('categies: ', this.categoryEnum);
  }

  initializeForm() {
    this.settingsForm = new FormGroup({
      region: new FormControl(this.settings.region, [Validators.required] ),
      price: new FormControl(this.settings.price),
      currency: new FormControl(this.settings.currency),
      visibleInStore: new FormControl(this.settings.visibleInStore),
      allowCrowdSourcing: new FormControl(this.settings.allowCrowdSourcing),
      allowMovingTags: new FormControl(this.settings.allowMovingTags),
      useAccessList: new FormControl(this.settings.useAccessList),
      category: new FormControl(this.settings.category, [Validators.required])
    });
  }

  saveStoreProperties(value) {
    this.editComplete.emit(value);
    console.log('settings:', value);
  }
}




