import {Component, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeEditService} from '../../services/theme-edit.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ThemeSettings} from '../../models/theme-models';
import {SelectOptionsModel} from '../../models/common.models';

@Component({
  selector: 'itt-settings-edit',
  templateUrl: './settings-edit.component.html',
  styleUrls: ['./settings-edit.component.css']
})
export class SettingsEditComponent implements OnInit {
  settings: ThemeSettings;
  themeId: string;
  selectOptions: SelectOptionsModel;
  isOpened = false;

  constructor(private activatedRoute: ActivatedRoute, private themeEditService: ThemeEditService, public toastr: ToastrManager,
              private router: Router) {
    this.settings = this.activatedRoute.snapshot.data['settings'];
    this.themeId = this.activatedRoute.snapshot.params['themeId'];
    this.selectOptions = activatedRoute.snapshot.data['selectOptions'];
  }

  ngOnInit() {
  }

  saveTheme(event) {
    this
      .themeEditService
      .updateThemeSettings(this.themeId, event)
      .subscribe(id => {
        this.toastr.successToastr('Theme Updated');
        // this.router.navigate(['my-themes']);
      }, error1 => this.toastr.errorToastr('Error updating tagProperties'));
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
