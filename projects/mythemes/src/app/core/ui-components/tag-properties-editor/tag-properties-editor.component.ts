import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectOptionUnit} from '../../models/edit-models';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute} from '@angular/router';
import {TagProperty} from '../../models/theme-models';

@Component({
  selector: 'itt-tag-properties-editor',
  templateUrl: './tag-properties-editor.component.html',
  styleUrls: ['./tag-properties-editor.component.css']
})
export class TagPropertiesEditorComponent implements OnInit {
  @Input()
  tagPropertyType: SelectOptionUnit[];
  @Input()
  tagProperties: TagProperty[] = [];
  @Output()
  editComplete = new EventEmitter();
  themeId: string;
  tagPropertiesFormArray = new FormArray([]);

  constructor(private activatedRoute: ActivatedRoute, public toastr: ToastrManager) {
    this.tagProperties = this.activatedRoute.snapshot.data['tagProperties'];
    this.themeId = this.activatedRoute.snapshot.params['themeId'];
  }

  ngOnInit() {
    this.initializeTagPropertiesFormArray();
  }

  add() {
    this.tagPropertiesFormArray.push(this.getFormItem(new TagProperty()));
    this.tagPropertiesFormArray.markAsTouched();
  }

  delete(index: number) {
    this.tagPropertiesFormArray.removeAt(index);
    this.tagPropertiesFormArray.markAsTouched();
  }

  initializeTagPropertiesFormArray() {
    this.tagProperties.forEach(tagProperty => {
      this.tagPropertiesFormArray.push(this.getFormItem(tagProperty));
    });
  }

  getFormItem(tagProperty: TagProperty) {
    return new FormGroup(
      {
        name: new FormControl(tagProperty.name, [Validators.required, this.duplicateNameValidator.bind(this)]),
        tagPropertyType: new FormControl(tagProperty.tagPropertyType, [Validators.required]),
        mandatory: new FormControl(tagProperty.mandatory, [Validators.required]),
        showOnMap: new FormControl(tagProperty.showOnMap, [Validators.required]),
        showOnTip: new FormControl(tagProperty.showOnTip, [Validators.required]),
      }
    );
  }

  reOrderTags(fromIndex, toIndex) {
    const formArrayControl = this.tagPropertiesFormArray.controls[`${fromIndex}`];
    this.tagPropertiesFormArray.removeAt(fromIndex);
    this.tagPropertiesFormArray.insert(toIndex, formArrayControl);
    this.tagPropertiesFormArray.markAsTouched();
  }


  duplicateNameValidator(control: AbstractControl) {
    if (control.pristine) {
      return null;
    }

    const nameControls = this.tagPropertiesFormArray.controls.map(x => {
      const currentLineItem = x as FormGroup;
      return currentLineItem.controls['name'];
    });

    const otherControls = nameControls.filter(x => x !== control);
    const existingNames = [];
    otherControls.forEach(x => {
      if (x.value) {
        existingNames.push(x.value.toLowerCase());
      }
    });

    const currentControlValue = control.value.toLowerCase();

    if (existingNames.includes(currentControlValue)) {
      return {duplicateName: true};
    } else {
      return null;
    }
  }

  save() {
    this.tagPropertiesFormArray.updateValueAndValidity();
    if (this.tagPropertiesFormArray.valid) {
      const formValue = this.tagPropertiesFormArray.value;
      formValue.forEach(x => x.normalizedName = x.name.toLowerCase());
      console.log(formValue);
      this.editComplete.emit(formValue);
    } else {
      this.toastr.errorToastr('Please check for errors');
    }
  }
}
