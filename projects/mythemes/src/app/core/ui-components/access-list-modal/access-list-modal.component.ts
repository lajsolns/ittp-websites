import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AccessListService} from '../../services/access-list.service';

@Component({
  selector: 'itt-access-list-modal',
  templateUrl: './access-list-modal.component.html',
  styleUrls: ['./access-list-modal.component.css']
})
export class AccessListModalComponent implements OnInit {

  @Input() userEmail: string;
  @Input() themeId: string;
  @Output() add = new EventEmitter();

  constructor(public accesslistModal: NgbActiveModal) {
  }

  ngOnInit() {
  }


  cancel() {
    this.accesslistModal.dismiss();
  }

  save() {
    this.add.emit(true);
    this.accesslistModal.close();
  }
}


