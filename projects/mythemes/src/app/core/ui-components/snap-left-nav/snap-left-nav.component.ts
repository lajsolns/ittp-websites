import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'itt-snap-left-nav',
  templateUrl: './snap-left-nav.component.html',
  styleUrls: ['./snap-left-nav.component.css']
})
export class SnapLeftNavComponent implements OnInit {
  @Input() themeId;

  constructor() {

  }

  ngOnInit() {
  }
}
