import {Component, Inject, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeEditService} from '../../services/theme-edit.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FileUploader} from 'ng2-file-upload';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'itt-media-edit',
  templateUrl: './media-edit.component.html',
  styleUrls: ['./media-edit.component.css']
})
export class MediaEditComponent implements OnInit {
  media;
  isOpened = false;
  themeId: string;
  supportedImageFileTypes = 'image/jpeg, image/jpg, image/png, image/bmp, image/tiff, image/gif';
  imageUrl;
  file;
  uploader: FileUploader;

  constructor(
    private activatedRoute: ActivatedRoute,
    private themeEditService: ThemeEditService,
    public toastr: ToastrManager,
    private router: Router,
    private authSvc: AuthenticationService,
    @Inject('API_BASE_URL') private apiBaseUrl: string) {
      this.themeId = activatedRoute.snapshot.params['themeId'];
      const url = `${this.apiBaseUrl}/my-themes/theme/${this.themeId}/media-upload`;
      const authToken = `Bearer ${this.authSvc.currentUserValue.token}`;
      this.uploader = new FileUploader({url, authToken, itemAlias: 'file'});
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.toastr.successToastr('save successful');
    };
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

  uploadImage() {
    const fileReader = new FileReader();
    fileReader.onload = (e: any) => {
      this.imageUrl = e.currentTarget.result;
      console.log(this.imageUrl);
    };
    fileReader.readAsDataURL(this.file);
    // fileReader.readAsBinaryString(this.file);
  }

  fileChanged(e) {
    this.file = e.target.files[0];
    this.uploadImage();
  }
}
