import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ThemeEditService} from '../../services/theme-edit.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeBasicInfo} from '../../models/theme-models';
import {SelectOptionsModel} from '../../models/common.models';

@Component({
  selector: 'itt-basic-info-edit',
  templateUrl: './basic-info-edit.component.html',
  styleUrls: ['./basic-info-edit.component.css']
})
export class BasicInfoEditComponent implements OnInit {
  themeId: string;
  basicInfo: ThemeBasicInfo;
  selectOptions: SelectOptionsModel;
  isOpened = false;


  constructor(private activatedRoute: ActivatedRoute, private themeEditService: ThemeEditService, public toastr: ToastrManager,
              private router: Router) {
    this.basicInfo = this.activatedRoute.snapshot.data['basicInfo'];
    this.selectOptions = activatedRoute.snapshot.data['selectOptions'];
    this.themeId = this.activatedRoute.snapshot.params['themeId'];


  }

  ngOnInit() {
    console.log('options', this.selectOptions);
  }

  saveTheme(basicInfo: ThemeBasicInfo) {
    this
      .themeEditService
      .updateThemeBasicInfo(this.themeId, basicInfo)
      .subscribe(id => {
        // this.toastr.successToastr('Theme Updated');
        window.location.reload(true);
        // this.router.navigate(['my-themes']);
      }, error1 => this.toastr.errorToastr('Error updating tagProperties'));
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
