  import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Theme} from '../../models/theme-models';

@Component({
  selector: 'itt-theme-edit-tabs',
  templateUrl: './theme-edit-tabs.component.html',
  styleUrls: ['./theme-edit-tabs.component.css']
})
export class ThemeEditTabsComponent implements OnInit {
  themeId;
  theme: Theme;

  constructor(private state: ActivatedRoute) {
    // this.theme = this.state.snapshot.data['theme'];
    this.themeId = this.state.snapshot.params['themeId'];

  }

  ngOnInit() {
  }

}
