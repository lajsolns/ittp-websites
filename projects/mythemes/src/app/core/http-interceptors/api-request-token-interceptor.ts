import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {AuthenticationService} from '../services/authentication.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class ApiRequestTokenInterceptor implements HttpInterceptor {

  constructor(private authenticationSvc: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = req.url.toLowerCase();

    if (url.startsWith(environment.apiBaseUrl)) {
      const currentUser = this.authenticationSvc.currentUserValue;

      if (currentUser && currentUser.token) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${currentUser.token}`
          }
        });
      }
    }

    return next.handle(req);
  }
}
