import {Inject, Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs/internal/observable/throwError';
import {UserAuthService} from '../../../../../theme-store/src/app/core/services/user-auth.service';

@Injectable()
export class TokenErrorResponseInterceptor implements HttpInterceptor {

  constructor(private authenticationSvc: UserAuthService, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(err => {
      const url = req.url.toLowerCase();
      const isApiRequest = url.startsWith(this.apiBaseUrl);
      const isNotLoginOrRegisterReq = !url.includes('login') || !url.includes('register');
      const is401or403Error = err.status === 401 || err.status === 403;

      if (isApiRequest && isNotLoginOrRegisterReq && is401or403Error) {
        this.authenticationSvc.signOut();
        location.reload(true);
      }

      return throwError(err);
    }));
  }
}
