import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  unsubscribe(themeId: string): Observable<any> {
    const url = `${this.apiBaseUrl}/subscription/theme/unsubscribe/${themeId}`;
    return this.http.post<any>(url, {});
  }

}

