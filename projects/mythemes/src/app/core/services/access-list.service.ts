import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaginationResModel} from './theme-edit.service';

@Injectable({
  providedIn: 'root'
})
export class AccessListService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  getWhitelistPaginated(themeId: string, currentPage: number, pageSize: number): Observable<PaginationModel> {
    const url = `${this.apiBaseUrl}/my-themes/access-list/${themeId}/white-list`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationModel>(url, {params});
  }

  addToWhiteList(userId, themeId) {
    const url = `${this.apiBaseUrl}/my-themes/${userId}/${themeId}/white-list`;
    return this.http.post(url, {});
  }

  removeFromWhiteList(userId, themeId) {
    const url = `${this.apiBaseUrl}/my-themes/${userId}/${themeId}/white-list/remove`;
    return this.http.post(url, {});
  }

  searchUser(userId): Observable<{ _id: string, email: string }> {
    const url = `${this.apiBaseUrl}/user/${userId}/search-exact-user`;
    return this.http.get<{ _id: string, email: string }>(url);
  }
}

export class WhiteList {
  userId: string;
  name: string;
}

export class PaginationModel {
  count: number;
  data: WhiteList[];
}
