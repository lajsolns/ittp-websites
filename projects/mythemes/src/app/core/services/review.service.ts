import {Injectable, Inject} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {PaginationResult} from '../models/pagination-result.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  saveReview(review, themeId, name, imageUrl, userId): Observable<Review> {
    const addReview = {
      message: review.message,
      themeId: themeId,
      rating: review.rating,
      name: name,
      profileImageUrl: imageUrl,
      userId: userId
    };
    return this.http.post<Review>(`${this.apiBaseUrl}/store/review/add`, addReview);
  }

  updateReview(review, themeId, userId): Observable<any> {
    const updateReview = {
      message: review.message,
      themeId: themeId,
      rating: review.rating,
      userId: userId
    };
    return this.http.post<any>(`${this.apiBaseUrl}/store/review/edit`, updateReview);
  }


  getMyReviewPaginated(currentPage, pageSize: number, themeId): Observable<PaginationResult> {
    const url = `${this.apiBaseUrl}/store/review/${themeId}`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    console.log('this is the params', params);
    return this.http.get<PaginationResult>(url, {params: params});

  }

  getThemeRatings(themeId): Observable<{labels: string[], data: number[]}> {
    const url = `${this.apiBaseUrl}/store/review/ratings/${themeId}`;
    return this.http.get<{labels: string[], data: number[]}>(url);
  }

  getMyReview(themeId, userId): Observable<Review> {
    const params = new HttpParams()
      .set('userId', userId);
    const url = `${this.apiBaseUrl}/store/my-review/${themeId}`;
    return this.http.get<Review>(url, {params: params});
  }


}



export interface Review {
  themeId: string;
  message: string;
  rating: number;
}
