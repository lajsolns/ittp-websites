import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ThemeEditPreview} from '../models/edit-models';
import {TagProperty, Theme, ThemeBasicInfo, ThemeMedia, ThemeSettings} from '../models/theme-models';
import {ThemeDetail} from '../models/theme-detail.model';

@Injectable({
  providedIn: 'root'
})
export class ThemeEditService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getUserThemePreviewsPagination(model: PaginationReqModel): Observable<PaginationResModel> {
    const url = `${this.apiBaseUrl}/my-themes/theme/user-themes`;
    const paramsModel = {
      page: model.page.toString(),
      pageSize: model.pageSize.toString()
    };
    const params = new HttpParams({fromObject: paramsModel});
    return this.http.get<PaginationResModel>(url, {params});
  }

  getThemeById(themeId: string): Observable<Theme> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}`;
    return this.http.get<Theme>(url);
  }

  getNewBasicInfo(): Observable<ThemeBasicInfo> {
    const url = `${this.apiBaseUrl}/my-themes/new-theme`;
    return this.http.get<ThemeBasicInfo>(url);
  }

  createTheme(basicInfo: ThemeBasicInfo): Observable<number> {
    const url = `${this.apiBaseUrl}/my-themes/theme/create`;
    return this.http.post<number>(url, basicInfo);
  }

  getThemeTagProperties(themeId: string): Observable<TagProperty[]> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-properties`;
    return this.http.get<TagProperty[]>(url);
  }

  updateThemeTagProperties(themeId: string, tagProperties: TagProperty[]): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-properties`;
    return this.http.post<any>(url, tagProperties);
  }

  getThemeBasicInfo(themeId: string): Observable<ThemeBasicInfo> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/basic-info`;
    return this.http.get<ThemeBasicInfo>(url);
  }

  updateThemeBasicInfo(themeId: string, basicInfo: ThemeBasicInfo): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/basic-info`;
    return this.http.post<any>(url, basicInfo);
  }

  getThemeSettings(themeId: string): Observable<ThemeSettings> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/settings`;
    return this.http.get<ThemeSettings>(url);
  }

  updateThemeSettings(themeId: string, themeSettings: ThemeSettings): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/settings`;
    return this.http.post<any>(url, themeSettings);
  }

  getDetailById(themeId: string): Observable<ThemeDetail> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/detail`;
    return this.http.get<ThemeDetail>(url);
  }

  getMediaById(themeId: string): Observable<ThemeMedia> {
    const paramsModel = {
      filter: 'themeMedia'
    };
    const params = new HttpParams({fromObject: paramsModel});
    const url = `${this.apiBaseUrl}/my-themes/theme/edit/${themeId}:`;
    return this.http.get<ThemeMedia>(url, {params});
  }

  updateThemeMedia(themeId: string, file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/media-upload`;
    return this.http.post<any>(url, formData);
  }
}

export interface PaginationReqModel {
  page: number;
  pageSize: number;
}

export interface PaginationResModel {
  data: ThemeEditPreview[];
  count: number;
  page: number;
}
