import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {PaginationResult} from '../models/pagination-result.model';
import {WishList} from '../../routes/wishlist/wishlist.component';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getMyWishListPaginated(currentPage, pageSize, themeId): Observable<PaginationResult> {

    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/wishlist`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(url, {params: params});


  }

  getWishListUserDetail(themeId, WishListUserId): Observable<WishList> {
    return of({
      _id: '5bd88a13503dda16fb3a6017',
      userName: 'jon Doe',
      profileImageUrl: 'https://i.ytimg.com/vi/XPTc5XmnHfI/maxresdefault.jpg'
    });
  }

  subscribe(themeId: any, userId: any): Observable <void> {
    const url =   `${this.apiBaseUrl}/my-themes/subscription/subscribe/${themeId}/${userId}`;
    return this.http.post<void>(url, {});
  }
}
