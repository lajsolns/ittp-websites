import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IUser} from '../models/user';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {PaginationResult} from '../models/pagination-result.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  me(): Observable<IUser> {
    const url = `${this.apiBaseUrl}/user/me`;
    return this.http.get<IUser>(url);
  }
  getMyDevicesPaginated(currentPage, pageSize): Observable<PaginationResult> {

    const url = `${this.apiBaseUrl}/store/user-devices`;
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(url, {params: params});
    // return of({
    //   count: 3, data: [{
    //     id: '5bd88a13503dda16fb3a6017', hardwareId: 'Genymotion Custom Android',
    //     manufacturer: 'Genymotion',
    //     model: 'Custom',
    //     operatingSystem: 'Android',
    //     operatingSystemVersion: '8.0.0',
    //     installDate: 'Oct 31, 2018',
    //   }], page: 1
    // });

  }

  getUsersPaginate(currentPage: number, pageSize: number = 10): Observable<PaginationResult> {
    const userUrl = `${this.apiBaseUrl}/my-themes/users`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(userUrl, {params: params});
  }


  tagCount: string;



}
