import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ThemeEditPreview} from '../models/edit-models';
import {Subscriber} from '../models/subcriber-models';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  searchTheme(searchInput): Observable<ThemeEditPreview[]> {
    const url = `${this.apiBaseUrl}/my-themes/search-theme`;
    return this.http.post<any>(url, {searchInput});
  }

  searchSubscriber(themeId, searchInput): Observable<Subscriber[]> {
    const url = `${this.apiBaseUrl}/my-themes/search-subscriber`;
    return this.http.post<any>(url, {searchInput: searchInput, themeId: themeId});
  }
}
