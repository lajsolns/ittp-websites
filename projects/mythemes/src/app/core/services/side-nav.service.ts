import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})

export class SideNavService {
  navState: Observable<boolean>;

  private navStateSubject: BehaviorSubject<boolean>;

  constructor() {
    this.navStateSubject = new BehaviorSubject<boolean>(false);

    this.navState = this.navStateSubject.asObservable();
  }

  toggleNavState() {
    const newState = !this.isOpen;
    this.navStateSubject.next(newState);
  }

  resetNav() {
    this.navStateSubject.next(false);
  }

  get isOpen(): boolean {
    return this.navStateSubject.value;
  }
}
