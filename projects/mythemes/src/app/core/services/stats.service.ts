import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SubscriptionStats} from '../../routes/subscription-stats/subscription-stats.component';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }


  getSubscriptionStats(themeId): Observable<SubscriptionStats> {
    const url = `${this.apiBaseUrl}/my-themes/stats/${themeId}/subscriptions`;
    return this.http.get<SubscriptionStats>(url);
  }

  getRatingStats(themeId): Observable<{labels: string[], data: number[]}> {
    const url = `${this.apiBaseUrl}/my-themes/stats/${themeId}/ratings`;
    return this.http.get<{labels: string[], data: number[]}>(url);
  }

  getTagStats(themeId): Observable< {tagCount: number}> {
    const url = `${this.apiBaseUrl}/my-themes/stats/${themeId}/tags`;
    return this.http.get<{tagCount: number}>(url);
  }
}
