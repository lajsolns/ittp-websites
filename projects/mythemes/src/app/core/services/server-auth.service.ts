import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServerAuthService {
  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }
  signinFirebase(token: string): Observable<UserLoginModel> {
    const url = `${this.apiBaseUrl}/auth`;
    return this.http.post<UserLoginModel>(url, {token});
  }
}

export interface UserLoginModel {
  token: string;
  email: string;
  _id: string;
  name: string;
}

export interface RegistrationErrorResponse {
  code: number;
  msg: string;
  reasons: string[];
}
