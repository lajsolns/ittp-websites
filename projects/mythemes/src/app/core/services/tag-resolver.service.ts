import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {TagEditModel} from '../models/tag-models';
import {TagService} from './tag.service';
import {UserLocationService} from './user-location-service';

@Injectable({
  providedIn: 'root'
})

export class TagsResolverService implements Resolve<any> {

  constructor(private locationService: UserLocationService, private tagService: TagService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TagEditModel> {
    const tagId = route.paramMap.get('tagId');
    const themeId = route.paramMap.get('themeId');
    return this.tagService.getTagEditModel({themeId, tagId});
  }
}
