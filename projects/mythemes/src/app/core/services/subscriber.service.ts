import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Subscriber, SubscriberDetail} from '../models/subcriber-models';
import {PaginationResult} from '../models/pagination-result.model';
import {of} from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  // unsubscribeFromTheme(themeId: string, userId: string): Observable<any> {
  //
  // }

  getSubscribedUsersPaginated(themeId: string, currentPage: number, pageSize: number): Observable<PaginationResult> {

    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());

    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/subscribers`;
    return this.http.get<PaginationResult>(url, {params: params});
  }


  unsubscribeFromTheme(_id: string, userId: any): Observable<string> {
    const url = `${this.apiBaseUrl}/subscription/theme/unsubscribe/${_id}`;
    const body = {userId: userId};
    console.log('about to unsubscribe');
    return this.http.post<string>(url, body);
  }

  getSubscriberDetail(themeId, subscriberId): Observable<SubscriberDetail> {
    const url = `${this.apiBaseUrl}/theme-manager/theme/${themeId}/subscription/${subscriberId}`;
    return this.http.get<SubscriberDetail>(url);

  }

  saveSubscription(themeId, userId, permissions): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-permissions/${userId}`;
    return this.http.post<any>(url, permissions);
    // return this.http.get<any>(url);
  }

  searchSubscriber(themeId: string, currentPage: number, pageSize: number, searchTermUsers: any): Observable<PaginationResult> {
    const params = new HttpParams()
      .set('currentPage', currentPage.toString())
      .set('pageSize', pageSize.toString());
    const url = `${this.apiBaseUrl}/my-themes/${themeId}/search-subscriber/${searchTermUsers}`;
    return this.http.get<PaginationResult>(url);
  }
}
