import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/finally';


@Injectable()
export class LoadInterceptor implements HttpInterceptor {

  private _pending = new Subject();
  private _pendingRequests = 0;

  public constructor(private loadingBarService: SlimLoadingBarService) {
    this._pending.subscribe((progress: any) => {
      if (progress.started) {
        this.loadingBarService.start();
      }
      if (progress.completed) {
        this.loadingBarService.complete();
      }
    });
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this._requestStarted();
    return next.handle(req).finally(
      () => this._requestEnded()
    );
  }

  private _requestStarted() {
    this._pending.next({
      started: this._pendingRequests === 0,
      pendingRequests: ++this._pendingRequests,
    });
  }

  private _requestEnded() {
    this._pending.next({
      completed: this._pendingRequests === 1,
      pendingRequests: --this._pendingRequests,
    });
  }

}
