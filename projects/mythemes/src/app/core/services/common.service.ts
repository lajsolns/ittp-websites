import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {SelectOptionsModel, ThemeHeaderModel} from '../models/common.models';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getThemeHeaderModel(themeId): Observable<ThemeHeaderModel> {
    return this.http.get<ThemeHeaderModel>(`${this.apiBaseUrl}/theme/${themeId}/header`);
  }

  getAllOptions(): Observable<SelectOptionsModel> {
    const url = `${this.apiBaseUrl}/select-option`;
    return this.http.get<SelectOptionsModel>(url);
  }

  sendInvitation(themeId: string, userId: String): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/users/theme-invite`;
    return this.http.post<any>(url, {userId, themeId});
  }
}
