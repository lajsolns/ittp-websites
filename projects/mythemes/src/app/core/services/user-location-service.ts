import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UserPreferencesService} from './user-preferences.service';
import {LatLng} from '../models/common.models';

@Injectable({
  providedIn: 'root'
})
export class UserLocationService {

  constructor(private userPrefs: UserPreferencesService) {
  }

  getUserLocation(): Observable<LatLng> {
    return Observable.create(observer => {
      if (navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          position => {
            const loc = {lat: position.coords.latitude, lng: position.coords.longitude};
            this.userPrefs.setLastLocation(loc);
            observer.next();
            observer.complete();
          },
          error => {
            observer.next(this.userPrefs.getLastLocation());
            observer.complete();
          }
        );
      }
    });
  }
}
