import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MyThemeService {

  constructor( @Inject('API_BASE_URL') private apiBaseUrl, private http: HttpClient) { }


  deleteTheme(themeId: string) {
    return this.http.delete(`${this.apiBaseUrl}/my-themes/theme/delete/${themeId}`);
  }
}
