import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {ServerAuthService, UserLoginModel} from './server-auth.service';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  currentUser: Observable<UserLoginModel>;
  loginState: Observable<boolean>;

  private currentUserSubject: BehaviorSubject<UserLoginModel>;
  private loginStateSubject: BehaviorSubject<boolean>;
  private KEY_USER = 'KEY_USER';

  constructor(private loginService: ServerAuthService, private afAuth: AngularFireAuth, private serverAuth: ServerAuthService) {
    const user = JSON.parse(localStorage.getItem(this.KEY_USER));

    this.currentUserSubject = new BehaviorSubject<UserLoginModel>(user);
    this.loginStateSubject = new BehaviorSubject<boolean>(!!user);

    this.currentUser = this.currentUserSubject.asObservable();
    this.loginState = this.loginStateSubject.asObservable();
  }

  get currentUserValue(): UserLoginModel {
    return this.currentUserSubject.value;
  }

  get isLogggedIn(): boolean {
    return this.loginStateSubject.value;
  }

  login(userLoginModel: UserLoginModel) {
    localStorage.setItem(this.KEY_USER, JSON.stringify(userLoginModel));
    this.currentUserSubject.next(userLoginModel);
    this.loginStateSubject.next(true);
  }

  logout() {
    localStorage.removeItem(this.KEY_USER);
    this.currentUserSubject.next(null);
    this.loginStateSubject.next(false);
    location.reload(true);
  }

  private retrieveToken(): string {
    return localStorage.getItem(this.KEY_USER);
  }

  private saveToken(token: string) {
    localStorage.setItem(this.KEY_USER, token);
  }

  doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        }, err => {
          console.log(err);
          reject(err);
        });
    });
  }

  doTwitterLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.TwitterAuthProvider();
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        }, err => {
          console.log(err);
          reject(err);
        });
    });
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        }, err => {
          console.log(err);
          reject(err);
        });
    });
  }


  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      const user = firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          resolve(user);
        } else {
          reject('No user logged in');
        }
      });
    });
  }

  getFirebaseUserToken() {
    return new Promise<any>((resolve, reject) => {
      const token = firebase.auth().currentUser.getIdToken(true).then((idToken) => {
        if (idToken) {
          console.log('id token:', idToken);
          resolve(idToken);
        }
      }).catch(function (error) {
        resolve(error);
      });
    });
  }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, err => reject(err));
    });
  }

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, err => reject(err));
    });
  }
}
