import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MapMarker, Tag, TagEditModel, TagPreview} from '../models/tag-models';
import {PaginationResult} from '../models/pagination-result.model';
import {of} from 'rxjs';
import {UserInfo} from '../models/theme-models';
import {environment} from '../../../environments/environment';
import {FileOperationModel} from '../../routes/file-operations/file-operations.component';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private  http: HttpClient, @Inject('API_BASE_URL') private apiBaseUrl) {
  }

  getFileOperations(themeId: string): Observable<FileOperationModel[]> {
    return this.http.get<FileOperationModel[]>(`${this.apiBaseUrl}/my-themes/theme/${themeId}/file-operations`);
  }

  uploadTags(themeId: string, fileToUpload: FormData) {
    const uploadUrl = `${this.apiBaseUrl}/themes/${themeId}/themes/${themeId}/file-operations`;
    return this.http.get<any[]>(`${environment.apiBaseUrl}/themes/${themeId}/file-operations`);
  }

  getTagById(themeId: string): Observable<TagPreview[]> {
    // const tagsUrls = `${environment.apiBaseUrl}/my-themes/tagProperties/${themeId}/tags`;
    // return this.http.get<Tag[]>(tagsUrls);
    return of([{_id: '', latitude: '', longitude: '', updatedAt: '', updatedBy: ''}]);
  }


  deleteAllTags(themeId: string) {
    const tagsUrls = `${environment.apiBaseUrl}/themes/${themeId}/tags`;
    return this.http.delete(tagsUrls);
  }

  getTagsPaginate(themeId: string, currentPage: number, pageSize: number = 30): Observable<PaginationResult> {
    const tagsUrls = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tags`;
    const params = new HttpParams()
      .set('page', currentPage.toString())
      .set('pageSize', pageSize.toString());
    return this.http.get<PaginationResult>(tagsUrls, {params: params});
  }

  getOneTag(themeId: string, tagId: string): Observable<TagPreview> {
    const singleTagUrl = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-edit-model/tag/${tagId}`;
    return this.http.get<TagPreview>(singleTagUrl);

  }

  getUserInfo(id): Observable<UserInfo> {
    return of({_id: '65456464', email: 'o@email.com', profileImageUrl: 'dd'});
  }

  getNewTag(themeId: string, location: number[]): Observable<any> {
    const lat = location[0].toString();
    const lon = location[1].toString();
    const params = new HttpParams()
      .set('latitude', lat)
      .set('longitude', lon)
      .set('themeId', themeId.toString());

    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tags/new-tag`;
    return this.http.get<any>(url, {params: params});
  }

  getTagsInBounds(params: { bottomLeft, upperRight, themeId: string }): Observable<{markers: MapMarker[], iconUrl: string}> {
    const { bottomLeft, upperRight, themeId} = params;
    const url = `${environment.apiBaseUrl}/my-themes/theme/${themeId}/tags/map`;
    return this.http.post<{markers: MapMarker[], iconUrl: string}>(url, {bottomLeft, upperRight});
  }

  uploadUpdatedTags(themeId: string, fileToUpload: FormData) {
    const uploadUrl = `${this.apiBaseUrl}/my-themes/themes/${themeId}/file-operations`;
    return this.http.post(uploadUrl, fileToUpload, {params: {type: 'updated'}});
  }

  saveTag(themeId: string, tag: Tag): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tags/${tag._id}/save`;
    return this.http.post<any>(url, tag);
  }

  createTag(themeId: string, tag: Tag): Observable<any> {
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tags/create`;
    return this.http.post<any>(url, tag);

  }

  deleteTag(themeId, tagId) {
    const tagsUrl = `${this.apiBaseUrl}/themes/${themeId}/tags/${tagId}`;
    return this.http.delete(tagsUrl);
  }

  downloadTagFile(themeId) {
    const url = `${this.apiBaseUrl}/theme/${themeId}/file-download/template`;
    window.location.href = url;
  }

  downloadExistingTags(themeId) {
    const url = `${this.apiBaseUrl}/theme/${themeId}/file-download/tags`;
    window.location.href = url;
  }

  getTagEditModel(params: {themeId: string, tagId: string}): Observable<TagEditModel> {
    const {themeId, tagId} = params;
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-edit-model/tag/${tagId}`;
    return this.http.get<TagEditModel>(url);
  }

  getNewTagEditModel(params: {themeId: string, latitude: number, longitude: number}): Observable<TagEditModel> {
    const {themeId, latitude, longitude} = params;
    const query = new HttpParams()
      .set('latitude', latitude.toString())
      .set('longitude', longitude.toString());
    const url = `${this.apiBaseUrl}/my-themes/theme/${themeId}/tag-edit-model/new`;
    return this.http.get<TagEditModel>(url, {params: query});
  }
}
