import { Injectable } from '@angular/core';
import {LatLng} from '../models/common.models';
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserPreferencesService {
  private KEY = 'user-preference';
  private prefsModel: PrefsModel;

  constructor() {
    this.init();
  }

  getLastLocation() {
    return this.prefsModel.lastLocation;
  }

  setLastLocation(loc: {lat: number, lng: number}) {
    this.prefsModel.lastLocation = loc;
    this.save();
  }

  get(): Observable<any> {
    return of(JSON.parse(sessionStorage.getItem('user-preference')));
  }

  private save() {
    sessionStorage.setItem(this.KEY, JSON.stringify(this.prefsModel));
  }

  private init() {
    this.prefsModel = (JSON.parse(sessionStorage.getItem(this.KEY))) || new PrefsModel();
  }
}

export class PrefsModel {
  lastLocation: LatLng = {lat: 40.7484, lng: -73.9857};
}
