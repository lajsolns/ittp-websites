import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TagProperty} from '../models/theme-models';
import {ThemeEditService} from '../services/theme-edit.service';

@Injectable({
  providedIn: 'root'
})
export class TagPropertiesResolver implements Resolve<TagProperty[]> {
  constructor(private themeEditSvc: ThemeEditService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TagProperty[]> {
    const themeId = route.paramMap.get('themeId');
    return this.themeEditSvc.getThemeTagProperties(themeId);
  }

}
