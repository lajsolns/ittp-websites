import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {SubscriberService} from '../services/subscriber.service';
import {SubscriberDetail} from '../models/subcriber-models';

@Injectable({
  providedIn: 'root'
})
export class SubscriberResolver implements Resolve<SubscriberDetail> {
 themeId;
 subscriberId;
  constructor(private subscriberService: SubscriberService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SubscriberDetail> {
    this.themeId = route.paramMap.get('themeId');
    this.subscriberId = route.paramMap.get('subscriberId');
    return this.subscriberService.getSubscriberDetail(this.themeId, this.subscriberId);
  }
}
