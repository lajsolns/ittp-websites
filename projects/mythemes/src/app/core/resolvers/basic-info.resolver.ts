import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeEditService} from '../services/theme-edit.service';
import {ThemeBasicInfo} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class BasicInfoResolver implements Resolve<ThemeBasicInfo> {
  constructor(private themeEditSvc: ThemeEditService, private toastr: ToastrManager) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeBasicInfo> {
    const themeId = route.paramMap.get('themeId');
    return this.themeEditSvc.getThemeBasicInfo(themeId);
  }

}
