import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SearchService} from '../services/search.service';
import {Subscriber} from '../models/subcriber-models';

@Injectable({
  providedIn: 'root'
})
export class SubscriberSearchResolver implements Resolve<Subscriber[]> {

  themeId;

  constructor(private router: Router, private searchSvc: SearchService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Subscriber[]> {
    const searchInput = route.paramMap.get('searchInput');
    this.themeId = route.paramMap.get('themeId');

    return this.searchSvc.searchSubscriber(this.themeId, searchInput);
  }
}
