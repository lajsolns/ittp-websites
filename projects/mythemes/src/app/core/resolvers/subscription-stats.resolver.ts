import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {StatsService} from '../services/stats.service';
import {SubscriptionStats} from '../../routes/subscription-stats/subscription-stats.component';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionStatsResolver {

  constructor(private statService: StatsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SubscriptionStats> {
    const themeId = route.paramMap.get('themeId');
    return this.statService.getSubscriptionStats(themeId);

  }
}
