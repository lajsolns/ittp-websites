import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CommonService} from '../services/common.service';
import {SelectOptionsModel} from '../models/common.models';

@Injectable({
  providedIn: 'root'
})
export class SelectOptionResolverService {

  constructor(private commonService: CommonService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SelectOptionsModel> {
    return this.commonService.getAllOptions();
  }
}
