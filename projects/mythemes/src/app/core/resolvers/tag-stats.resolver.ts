import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {StatsService} from '../services/stats.service';


@Injectable({
  providedIn: 'root'
})
export class TagStatsResolver {

  constructor(private statService: StatsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{tagCount: number}> {
    const themeId = route.paramMap.get('themeId');
    return this.statService.getTagStats(themeId);
  }
}
