import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {StatsService} from '../services/stats.service';


@Injectable({
  providedIn: 'root'
})
export class RatingStatsResolver {

  constructor(private statService: StatsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{labels: string[], data: number[]}> {
    const themeId = route.paramMap.get('themeId');
    return this.statService.getRatingStats(themeId);
  }
}
