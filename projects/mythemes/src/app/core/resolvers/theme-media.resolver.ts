import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ThemeMedia} from '../models/theme-models';
import {ThemeEditService} from '../services/theme-edit.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeMediaResolver implements Resolve<ThemeMedia> {
  constructor(private themeEditSvc: ThemeEditService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeMedia> {
    const themeId = route.paramMap.get('themeId');
    return this.themeEditSvc.getMediaById(themeId);
  }

}
