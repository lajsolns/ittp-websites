import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

import {TagService} from '../services/tag.service';

@Injectable({
  providedIn: 'root'
})
export class FileOperationsResolver {

  constructor(private tagService: TagService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const themeId = route.paramMap.get('themeId');
      return this.tagService.getFileOperations(themeId);
  }
}
