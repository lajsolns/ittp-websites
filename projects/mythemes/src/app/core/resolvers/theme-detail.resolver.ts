import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {ThemeDetail} from '../models/theme-detail.model';

@Injectable({
  providedIn: 'root'
})
export class ThemeDetailResolver {

  constructor(private themeEditService: ThemeEditService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeDetail> {
    const themeId = route.paramMap.get('themeId');
    return this.themeEditService.getDetailById(themeId);
  }
}
