import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {TagService} from '../services/tag.service';
import {TagEditModel, TagPreview} from '../models/tag-models';
import {UserPreferencesService} from '../services/user-preferences.service';

@Injectable({
  providedIn: 'root'
})
export class NewTagResolver {

  constructor(private tagService: TagService, private userPrefs: UserPreferencesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TagEditModel> {
    const themeId = route.paramMap.get('themeId');
    const {lat: latitude, lng: longitude} = this.userPrefs.getLastLocation();
    return this.tagService.getNewTagEditModel({themeId, longitude, latitude});
  }
}
