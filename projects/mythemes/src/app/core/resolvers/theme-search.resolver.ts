import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Observable} from 'rxjs';
import {SearchService} from '../services/search.service';
import {UserDetails} from '../../../../../admin-site/src/app/models/themes.model';
import {UserService} from '../../../../../admin-site/src/app/ui-components/user/users-details/user-details.service';
import {ThemeEditPreview} from '../models/edit-models';

@Injectable({
  providedIn: 'root'
})
export class ThemeSearchResolver implements Resolve<ThemeEditPreview[]> {

  constructor(private router: Router, private searchSvc: SearchService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeEditPreview[]> {
    const searchInput = route.paramMap.get('searchInput');
    return this.searchSvc.searchTheme(searchInput);
  }
}
