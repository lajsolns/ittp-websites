import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TagService} from '../services/tag.service';
import {TagEditModel, TagPreview} from '../models/tag-models';

@Injectable({
  providedIn: 'root'
})
export class TagResolver {

  constructor(private tagService: TagService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TagEditModel> {
    const tagId = route.paramMap.get('tagId');
    const themeId = route.paramMap.get('themeId');
    return this.tagService.getTagEditModel({themeId, tagId});
  }
}
