import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {TagService} from '../services/tag.service';
import {UserInfo} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class UserInfoResolver {

  constructor(private tagService: TagService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserInfo> {
    const themeId = route.paramMap.get('themeId');
    return this.tagService.getUserInfo(themeId);
  }
}
