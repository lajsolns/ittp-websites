import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {Theme} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class ExistingThemeResolverService {

  constructor(private themeEditService: ThemeEditService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Theme> {
    const themeId = route.paramMap.get('themeId');
    return this.themeEditService.getThemeById(themeId);
  }
}
