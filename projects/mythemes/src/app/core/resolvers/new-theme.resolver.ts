import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ThemeEditService} from '../services/theme-edit.service';
import {ThemeBasicInfo} from '../models/theme-models';

@Injectable({
  providedIn: 'root'
})
export class NewThemeResolverService {

  constructor(private themeEditService: ThemeEditService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ThemeBasicInfo> {
    return this.themeEditService.getNewBasicInfo();
  }
}
