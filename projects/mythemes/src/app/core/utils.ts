import { ActivatedRoute } from "@angular/router";

export const getThemeIdFromParentRoute = (route: ActivatedRoute) => {
    return route.parent.snapshot.paramMap.get('themeId');
};