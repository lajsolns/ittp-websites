export class  SubscriberDetail {
  payment: Payment;
  user: User;
  subscription: Subscription;
}

export class Payment {
  id: string;
  price: string;
  currency: string;
}

export class User {
  id: string;
  name: string;
  profileImageUrl: string;
}

export class Subscription {
  id: string;
  tagPermissions: {
    canDelete: boolean;
    canEdit: boolean;
    canCreate: boolean;
  };
}


export class Subscriber {
          id: string;
          canCreate: true;
          canDelete: true;
          canEdit: true;
          themeId: string;
          name: string;
}
