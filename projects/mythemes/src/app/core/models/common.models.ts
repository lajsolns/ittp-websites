import {SelectOptionUnit} from './edit-models';

export interface SelectOptionsModel {
  distance: SelectOptionUnit[];
  time: SelectOptionUnit[];
  currency: SelectOptionUnit[];
  regions: SelectOptionUnit[];
  categories: SelectOptionUnit[];
  tagPropertyType: SelectOptionUnit[];
}

export interface ThemeHeaderModel {
  id: string;
  name: string;
  themeImageUrl: string;
}

export interface StorageSchema {
  createdBy: string;
  createdById: string;
  updatedBy: string;
  updatedById: string;
}

export interface LatLng {
  lat: number;
  lng: number;
}


