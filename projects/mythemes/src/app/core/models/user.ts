export interface IUser {
  email: string;
  name: string;
  isAdmin: boolean;
  profileImageUrl: string;
}

export interface SubscriptionStatus {
  isPaidTheme: boolean;
  isSubscribed: boolean;
  isPaymentRequired: boolean;
  info: Info;
}

interface Info {
  subscribedAt: number;
  $init: boolean;
  canDelete: false;
  canEdit: false;
  canCreate: false;
}

export class DeviceInfo {
  _id: string;
  hardwareId: string;
  operatingSystem: string;
  manufacturer: string;
  model: string;
  operatingSystemVersion: string;
  installDate: string;
}
