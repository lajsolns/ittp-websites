export interface ThemeEditPreview {
  _id: string;
  name: string;
  description: string;
  tagCount: string;
  themeImageUrl: string;
}

export interface SelectOptionUnit {
  value: number;
  order: number;
  description: string;
  label: string;
}

export class ThemeEditPreview {
  _id: string;
  name: string;
  description: string;
  tagCount: string;
  themeImageUrl: string;
}
