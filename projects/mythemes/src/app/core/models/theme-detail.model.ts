export interface  ThemeDetail {
  _id: string;
  name: string;
  description: string;
  themeImageUrl: string;
  keywords: string;
  createdBy: string;
  createdAt: string;
  updatedBy: string;
  updatedAt: string;
  tagCount: string;
  price: string;
  tagIconImageUrl: string;
  visibleInStore: boolean;
  activeSubscribers: string;
  totalNumberOfRatings: string;
  defaultTagSilence: string;
  defaultNotificationDistance: string;
  color: string;
  allowFastTags: boolean;
  allowMovingTags: boolean;
  allowCrowdSourcing: boolean;
  region: string;
  useAccessList: boolean;
  category: string;
}


