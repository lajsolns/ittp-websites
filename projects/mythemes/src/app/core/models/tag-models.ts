import {StorageSchema} from './common.models';
import {TagProperty} from './theme-models';

export class TagPreview {
  _id: string;
  latitude: string;
  longitude: string;
  updatedBy: string;
  updatedAt: string;
}

export interface Location {
  type: 'Point';
  coordinates: number[];
}

export class MapMarker {
  id: string;
  latitude: number;
  longitude: number;
}

export interface Tag {
  _id: string;
  themeId: string;
  createdAt: string;
  updatedAt: string;
  location: Location;
  active: boolean;
  propertyValues: {
    normalizedName: string;
    value: string;
  }[];
  _storage: StorageSchema;
}

export interface TagEditModel {
  tagProperties: TagProperty[];
  tag: Tag;
  isNew: boolean;
}

