import {ProfileComponent} from './core/ui-components/profile/profile.component';
import {RegisterComponent} from './routes/register/register.component';
import {ThemeDetailComponent} from './routes/theme-detail/theme-detail.component';
import {NotFound404Component} from './routes/not-found404/not-found404.component';
import {TagPropertiesEditorComponent} from './core/ui-components/tag-properties-editor/tag-properties-editor.component';
import {ThemeSearchComponent} from './routes/theme-search/theme-search.component';
import {ThemeListComponent} from './routes/theme-list/theme-list.component';
import {NewThemeComponent} from './routes/theme-new/new-theme.component';
import {NewThemeResolverService} from './core/resolvers/new-theme.resolver';
import {SelectOptionResolverService} from './core/resolvers/select-option.resolver';
import {ThemeDetailResolver} from './core/resolvers/theme-detail.resolver';
import {ThemeUsersComponent} from './routes/theme-users/theme-users.component';
import {BasicInfoEditComponent} from './core/ui-components/basic-info-edit/basic-info-edit.component';
import {TagPropertiesEditComponent} from './core/ui-components/tag-properties-edit/tag-properties-edit.component';
import {SettingsEditComponent} from './core/ui-components/settings-edit/settings-edit.component';
import {MediaEditComponent} from './core/ui-components/media-edit/media-edit.component';
import {TagListComponent} from './routes/tag-list/tag-list.component';
import {TagEditComponent} from './routes/tag-edit/tag-edit.component';
import {TagResolver} from './core/resolvers/tag.resolver.service';
import {TagNewComponent} from './routes/tag-new/tag-new.component';
import {NewTagResolver} from './core/resolvers/new-tag.resolver.service';
import {FileOperationsComponent} from './routes/file-operations/file-operations.component';
import {TagMapComponent} from './routes/tag-map/tag-map.component';
import {AuthGuard} from './core/auth.guard';
import {ThemeSearchResolver} from './core/resolvers/theme-search.resolver';
import {BasicInfoResolver} from './core/resolvers/basic-info.resolver';
import {TagPropertiesResolver} from './core/resolvers/tag-properties.resolver';
import {ThemeSettingsResolver} from './core/resolvers/theme-settings.resolver';
import {LoginComponent} from './routes/login/login.component';
import {InvitationComponent} from './routes/invitation/invitation.component';
import {SubscriberDetailsComponent} from './routes/subscriber-details/subscriber-details.component';
import {WishlistComponent} from './routes/wishlist/wishlist.component';
import {TagStatsComponent} from './routes/tag-stats/tag-stats.component';
import {SubscriptionStatsComponent} from './routes/subscription-stats/subscription-stats.component';
import {WishlistUserDetailsComponent} from './routes/wishlist-user-details/wishlist-user-details.component';
import {AccessListComponent} from './routes/access-list/access-list.component';
import {SubscriberSearchComponent} from './routes/subscriber-search/subscriber-search.component';
import {SubscriberSearchResolver} from './core/resolvers/subscriber-search.resolver';
import {SubscriberResolver} from './core/resolvers/subscriber.resolver';
import {SubscriptionStatsResolver} from './core/resolvers/subscription-stats.resolver';
import {RatingStatsComponent} from './routes/rating-stats/rating-stats.component';
import {RatingStatsResolver} from './core/resolvers/rating-stats.resolver';
import {TagStatsResolver} from './core/resolvers/tag-stats.resolver';
import {AccessComponent} from "./routes/access/access.component";

export const routes = [
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'tagProperties-detail', component: ThemeDetailComponent},
  {path: '404', component: NotFound404Component},
  {path: 'tag', component: TagPropertiesEditorComponent},
  {path: '', redirectTo: '/my-themes', pathMatch: 'full'},
  {path: 'access', component: AccessComponent},
  {path: 'search-theme/:searchInput', component: ThemeSearchComponent, resolve: {searchInput: ThemeSearchResolver}},
  {
    path: 'my-themes/:themeId/search-subscriber/:searchInput',
    component: SubscriberSearchComponent,
    resolve: {searchInput: SubscriberSearchResolver}
  },
  {path: 'my-themes', component: ThemeListComponent, canActivate: [AuthGuard]},
  {
    path: 'my-themes/new',
    component: NewThemeComponent,
    canActivate: [AuthGuard],
    resolve: {basicInfo: NewThemeResolverService, selectOptions: SelectOptionResolverService}
  },
  {
    path: 'my-themes/:themeId/details',
    component: ThemeDetailComponent,
    canActivate: [AuthGuard],
    resolve: {themeDetail: ThemeDetailResolver}
  },
  {
    path: 'my-themes/:themeId/users',
    component: ThemeUsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my-themes/:themeId/edit',
    component: BasicInfoEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      basicInfo: BasicInfoResolver,
      selectOptions: SelectOptionResolverService
    }
  },
  {
    path: 'my-themes/:themeId/edit/basic-info',
    component: BasicInfoEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      basicInfo: BasicInfoResolver,
      selectOptions: SelectOptionResolverService,
    }
  },
  {
    path: 'my-themes/:themeId/edit/tag-properties',
    component: TagPropertiesEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      tagProperties: TagPropertiesResolver,
      selectOptions: SelectOptionResolverService,
    }
  },
  {
    path: 'my-themes/:themeId/edit/settings',
    component: SettingsEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      settings: ThemeSettingsResolver,
      selectOptions: SelectOptionResolverService
    }
  },
  {
    path: 'my-themes/:themeId/edit/media',
    component: MediaEditComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'my-themes/:themeId/wish-list',
    component: WishlistComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'my-themes/:themeId/tags',
    component: TagListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my-themes/:themeId/tags/:tagId/edit',
    component: TagEditComponent,
    canActivate: [AuthGuard],
    resolve: {tagEditModel: TagResolver}
  },
  {
    path: 'my-themes/:themeId/users/:subscriberId/subscriber',
    component: SubscriberDetailsComponent,
    canActivate: [AuthGuard],
    resolve: {subscriberDetails: SubscriberResolver}
  },
  {
    path: 'my-themes/:themeId/tags/new',
    component: TagNewComponent,
    canActivate: [AuthGuard],
    resolve: {tagEditModel: NewTagResolver}
  },

  {
    path: 'my-themes/:themeId/tags/file-operations',
    component: FileOperationsComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'my-themes/:themeId/tags/tag-map',
    component: TagMapComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my-themes/:themeId/invitation',
    component: InvitationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my-themes/:themeId/rating-stats',
    component: RatingStatsComponent,
    canActivate: [AuthGuard],
    resolve: {ratingStats: RatingStatsResolver}
  },
  {
    path: 'my-themes/:themeId/wish-list',
    component: WishlistComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'my-themes/:themeId/wish-list/:userId/details',
    component: WishlistUserDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'my-themes/:themeId/tag-stats',
    component: TagStatsComponent,
    canActivate: [AuthGuard],
    resolve: {tagStats: TagStatsResolver}
  },
  {
    path: 'my-themes/:themeId/access-list',
    component: AccessListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'my-themes/:themeId/subscription-stats',
    component: SubscriptionStatsComponent,
    canActivate: [AuthGuard],
    resolve: {subscriptionStats: SubscriptionStatsResolver}
  },
  {path: '**', component: NotFound404Component},
  {path: '**', redirectTo: '/404'}
];
