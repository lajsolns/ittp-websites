import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbDropdownModule, NgbModalModule, NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {ChartsModule} from 'ng2-charts';
import {MatProgressBarModule, MatTabsModule} from '@angular/material';
import {ToastrModule} from 'ng6-toastr-notifications';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {SidebarModule} from 'ng-sidebar';
import {RouterModule} from '@angular/router';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AgmCoreModule} from '@agm/core';
import {AuthServiceConfig, SocialLoginModule} from 'angular-6-social-login-v2';
import {ColorPickerModule} from 'ngx-color-picker';
import {TagNewComponent} from './routes/tag-new/tag-new.component';
import {TagEditComponent} from './routes/tag-edit/tag-edit.component';
import {TagEditorComponent} from './routes/tag-editor/tag-editor.component';
import {TagListComponent} from './routes/tag-list/tag-list.component';
import {ThemeHeaderComponent} from './core/ui-components/theme-header/theme-header.component';
import {TagPropertiesEditComponent} from './core/ui-components/tag-properties-edit/tag-properties-edit.component';
import {FileOperationsComponent} from './routes/file-operations/file-operations.component';
import {TagsComponent} from './routes/tags/tags.component';
import {MediaEditComponent} from './core/ui-components/media-edit/media-edit.component';
import {SettingsEditComponent} from './core/ui-components/settings-edit/settings-edit.component';
import {MyThemesComponent} from './routes/my-themes/my-themes.component';
import {BasicInfoEditorComponent} from './core/ui-components/basic-info-editor/basic-info-editor.component';
import {BasicInfoEditComponent} from './core/ui-components/basic-info-edit/basic-info-edit.component';
import {ThemeUsersComponent} from './routes/theme-users/theme-users.component';
import {NewThemeComponent} from './routes/theme-new/new-theme.component';
import {ThemeEditorComponent} from './routes/theme-editor/theme-editor.component';
import {SettingsEditorComponent} from './core/ui-components/settings-editor/settings-editor.component';
import {TagPropertiesEditorComponent} from './core/ui-components/tag-properties-editor/tag-properties-editor.component';
import {AddTagModalComponent} from './routes/add-tag-modal/add-tag-modal.component';
import {DeleteModalComponent} from './routes/delete-modal/delete-modal.component';
import {TagMapComponent} from './routes/tag-map/tag-map.component';
import {ThemePriceComponent} from './routes/theme-price/theme-price.component';
import {ThemeDetailComponent} from './routes/theme-detail/theme-detail.component';
import {ThemeListComponent} from './routes/theme-list/theme-list.component';
import {SnapLeftNavComponent} from './core/ui-components/snap-left-nav/snap-left-nav.component';
import {MainHeaderComponent} from './core/ui-components/main-header/main-header.component';
import {ThemeEditTabsComponent} from './core/ui-components/theme-edit-tabs/theme-edit-tabs.component';
import {environment} from '../environments/environment';
import {TokenErrorResponseInterceptor} from './core/http-interceptors/token-error-reponse-interceptor';
import {ErrorResponseInterceptor} from './core/http-interceptors/error-body-response-interceptor';
import {LoadInterceptor} from './core/services/loading-bar-interceptor';
import {ApiRequestTokenInterceptor} from './core/http-interceptors/api-request-token-interceptor';
import {FilterByPipe} from './pipes/filter-by.pipe';
import {NotFound404Component} from './routes/not-found404/not-found404.component';
import {SideNavDetailsComponent} from './core/ui-components/side-nav-details/side-nav-details.component';
import {ThemeSearchComponent} from './routes/theme-search/theme-search.component';
import {RegisterComponent} from './routes/register/register.component';
import {ProfileComponent} from './core/ui-components/profile/profile.component';
import {SearchBarComponent} from './core/ui-components/search-bar/search-bar.component';
import {routes} from './app.routes';
import {LoginComponent} from './routes/login/login.component';
import {ProfileHeaderComponent} from './core/ui-components/profileHeader/profile-header.component';
import {FooterComponent} from './core/ui-components/footer/footer.component';
import {InvitationComponent} from './routes/invitation/invitation.component';
import {SubscriberDetailsComponent} from './routes/subscriber-details/subscriber-details.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';

import {provideConfig} from '../../../theme-store/src/app/app.module';
import {FileUploadModule} from 'ng2-file-upload';
import {MatSidenavModule} from '@angular/material/sidenav';
import {WishlistComponent} from './routes/wishlist/wishlist.component';
import {TagStatsComponent} from './routes/tag-stats/tag-stats.component';
import {SubscriptionStatsComponent} from './routes/subscription-stats/subscription-stats.component';
import {WishlistUserDetailsComponent} from './routes/wishlist-user-details/wishlist-user-details.component';
import {FileOperationRowComponent} from './routes/file-operations/file-operation-row/file-operation-row.component';
import {MomentModule} from 'ngx-moment';
import {AccessListComponent} from './routes/access-list/access-list.component';
import {SubscriberSearchComponent} from './routes/subscriber-search/subscriber-search.component';
import {AccessComponent} from './routes/access/access.component';
import {MyThemeHeaderComponent} from './routes/my-theme-header/my-theme-header.component';
import {ReviewChartComponent} from './core/ui-components/review-chart/review-chart.component';
import { RatingStatsComponent } from './routes/rating-stats/rating-stats.component';
import { AccessListModalComponent } from './core/ui-components/access-list-modal/access-list-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    ProfileHeaderComponent,
    LoginComponent,
    FooterComponent,
    ThemeDetailComponent,
    NotFound404Component,
    SearchBarComponent,
    ThemeListComponent,
    FilterByPipe,
    RegisterComponent,
    SnapLeftNavComponent,
    MainHeaderComponent,
    SideNavDetailsComponent,
    ProfileComponent,
    ThemeSearchComponent,
    TagPropertiesEditorComponent,
    SettingsEditorComponent,
    ThemeEditorComponent,
    NewThemeComponent,
    ThemeUsersComponent,
    ThemeEditTabsComponent,
    BasicInfoEditComponent,
    BasicInfoEditorComponent,
    MyThemesComponent,
    SettingsEditComponent,
    MediaEditComponent,
    ThemePriceComponent,
    TagPropertiesEditComponent,
    DeleteModalComponent,
    FileOperationsComponent,
    TagsComponent,
    ThemeHeaderComponent,
    MyThemeHeaderComponent,
    TagListComponent,
    TagEditorComponent,
    AddTagModalComponent,
    TagMapComponent,
    TagEditComponent,
    TagNewComponent,
    AddTagModalComponent,
    DeleteModalComponent,
    InvitationComponent,
    SubscriberDetailsComponent,
    WishlistComponent,
    TagStatsComponent,
    SubscriptionStatsComponent,
    WishlistUserDetailsComponent,
    SubscriberDetailsComponent,
    FileOperationRowComponent,
    AccessListComponent,
    SubscriberSearchComponent,
    AccessComponent,
    MyThemeHeaderComponent,
    ReviewChartComponent,
    RatingStatsComponent,
    AccessListModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgxPaginationModule,
    NgbRatingModule,
    NgbDropdownModule,
    NgbModalModule,
    MultiselectDropdownModule,
    BrowserAnimationsModule,
    MultiselectDropdownModule,
    ColorPickerModule,
    MatProgressBarModule,
    MatTabsModule,
    FileUploadModule,
    MatSidenavModule,
    MomentModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    SlimLoadingBarModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey
    }),
    ToastrModule.forRoot(),
    SidebarModule.forRoot(),
    RouterModule.forRoot(routes, {enableTracing: false}),

  ], entryComponents: [DeleteModalComponent, AddTagModalComponent, AccessListModalComponent],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    {
      provide: 'API_BASE_URL',
      useValue: environment.apiBaseUrl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiRequestTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenErrorResponseInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorResponseInterceptor,
      multi: true
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadInterceptor,
      multi: true
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
