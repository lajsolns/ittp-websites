import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MyThemeService} from '../../core/services/mytheme.service';

@Component({
  selector: 'itt-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  @Input() themeId;
  @Input() itemName;
  constructor(  public activeModal: NgbActiveModal, private myThemeService: MyThemeService) { }

  ngOnInit() {
  }

  // this.activeModal.close('Close click');

  // @ts-ignore
  deleteTheme(themeId: string): boolean {
    this.myThemeService.deleteTheme(themeId).subscribe(res => {
        this.activeModal.close('Ok');
        return true;
      },
      err => {
        this.activeModal.close('Error');
        return false;
      });
  }
}
