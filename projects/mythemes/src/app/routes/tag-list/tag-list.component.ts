import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Theme} from '../../core/models/theme-models';
import {TagPreview} from '../../core/models/tag-models';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute, Router} from '@angular/router';
import {TagService} from '../../core/services/tag.service';
import {UserService} from '../../core/services/user.service';

@Component({
  selector: 'itt-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {
  @Output() pageChange: EventEmitter<number>;


  imageUrl: string;
  tags: TagPreview[] = [];
  themeId: string;
  theme: Theme;
  pageSize = 5;
  currentPage = 1;
  count = 10;
  searchTerm;
  tagFilter;
  isOpened = false;


  constructor(private toastr: ToastrManager,
              private userService: UserService,
              private tagService: TagService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.themeId = activatedRoute.snapshot.params['themeId'];
  }

  findTag() {
    if (this.searchTerm) {
      this.tagService.getOneTag(this.themeId, this.searchTerm).subscribe(result => {
        this.count = 1;
        this.tags = [];
        this.tags.push(result);
      }, err => {
        this.toastr.errorToastr('Tag not found, check ID well', 'Error');

      });
    } else {
      this.toastr.errorToastr('Search cant be empty', 'Error');
    }
  }

  ngOnInit() {

    this.loadData();
    // this.tagFilter = {id: ''};
  }

  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.navigateToPage();
    this.loadData();
  }

  public navigateToPage() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        page: this.currentPage,
      }
    });
  }

  refresh() {
    this.loadData();
  }

  private loadData() {
    this.tagService.getTagsPaginate(this.themeId, this.currentPage, this.pageSize).subscribe(result => {
      this.count = result.count;
      this.tags = result.data;
    });
  }

  delete(tag, index) {
    this.tagService.deleteTag(this.themeId, tag._id).subscribe(result => {
      this.toastr.successToastr('Deleted', 'Success');
      this.loadData();
    }, err => {
      this.toastr.errorToastr('Tag could not be deleted,contact admin to check if you have permission and try again', 'Error');
    });
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
