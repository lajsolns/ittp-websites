import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MyThemeService} from '../../core/services/mytheme.service';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-rating-stats',
  templateUrl: './rating-stats.component.html',
  styleUrls: ['./rating-stats.component.css']
})
export class RatingStatsComponent implements OnInit {
  themeId;
  ratingStats;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels: string[] = ['Today', 'This Week', 'This Month'];
  barChartType = 'bar';
  barChartLegend = true;
  barChartData = [
    {
      data: [10, 50, 100],
      label: 'Subscribers'
    },
    {
      data: [2, 10, 20],
      label: 'Unsubscribers'
    }
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private myThemeService: MyThemeService,
              private toastr: ToastrManager,
              private router: Router) {
    this.themeId = activatedRoute.snapshot.paramMap.get('themeId');
    this.ratingStats = activatedRoute.snapshot.data['ratingStats'];
    this.ratingStats = this.ratingStats.stats.ratings;
  }

  ngOnInit() {

    console.log('data ', this.ratingStats);
  }

  chartClicked($event: any) {
    //  todo
  }

  chartHovered($event: any) {
    //  todo
  }


}

export class BarChartData {
  label: string[];
  data: { subscribers: number, unsubscribers: number }[];
}

export class GeneralStats {
  tagCount: number;
  activeSubscribers: number;
  rating: { average: number, count: number };
  barChartData: BarChartData;
}
