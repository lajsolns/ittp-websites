import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'itt-add-tag-modal',
  templateUrl: './add-tag-modal.component.html',
  styleUrls: ['./add-tag-modal.component.css']
})
export class AddTagModalComponent implements OnInit {
  @Input() themeId;
  @Input() lat;
  @Input() lng;

  constructor(private router: Router, public activeModal: NgbActiveModal) {
  }

  ngOnInit() {

  }

  goToTagEdit() {
    this.router.navigate(['my-themes', this.themeId, 'tags', 'new', {lat: this.lat, lng: this.lng}]);
    this.activeModal.close();

  }
}
