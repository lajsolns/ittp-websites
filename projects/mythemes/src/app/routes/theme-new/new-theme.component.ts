import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {ThemeEditService} from '../../core/services/theme-edit.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ThemeBasicInfo} from '../../core/models/theme-models';
import {SelectOptionsModel} from '../../core/models/common.models';

@Component({
  selector: 'itt-theme-creator',
  templateUrl: './new-theme.component.html',
  styleUrls: ['./new-theme.component.css']
})
export class NewThemeComponent implements OnInit {
  basicInfo: ThemeBasicInfo;
  selectOptions: SelectOptionsModel;
  isOpened = false;

  constructor(
    activatedRoute: ActivatedRoute,
    private router: Router,
    private themeEditService: ThemeEditService,
    public toastr: ToastrManager,
  ) {
    this.basicInfo = activatedRoute.snapshot.data['basicInfo'];
    this.selectOptions = activatedRoute.snapshot.data['selectOptions'];
  }

  ngOnInit() {
    console.log('options', this.selectOptions);
    console.log('basic info', this.basicInfo);
  }

  saveTheme(value: any) {
    this
      .themeEditService
      .createTheme(value)
      .subscribe(id => {
        this.toastr.successToastr('Theme Created');
        this.router.navigate(['my-themes', id, 'edit']);
      }, error1 => this.toastr.errorToastr('Error saving tagProperties'));
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
