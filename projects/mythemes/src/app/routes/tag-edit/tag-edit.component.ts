import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TagService} from '../../core/services/tag.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IUser} from '../../core/models/user';
import {TagEditModel} from '../../core/models/tag-models';


@Component({
  selector: 'itt-tag-edit',
  templateUrl: './tag-edit.component.html',
  styleUrls: ['./tag-edit.component.css']
})
export class TagEditComponent implements OnInit {
  tagEditModel: TagEditModel;
  themeId: string;

  constructor(private activatedRoute: ActivatedRoute, private tagService: TagService,
              private toastr: ToastrManager, private router: Router) {
    this.tagEditModel = this.activatedRoute.snapshot.data['tagEditModel'];
    console.log('tag edit: ', this.tagEditModel);
    this.themeId = this.activatedRoute.snapshot.paramMap.get('themeId');
  }

  ngOnInit() {
  }

  saveTag(tag) {
    this
      .tagService
      .saveTag(this.themeId, tag)
      .subscribe(res => {
            this.toastr.successToastr('Tag Saved!', 'Success!');
        },
        err => {
          this.toastr.errorToastr('Tag could not save!', 'Error!');
        });
  }

  deleteTag() {
  }
}
