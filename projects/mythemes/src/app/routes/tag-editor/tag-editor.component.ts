import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {Tag, TagEditModel} from '../../core/models/tag-models';
import {TagProperty} from '../../core/models/theme-models';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-tag-editor',
  templateUrl: './tag-editor.component.html',
  styleUrls: ['./tag-editor.component.css']
})
export class TagEditorComponent implements OnInit {
  @Input() tagEditModel: TagEditModel;
  @Output() editComplete = new EventEmitter();

  tagForm: FormGroup;
  tagPropertiesFormArray = new FormArray([]);
  initialLatitude: number;
  initialLongitude: number;
  tag: Tag;
  markerImageUrl;
  tagProperties: TagProperty[];

  constructor(public toastr: ToastrManager) {
  }

  ngOnInit() {
    this.tag = this.tagEditModel.tag;
    this.tagProperties = this.tagEditModel.tagProperties;
    this.initMap();
    this.createForm();
  }

  createForm() {
    this.tagForm = new FormGroup({
      active: new FormControl(this.tagEditModel.tag.active),
      longitude: new FormControl(this.initialLongitude),
      latitude: new FormControl(this.initialLatitude),
    });
    this.tagForm.setControl('propertyValues', this.tagPropertiesFormArray);
    this.tag.propertyValues
      .forEach(x => {
        this.tagPropertiesFormArray.push(this.getTagPropItemForm(x));
      });
  }

  getTagPropItemForm(tagPropertyValue: { normalizedName: string; value: string; }) {
    const {value, normalizedName} = tagPropertyValue;
    const tagProperty = this.tagProperties.find(x => x.normalizedName === normalizedName);
    const name = this.getItemName(tagProperty);
    return new FormGroup({
      value: new FormControl(value, [this.getValidator(tagProperty)]),
      name: new FormControl(name),
      normalizedName: new FormControl(normalizedName),
    });
  }

  getValidator(tagProperty: TagProperty) {
    return (control: FormControl) => {
      const value = control.value;
      if (value) {
        if (this.isValueTypeCorrect(tagProperty.tagPropertyType, value)) {
          return null;
        } else {
          return {invalid: 'Type invalid'};
        }
      } else {
        if (tagProperty.mandatory) {
          return {invalid: 'Mandatory Field!'};
        } else {
          return null;
        }
      }
    };
  }

  isValueTypeCorrect(type: number, value: string) {
    switch (type) {
      case 0:
      case 1:
      case 7:
        return (typeof value === 'string');
      case 6:
        // @ts-ignore
        return !isNaN(value);
      case 2:
        return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
          .test(value);
      case 3:
      case 4:
      case 5:
        return /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g.test(value);
    }
  }

  getItemName(tagProp: TagProperty) {
    if (tagProp.mandatory) {
      return `${tagProp.name}*`;
    } else {
      return `${tagProp.name}`;
    }
  }

  saveTag() {
    if (this.tagForm.valid) {
      const formValue = this.tagForm.value;
      this.tag.active = formValue.active;
      this.tag.propertyValues = formValue.propertyValues;
      this.tag.location.coordinates = [formValue.longitude, formValue.latitude];
      this.editComplete.emit(this.tag);
    } else {
      this.toastr.errorToastr('Please check for errors');
    }
  }

  initMap() {
    const [longitude, latitude] = this.tagEditModel.tag.location.coordinates;
    this.initialLongitude = longitude;
    this.initialLatitude = latitude;
  }

  onMarkerDragged(event) {
    this.tagForm.controls['longitude'].setValue(event.coords.lng);
    this.tagForm.controls['latitude'].setValue(event.coords.lat);
  }
}
