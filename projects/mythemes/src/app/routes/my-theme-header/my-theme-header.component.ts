import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../core/services/authentication.service';
import {Router} from '@angular/router';
import {UserService} from '../../core/services/user.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-my-theme-header',
  templateUrl: './my-theme-header.component.html',
  styleUrls: ['./my-theme-header.component.css']
})
export class MyThemeHeaderComponent implements OnInit {

  @Input()
  returnUrl: string;

  email: string;
  isLoggedIn = false;
  profileImage = 'assets/images/ic_profile-pic.png';
storeUrl = environment.storeUrl;

  constructor(private authenticationSvc: AuthenticationService,
              private router: Router,
              private userSvc: UserService,
              private toastr: ToastrManager) {
  }

  ngOnInit() {
    this.authenticationSvc.currentUser.subscribe(user => {
      this.email = (user && user.name) || 'Login';
    });

    this.authenticationSvc.loginState.subscribe(isLoggedIn => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn === true) {
        this.userSvc.me().subscribe((user) => {
          this.profileImage = user.profileImageUrl;
        }, (error) => {
          this.profileImage = 'assets/images/ic_profile-pic.png';

        });
      }
    });


  }

  logout() {
    this.authenticationSvc.logout();
    this.router.navigate(['/my-themes']);

  }

  login() {
    this.router.navigate(['/local'], {queryParams: {returnUrl: this.returnUrl}});
  }
}
