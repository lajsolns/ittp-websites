import {Component, ElementRef, Inject, OnInit, Renderer, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AccessListService} from '../../core/services/access-list.service';
import {FileUploader} from 'ng2-file-upload';
import {AuthenticationService} from '../../core/services/authentication.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AccessListModalComponent} from '../../core/ui-components/access-list-modal/access-list-modal.component';

@Component({
  selector: 'itt-access-list',
  templateUrl: './access-list.component.html',
  styleUrls: ['./access-list.component.css']
})
export class AccessListComponent implements OnInit {
  themeId: string;
  newEmailUploader: FileUploader;
  newIDUploader: FileUploader;
  whitelists;
  searchResult: { _id: string, email: string };
  userId;
  pageSize = 5;
  currentPage = 1;
  count = 10;
  display = 'none';
  myModal;


  constructor(private activatedRoute: ActivatedRoute, private accessListService: AccessListService, private toastrService: ToastrManager,
              private authSvc: AuthenticationService, @Inject('API_BASE_URL') private apiBaseUrl: string, private el: ElementRef,
              private renderer: Renderer,
              private modalService: NgbModal) {
    this.themeId = activatedRoute.snapshot.params['themeId'];
    const authToken = `Bearer ${this.authSvc.currentUserValue.token}`;
    const emailFileUrl = `${this.apiBaseUrl}/access-list/theme/${this.themeId}/emails`;
    const idsFileUrl = `${this.apiBaseUrl}/access-list/theme/${this.themeId}/ids`;
    this.newEmailUploader = new FileUploader({url: emailFileUrl, authToken, itemAlias: 'file'});
    this.newIDUploader = new FileUploader({url: idsFileUrl, authToken, itemAlias: 'file'});
  }

  ngOnInit() {
    this.loadData();
  }

  open(email) {
    const modalRef = this.modalService.open(AccessListModalComponent);
    modalRef.componentInstance.userEmail = email;

    modalRef.componentInstance.add.subscribe((result) => {
      this.addToWhiteList(this.searchResult._id);
    });
  }

  searchForUsers($q: { searchInput: string }) {
    this.accessListService.searchUser($q.searchInput)
      .subscribe((res) => {
        if (res) {
          this.searchResult = res;
          this.open(this.searchResult.email);
          // document.getElementById('exampleButton').click();

        } else {
          this.toastrService.errorToastr('user does not exist');
        }
      }, error => {
        this.toastrService.errorToastr('something went wrong, please enter user');

      });
  }

  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.loadData();
  }

  private loadData() {
    this.accessListService.getWhitelistPaginated(this.themeId, this.currentPage, this.pageSize).subscribe(result => {
      this.count = result.count;
      this.whitelists = result.data;
      console.log(this.whitelists);
    });
  }

  addToWhiteList(userId) {
    this.accessListService.addToWhiteList(userId, this.themeId)
      .subscribe((res) => {
        this.toastrService.successToastr('user added to white list');
        this.loadData();
      });
  }

  removeFromWhiteList(userId: string) {

    this.accessListService.removeFromWhiteList(userId, this.themeId)
      .subscribe(res => {
        this.toastrService.successToastr('user removed from white list');
        this.loadData();
      });
  }
}
