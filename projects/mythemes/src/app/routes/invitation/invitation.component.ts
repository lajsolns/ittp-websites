import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {UserService} from '../../core/services/user.service';
import {CommonService} from "../../core/services/common.service";

@Component({
  selector: 'itt-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {
  isOpened = false;
  users = [];
  searchTerm = '';
  pageSize = 10;
  currentPage = 1;
  count = 10;
  themeId: string;

  constructor(private userService: UserService,
              private toastr: ToastrManager,
              private route: ActivatedRoute,
              private commonService: CommonService) {
    this.themeId = route.snapshot.paramMap.get('themeId');
  }

  ngOnInit() {
    this.loadUser();
  }


  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }


  loadUser() {
    this.userService.getUsersPaginate(this.currentPage, this.pageSize).subscribe(users => {
      this.users = users.data;
      this.count = users.count;
    });

    console.log('users: ', this.users);
  }


  onPageChanged(currentPage: number) {
    this.currentPage = currentPage;
    this.loadUser();
  }


  searchForUser(searchTerm) {
    // this.searchTerm = searchTerm.searchInput;
    // this.userService.searchUser(this.searchTerm).subscribe(users => {
    //   this.users = users;
    // });
  }

  invite(userId) {

    this.commonService.sendInvitation(this.themeId, userId)
      .subscribe(res => {
        console.log('respose', res);
        this.toastr.successToastr('Inviation sent');
      });
  }

}



