import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'itt-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  theme;
  constructor(private state: ActivatedRoute) {
    this.theme = this.state.snapshot.data['tagProperties'];
    console.log(this.theme);
  }


  ngOnInit() {
  }

}
