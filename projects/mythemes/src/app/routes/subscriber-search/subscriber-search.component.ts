import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'itt-subscriber-search',
  templateUrl: './subscriber-search.component.html',
  styleUrls: ['./subscriber-search.component.css']
})
export class SubscriberSearchComponent implements OnInit {
  subscriberSearchResult;

  constructor(private activatedRoute: ActivatedRoute) {
    this.subscriberSearchResult = this.activatedRoute.snapshot.data['searchInput'];
  }

  ngOnInit() {
  }

}
