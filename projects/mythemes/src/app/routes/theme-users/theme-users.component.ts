import {Component, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscriber} from '../../core/models/subcriber-models';
import {SubscriberService} from '../../core/services/subscriber.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'itt-theme-users',
  templateUrl: './theme-users.component.html',
  styleUrls: ['./theme-users.component.css']
})
export class ThemeUsersComponent implements OnInit {
  searchTermUsers = '';
  searchTermNewUser = '';
  theme;
  currentPage = 1;
  pageSize = 20;
  count = 0;
  subscribers: Subscriber[] = [];
  themeId;
  isOpened = false;

  constructor(private activatedRoute: ActivatedRoute, private toastr: ToastrManager,
              private subscriberService: SubscriberService,
              private modalService: NgbModal,
              private router: Router) {
    this.themeId = activatedRoute.snapshot.paramMap.get('themeId');
  }

  ngOnInit() {
    this.loadUsers();
  }

  searchForUsers(searchTermUsers) {
    if (searchTermUsers.searchInput) {
      this.searchTermUsers = searchTermUsers.searchInput;
      this.subscriberService.searchSubscriber(this.themeId,  this.currentPage, this.pageSize, this.searchTermUsers).subscribe((res) => {
        this.count = res.count;
        this.subscribers = res.data;
        this.currentPage = res.page;
        console.log(res);
      });
    } else {
      this.loadUsers();
    }
  }

  searchForNewUser(searchTermNewUser) {
    this.searchTermNewUser = searchTermNewUser.searchInput;
    console.log(searchTermNewUser);
  }

  private loadUsers() {

    this.subscriberService.getSubscribedUsersPaginated(this.themeId, this.currentPage, this.pageSize).subscribe((res) => {
        this.count = res.count;
        this.subscribers = res.data;
        this.currentPage = res.page;
        console.log(res);
      }
    );

  }

  //
  // unsubscribeUser(subscription: any, i) {
  //   this.subscriptionService.unsubscribeFromTheme(subscription.themeId, subscription.userId).subscribe(res => {
  //       this.userSubscriptions.splice(index, 1);
  //       console.log(this.userSubscriptions);
  //       this.toastr.success('User Removed!', 'Succes!');
  //     },
  //     err => {
  //       this.toastr.error('User Removed!', 'Error!');
  //       console.log(err);
  //     });
  // }

  onPageChange(currentPage: number) {
    this.currentPage = currentPage;
    this.loadUsers();
  }

  unsubscribeUser(userId, index) {
    this.subscriberService.unsubscribeFromTheme(this.themeId, userId).subscribe(res => {
        if (res) {
          this.subscribers.splice(index, 1);
          this.toastr.successToastr('User unsubscribed!', 'Succes!');
        }
      },
      err => {
        this.toastr.errorToastr('User could not be unsubscribed', 'Error!');
      });

  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}

