import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'itt-my-themes',
  templateUrl: './my-themes.component.html',
  styleUrls: ['./my-themes.component.css']
})
export class MyThemesComponent implements OnInit {
  isOpened = false;
  events: string[] = [];
  opened: boolean;
  constructor() { }

  ngOnInit() {
  }
  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
