import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LatLngBounds} from '@agm/core';
import {MapMarker} from '../../core/models/tag-models';
import {UserPreferencesService} from '../../core/services/user-preferences.service';
import {Subscription} from 'rxjs';
import {TagService} from '../../core/services/tag.service';
import {Theme} from '../../core/models/theme-models';
import {UserLocationService} from '../../core/services/user-location-service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddTagModalComponent} from '../add-tag-modal/add-tag-modal.component';
import {LatLng} from '../../core/models/common.models';

@Component({
  selector: 'itt-tag-map',
  templateUrl: './tag-map.component.html',
  styleUrls: ['./tag-map.component.css']
})
export class TagMapComponent implements OnInit {
  isOpened = false;
  markers: MapMarker[];
  tagQuerySubscription: Subscription;
  bounds: LatLngBounds;
  themeId;
  markerIconUrl: string;
  location: LatLng;
  lat: string;
  lng: string;

  constructor(private activatedRoute: ActivatedRoute,
              private userPrefs: UserPreferencesService,
              private tagService: TagService,
              private router: Router,
              private locationService: UserLocationService,
              private modalService: NgbModal,
  ) {
    this.markers = [];
    this.themeId = this.activatedRoute.snapshot.paramMap.get('themeId');
    this.location = userPrefs.getLastLocation();
  }


  ngOnInit() {
    this.initMap();
  }

  private initMap() {
    this
      .locationService
      .getUserLocation()
      .subscribe(loc => this.location = loc, console.log);
  }


  onMarkerClicked(marker: MapMarker) {
    console.log('clicked marker', marker.id);
    // this.router.navigate(['themes', this.tagProperties.id, 'tags', 'edit', event.id]);
  }

  onMapReady(event: any) {

  }

  onMapClicked(e) {

  }

  onMapIdle() {
    if (this.tagQuerySubscription) {
      this.tagQuerySubscription.unsubscribe();
    }

    const bottomLeft = {longitude: this.bounds.getSouthWest().lng(), latitude: this.bounds.getSouthWest().lat()};
    const upperRight = {longitude: this.bounds.getNorthEast().lng(), latitude: this.bounds.getNorthEast().lat()};

    this.tagQuerySubscription = this.tagService.getTagsInBounds({bottomLeft, upperRight, themeId: this.themeId})
      .subscribe(res => {
        this.markers = res.markers;
        this.markerIconUrl = res.iconUrl;
      }, (err) => {
        console.log('marker errors', err);
      });
  }

  centerChange(event) {
    this.userPrefs.setLastLocation({lat: event.lat, lng: event.lng});
  }

  onBoundsChange(event: LatLngBounds) {
    this.bounds = event;
  }

  onMapRightClicked(event: any) {
    const modalRef = this.modalService.open(AddTagModalComponent, {centered: true});
    modalRef.componentInstance.themeId = this.themeId;
    modalRef.componentInstance.lat = this.location.lat;
    modalRef.componentInstance.lng = this.location.lng;

    const {lat, lng} = event.coords;
    this.userPrefs.setLastLocation({lat, lng});
    // todo open the modal here
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}

