import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TagService} from '../../core/services/tag.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {TagEditModel} from '../../core/models/tag-models';

@Component({
  selector: 'itt-tag-new',
  templateUrl: './tag-new.component.html',
  styleUrls: ['./tag-new.component.css']
})
export class TagNewComponent implements OnInit {
  isOpened = false;
  themeId: string;
  tagEditModel: TagEditModel;
  tagProperties;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tagService: TagService,
    private toastr: ToastrManager,
    private router: Router
  ) {
    this.tagEditModel = this.activatedRoute.snapshot.data['tagEditModel'];
    this.themeId = activatedRoute.snapshot.params['themeId'];
  }

  ngOnInit() {
  }

  createTag() {
    this
      .tagService
      .createTag(this.themeId, this.tagEditModel.tag)
      .subscribe(res => {
        this.toastr.successToastr('Tag Saved!', 'Succes!');
      },
      err => {
        this.toastr.errorToastr('Tag could not save!', 'Error!');
      });
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
