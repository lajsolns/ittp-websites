import {Component, OnInit} from '@angular/core';
import {ThemeEditPreview} from '../../core/models/edit-models';
import {PaginationResModel, ThemeEditService} from '../../core/services/theme-edit.service';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';


@Component({
  selector: 'itt-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.css']
})
export class ThemeListComponent implements OnInit {
  storeUrl = environment.storeUrl;
  themes: ThemeEditPreview[] = [];
  term = '';
  themeFilter;
  apiUrl;
  pageSize = 10;
  currentPage;
  count;
  isOpened: boolean = false;

  constructor(private themeEditService: ThemeEditService, private router: Router) {
  }

  ngOnInit() {
    this.loadFirstPage();
  }

  onPageChanged(page: number) {
    this
      .themeEditService
      .getUserThemePreviewsPagination({page, pageSize: this.pageSize})
      .subscribe(resModel => this.updatePage(resModel), console.log);
  }


  private loadFirstPage() {
    this
      .themeEditService
      .getUserThemePreviewsPagination({page: 1, pageSize: this.pageSize})
      .subscribe(resModel => this.updatePage(resModel), console.log);
  }

  updatePage(resModel: PaginationResModel) {
    this.themes = resModel.data;
    this.count = resModel.count;
    this.currentPage = resModel.page;
    console.log('these are the themes: ', this.themes);
  }

  findTheme(value) {
    const searchTerm = value.searchInput;
    this.router.navigate([`/search-theme/${searchTerm}`]);
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }
}
