import {Component, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ThemeEditPreview} from '../../core/models/edit-models';
import {PaginationResult} from '../../../../../theme-store/src/app/core/models/pagination-result.model';
import {WishlistService} from '../../core/services/wishlist.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  themeId;
  storeUrl = environment.storeUrl;
  pageSize = 10;
  currentPage = 1;
  count;
  wishLists: {_id: string; name: string}[] = [];

  constructor(private wishListService: WishlistService, private route: ActivatedRoute, private toastr: ToastrManager) {
    this.themeId = route.snapshot.paramMap.get('themeId');
  }

  ngOnInit() {
    this.loadWishList();
  }

  onPageChanged(page: number) {

  }


  private loadWishList() {
this.wishListService.getMyWishListPaginated(this.currentPage, this.pageSize, this.themeId).subscribe(wishlist => {
  this.wishLists = wishlist.data;
  this.count = wishlist.count;

});
  }

  updatePage(resModel: PaginationResult) {

  }

  searchForUsers(query) {

  }

  onPageChange(event) {

  }

  subscribe(userId, themeIndex) {
    console.log(userId);
    this.wishListService.subscribe(this.themeId, userId).subscribe((res) => {
      this.toastr.successToastr('successfully subscribed');
      this.wishLists.splice(themeIndex, 1);
    });
  }
}

export interface WishList {
  _id: string;
  userName: string;
  profileImageUrl: string;
}
