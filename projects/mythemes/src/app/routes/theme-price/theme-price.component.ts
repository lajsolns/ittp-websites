import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-theme-price',
  templateUrl: './theme-price.component.html',
  styleUrls: ['./theme-price.component.css']
})
export class ThemePriceComponent implements OnInit {

  theme;
  isOpened: boolean = false;


  constructor(private toastr: ToastrManager, private activatedRoute: ActivatedRoute) {
    this.theme = activatedRoute.snapshot.data['tagProperties'];
  }

  ngOnInit() {
  }

  save() {
    // To do
    console.log('save for price');
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

}
