import {Component, Input, OnInit} from '@angular/core';
import {FileOperationModel} from '../file-operations.component';

@Component({
  selector: 'itt-file-operation-row',
  templateUrl: './file-operation-row.component.html',
  styleUrls: ['./file-operation-row.component.css']
})
export class FileOperationRowComponent implements OnInit {
  @Input() fileOperation: FileOperationModel;
  constructor() { }

  ngOnInit() {
  }

  getStatusColor() {
    return this.fileOperation.hasErrors ? 'red' : 'green';
  }

}
