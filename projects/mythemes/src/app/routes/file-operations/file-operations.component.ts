import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {TagService} from '../../core/services/tag.service';
import {FileUploader} from 'ng2-file-upload';
import {AuthenticationService} from '../../core/services/authentication.service';

@Component({
  selector: 'itt-file-operations',
  templateUrl: './file-operations.component.html',
  styleUrls: ['./file-operations.component.css']
})
export class FileOperationsComponent implements OnInit {
  fileOperations: FileOperationModel[];
  isOpened = false;
  themeId: string;
  newUploader: FileUploader;
  updatedUploader: FileUploader;

  constructor(
    private toastr: ToastrManager,
    private tagService: TagService,
    private activatedRoute: ActivatedRoute,
    private authSvc: AuthenticationService,
    @Inject('API_BASE_URL') private apiBaseUrl: string) {
    this.themeId = activatedRoute.snapshot.params['themeId'];

    const newTagsFileUrl = `${this.apiBaseUrl}/my-themes/theme/${this.themeId}/file-upload/new`;
    const updatedTagsFileUrl = `${this.apiBaseUrl}/my-themes/theme/${this.themeId}/file-upload/updated`;

    const authToken = `Bearer ${this.authSvc.currentUserValue.token}`;

    this.newUploader = new FileUploader({url: newTagsFileUrl, authToken, itemAlias: 'file'});
    this.updatedUploader = new FileUploader({url: updatedTagsFileUrl, authToken, itemAlias: 'file'});
  }

  ngOnInit() {
    this.newUploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.newUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.toastr.successToastr(response.msg);
      this.getFileOperations();
    };

    this.updatedUploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.updatedUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.toastr.successToastr(response.msg);
      this.getFileOperations();
    };

    this.getFileOperations();
  }

  getFileOperations() {
    this.tagService.getFileOperations(this.themeId).subscribe(res => {
      this.fileOperations = res;
    });
  }

  uploadNewTags() {

  }

  uploadExistingTags() {


  }

  refresh() {
    this.getFileOperations();
  }

  downloadTagFile() {
    this.tagService.downloadTagFile(this.themeId);
  }

  downloadExistingTags() {
    this.tagService.downloadExistingTags(this.themeId);

  }
  newTagsUploader() {
    this.newUploader.uploadAll();
    this.toastr.successToastr('Tag uploading');
  }

  oldTagsUploader() {
    this.updatedUploader.uploadAll();
    this.toastr.successToastr('Tag uploading');
  }

  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

  reloadPage() {
    window.location.reload(true);
  }
}

export interface FileOperationModel {
  userId: string;
  jobType: string;
  fileInfo: {
    name: string;
    size: string;
  };
  status: string;
  hasErrors: boolean;
  summary: {
    processedLines: string;
    savedTagsCount: string;
    errorCount: string;
    duration: string;
    errorMessages: string;
  };
  createdAt: string;
}
