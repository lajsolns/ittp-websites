import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'itt-tag-stats',
  templateUrl: './tag-stats.component.html',
  styleUrls: ['./tag-stats.component.css']
})
export class TagStatsComponent implements OnInit {
  tagCount;
  themeId;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels: string[] = ['Today', 'This Week', 'This Month'];
  barChartType = 'bar';
  barChartLegend = true;
  tagStats = 0;
  barChartData = [
    {
      data: [10, 50, 100],
      label: 'Subscribers'
    },
    {
      data: [2, 10, 20],
      label: 'Unsubscribers'
    }
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.themeId = activatedRoute.snapshot.paramMap.get('themeId');
    this.tagCount = activatedRoute.snapshot.data['tagStats'];
    this.tagCount = this.tagCount.stats.tags.count;
  }

  ngOnInit() {

  }

  chartClicked($event: any) {
    //  todo
  }

  chartHovered($event: any) {
    //  todo
  }

}
