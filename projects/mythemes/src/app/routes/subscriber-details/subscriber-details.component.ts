import {Component, OnInit} from '@angular/core';
import {Subscriber, SubscriberDetail} from '../../core/models/subcriber-models';
import {ActivatedRoute, Router, RouterState, RouterStateSnapshot} from '@angular/router';
import {SubscriptionService} from '../../core/services/subscription.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Location} from '@angular/common';
import {SubscriberService} from '../../core/services/subscriber.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'itt-subscriber-details',
  templateUrl: './subscriber-details.component.html',
  styleUrls: ['./subscriber-details.component.css']
})
export class SubscriberDetailsComponent implements OnInit {
  isOpened = false;
  subscriber: SubscriberDetail;
  subscriberId;
  themeId;
  permissionForm;

  constructor(private  route: ActivatedRoute, private subscriberService: SubscriberService, private subscriptionService: SubscriptionService, private location: Location, private toastr: ToastrManager) {
    this.subscriber = this.route.snapshot.data['subscriberDetails'];
    this.themeId = this.route.snapshot.paramMap.get('themeId');
    this.subscriberId = route.snapshot.paramMap.get('subscriberId');

  }

  ngOnInit() {
    this.permissionForm = new FormGroup({
      canDelete: new FormControl(this.subscriber.subscription.tagPermissions.canDelete),
      canEdit: new FormControl(this.subscriber.subscription.tagPermissions.canEdit),
      canCreate: new FormControl(this.subscriber.subscription.tagPermissions.canCreate),
    });
  }


  toggleSidebar() {
    this.isOpened = !this.isOpened;
  }

  unsubscribe() {
    this
      .subscriptionService
      .unsubscribe(this.themeId)
      .subscribe(res => {
        this.toastr.successToastr('Successfully unsubscribed');
        this.location.back();
      }, error1 => {
        this.toastr.errorToastr('Could not unsubscribe');
      });
  }

  save(permissions) {
    this.subscriberService.saveSubscription(this.themeId, this.subscriberId, permissions).subscribe((res) => {
        this.toastr.successToastr('successfully saved');
      },
      (err) => {
        this.toastr.errorToastr('Could not save');
      });
  }

}
