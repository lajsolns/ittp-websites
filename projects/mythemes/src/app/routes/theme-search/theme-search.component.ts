import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-search',
  templateUrl: './theme-search.component.html',
  styleUrls: ['./theme-search.component.css']
})
export class ThemeSearchComponent implements OnInit {
  themeSearchResults;
  storeUrl = environment.storeUrl;

  constructor(private activatedRoute: ActivatedRoute) {
    this.themeSearchResults = this.activatedRoute.snapshot.data['searchInput'];
    console.log('theme-search results', this.themeSearchResults);
  }

  ngOnInit() {
  }

}
