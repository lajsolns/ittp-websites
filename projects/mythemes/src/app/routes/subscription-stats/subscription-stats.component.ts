import {Component, OnInit} from '@angular/core';
import {ThemeDetail} from '../../core/models/theme-detail.model';
import {ActivatedRoute, Router} from '@angular/router';
import {MyThemeService} from '../../core/services/mytheme.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {saveAs} from 'file-saver';

@Component({
  selector: 'itt-subscription-stats',
  templateUrl: './subscription-stats.component.html',
  styleUrls: ['./subscription-stats.component.css']
})
export class SubscriptionStatsComponent implements OnInit {
  themeDetail: ThemeDetail;
  subscriptionStats: SubscriptionStats;
  totalSubscription: number;
  themeId;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels: string[] = ['Today', 'This Week', 'This Month'];
  barChartType = 'bar';
  barChartLegend = true;
  barChartData = [
    {
      data: [0, 0, 0, 0],
      label: 'Subscribers'
    },
    {
      data: [0, 0, 0, 0],
      label: 'Unsubscribers'
    }
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private myThemeService: MyThemeService,
              private toastr: ToastrManager,
              private router: Router) {
    this.themeId = activatedRoute.snapshot.paramMap.get('themeId');
    this.activatedRoute.data.subscribe( stats => {
      console.log('stats: ', stats.subscriptionStats.stats.subscriptions);
      const res = stats.subscriptionStats.stats.subscriptions;
      this.barChartData[0].data = res.data.subscribe;
      this.barChartData[1].data = res.data.unsubscribe;
      this.totalSubscription = res.total.subscribe;
    });


  }

  ngOnInit() {

  }


  chartClicked($event: any) {
    //  todo
  }

  chartHovered($event: any) {
    //  todo
  }
}

export class BarChartData {
  label: string[];
  data: { subscribers: number, unsubscribers: number }[];
}

export class SubscriptionStats {
  data: {subscribe: number[], unsubscribe: number[]};
  total: {subscribe: number[], unsubscribe: number[]};
  updatedAt: number;
}
