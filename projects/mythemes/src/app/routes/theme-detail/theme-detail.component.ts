import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {saveAs} from 'file-saver';
import {MyThemeService} from '../../core/services/mytheme.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ThemeDetail} from '../../core/models/theme-detail.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'itt-theme-detail',
  templateUrl: './theme-detail.component.html',
  styleUrls: ['./theme-detail.component.css']
})
export class ThemeDetailComponent implements OnInit {
  storeUrl = environment.storeUrl;
  themeDetail: ThemeDetail;

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels: string[] = ['Today', 'This Week', 'This Month'];
  barChartType = 'bar';
  barChartLegend = true;
  barChartData: any[] = [
    {
      data: [10, 50, 100],
      label: 'Subscribers'
    },
    {
      data: [2, 10, 20],
      label: 'Unsubscribers'
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private myThemeService: MyThemeService,
    private toastr: ToastrManager,
    private router: Router,
    private modalService: NgbModal) {

    this.themeDetail = this.activatedRoute.snapshot.data['themeDetail'];
    console.log('detail: ', this.themeDetail);

  }


  ngOnInit() {
    // this.themeDetail = this.getDetailPreviewPlaceHolder()
  }


  exportTheme() {

    const blob = new Blob([JSON.stringify(this.themeDetail)], {type: 'application/json'});
    const url = window.URL.createObjectURL(blob);
    const file = new File([blob], `${this.themeDetail.name}.json`, {type: 'application/json'});
    saveAs(file);
  }

  deleteAllTags(_id: string) {
    //  todo
  }


  deleteTheme() {
    this.myThemeService.deleteTheme(this.themeDetail._id).subscribe((res) => {
      this.toastr.successToastr('Theme has been deleted');
      this.router.navigate(['/my-themes']);
    });
  }


}
