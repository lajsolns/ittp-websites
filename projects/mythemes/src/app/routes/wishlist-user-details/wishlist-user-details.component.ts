import {Component, OnInit} from '@angular/core';
import {WishList} from '../wishlist/wishlist.component';
import {ActivatedRoute} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'itt-wishlist-user-details',
  templateUrl: './wishlist-user-details.component.html',
  styleUrls: ['./wishlist-user-details.component.css']
})
export class WishlistUserDetailsComponent implements OnInit {
  wishListUserDetail: WishList;
  wishListUserId;
  themeId;
  constructor(private  route: ActivatedRoute, private toastr: ToastrManager) {
    this.wishListUserDetail = this.route.snapshot.data['wishListUserDetails'];
    console.log('wish list user', this.wishListUserDetail);
    this.themeId = this.route.snapshot.paramMap.get('themeId');
    this.wishListUserId = route.snapshot.paramMap.get('wishListUserId');
  }

  ngOnInit() {
  }

  subscribe() {
this.toastr.successToastr('subscribed User Successfully');
  }
}
